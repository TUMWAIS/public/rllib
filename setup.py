from setuptools import setup, find_packages
import platform

# Get the local package version
def _version():
    path = "__version__.py"
    namespace = {}
    with open(path) as stream:
        exec(stream.read(), namespace)
    return namespace["__version__"]


# Setup parameter
install_requires = ["tensorflow==2.3.0",
                    "tensorflow-probability==0.11.0",
                    "numpy==1.18.4",
                    "gym[box2D]==0.17.2",
                    "psutil==5.7.0",
                    "matplotlib==3.2.1",
                    "wandb==0.8.36"]

# Only add ray on Linux platforms
if platform.system() == "Linux":
    install_requires.append(["ray==1.0.0"])

setup(
        name = "rllib",
        version = _version(),
        description = "A deep reinforcement learning toolbox",
        author = "Jonas Zinn",
        author_email = "jonas.zinn@tum.de",
        packages = find_packages(),
        install_requires = install_requires
     )
