from rllib.classes.rollout_actors.xppu.RolloutActorXPPU import RolloutActorXPPU
from rllib.functions.parallel import run_parallel

@run_parallel()
class RolloutActorXPPUStd(RolloutActorXPPU):
    """
    Standard class used by DQNRainbow if no trainer is provided.
    This class cannot be inherited.
    """
    pass