import json
import os
import h5py

import numpy as np
import tensorflow as tf

from rllib.classes.errors.ConfigurationError import ConfigurationError
from rllib.functions.tensorflow import get_weights, set_weights
from copy import deepcopy


class CoordPredictorStatefulXPPU():
    """
    Class responsible for predicting workpiece coordinates from sensor values.
    """

    def __init__(self,
                 sensor_value_conf_file,
                 action_value_conf_file,
                 training_data_path,
                 model_architecture,  # =[500, 500, 500],
                 buffer_size=None,  # =25000,
                 batch_size=None,
                 num_epochs=None,  # =10,
                 learning_rate=None,
                 round_coords=None,
                 num_wps=None,
                 store_training_data=True):

        # Set paths
        self._buffer_path = os.path.join(training_data_path, "buffer")

        # Set parameters
        self.model_architecture = model_architecture
        self.buffer_size = buffer_size
        self.batch_size = batch_size
        self.num_epochs = num_epochs
        self.learning_rate = learning_rate
        self.num_wps = num_wps

        # Initialize internal state variables
        self.action = None
        self.state = None
        self.state_next = None
        self.predicted_coords = None
        self.predicted_coords_prev = None

        # Set configuration flags
        self.use_predicted_coords = True                    # Activate coordinate prediction
        self.prediction_state_from_sim = False              # Overwrite internal state with state from simulation (stateful predictors only)
        self.store_training_data = store_training_data      # Store transitions for training in buffer
        self.round_coords = round_coords                    # Round predicted coordinates

        # Load config file
        with open(sensor_value_conf_file) as json_file:
            self.sensor_value_conf = json.load(json_file)

        with open(action_value_conf_file) as json_file:
            self.action_value_conf = json.load(json_file)

        # Load or create model
        self.model, self.optimizer, self.loss = self.create_network()

        # Load buffer
        self.load_buffer_from_file()



    def reset(self, state, action):
        """
        Resets the internal state of the predictor

        :param states:  Dictionary with states from the simulation
        :param action:  Dictionary with action variables from the simulation (not used here)
        """
        # Reset internal states
        self.action = action
        self.state = state
        self.state_next = state
        self.predicted_coords = None
        self.predicted_coords_prev = None

        # Store inputs and labels for training
        if self.store_training_data is True:

            # Get number of workpieces for which coordinates need to be stored
            if self.num_wps is None:
                self.num_wps = len(state["workpiece"])

            # Store initial state in buffer
            self.store_sensor_values(state, action, self.state_next)



    def update(self, state, action, state_next):
        """
        Updates the internal state of the predictor

        :param states:      Dictionary with simulation state before applying the action
        :param action:      Dictionary with action variables from the simulation (not used here)
        :param state_next:  Dictionary with simulation state after applying the action
        """
        self.action = action
        self.state = state
        self.state_next = state_next
        self.predicted_coords_prev = deepcopy(self.predicted_coords)
        self.predicted_coords = None


        # Store inputs and labels for training
        if self.store_training_data is True:
            self.store_sensor_values(state, action, state_next)



    def predict(self):
        """
        Returns predicted coordinates if coordinate prediction is activated. Otherwise, coordinates from
        the simulation are returned.

        :param sensor_values:   Dictionary with sensor values from simulation
        :return:                List with coordinates [x, y, z]
        """

        # Predict coordinates for the current simulation state (if not yet predicted)
        if self.predicted_coords is None:
            if self.use_predicted_coords is True:
                input = []

                # Get coordinates in state t
                if self.prediction_state_from_sim is True or self.predicted_coords_prev is None:
                    for idx in range(self.num_wps):
                        input.append(self.state["workpiece"][idx]["position.x"])
                        input.append(self.state["workpiece"][idx]["position.y"])
                        input.append(self.state["workpiece"][idx]["position.z"])
                else:
                    input.extend(list(self.predicted_coords_prev))

                # Store sensor values in state t as inputs
                #for key, value in self._sensor_value_conf.items():
                #    if value is True:
                #        input.append(self.state[key])

                # Get action in state t
                for key, value in self.action_value_conf.items():
                    if value is True:
                        input.append(self.action[key])

                # Get sensor values in state t+1 as inputs
                for key, value in self.sensor_value_conf.items():
                    if value is True:
                        input.append(self.state_next[key])

                input = np.array([input]).astype(np.float32)

                # Predict coordinates
                self.predicted_coords = self.model(input)[0].numpy()

            else:
                x = self.state_next["workpiece"][0]["position.x"]
                y = self.state_next["workpiece"][0]["position.y"]
                z = self.state_next["workpiece"][0]["position.z"]
                self.predicted_coords = np.array([x, y, z])

        return self.predicted_coords



    def train(self):
        """
        Train the coordinate prectiction model with data from the buffer. A .lock file is created in order to
        avoid the simultanious training of the model by multiple actors. After training is completed the buffers are
        also saved to files for restart purposes.
        """
        if self.store_training_data is True:
            self.model.fit(np.array(self.input_list),
                           np.array(self.output_list),
                           batch_size=self.batch_size,
                           epochs=self.num_epochs,
                           workers=0,                           # Ensure that only the worker's core is used
                           verbose=2)

            weights = get_weights(self.model)

        else:
            raise ConfigurationError(self.store_training_data, "Training data need to be stored in order to train the model.")

        return weights



    def store_sensor_values(self, state, action, state_next):
        """
        Stores the provided sensor values in a buffer for training of the coordinate predictor. If the maximum buffer
        buffer size is reached random elements are deleted from the buffer.

        :param states:  Dictionary with states from the simulation
        """
        input_dataset = []

        # Store coordinates in state t as inputs
        for idx in range(self.num_wps):
            input_dataset.append(state["workpiece"][idx]["position.x"])
            input_dataset.append(state["workpiece"][idx]["position.y"])
            input_dataset.append(state["workpiece"][idx]["position.z"])

        # Store sensor values in state t as inputs
        #for key, value in self._sensor_value_conf.items():
        #    if value is True:
        #        input_dataset.append(state[key])

        # Store action in state t as inputs
        for key, value in self.action_value_conf.items():
            if value is True:
                input_dataset.append(action[key])

        # Store sensor values in state t+1 as inputs
        for key, value in self.sensor_value_conf.items():
            if value is True:
                input_dataset.append(state_next[key])

        self.input_list.append(input_dataset)

        # Store actual coordinates in state t+1 as labels
        output_dataset = []
        for idx in range(self.num_wps):
            output_dataset.append(state_next["workpiece"][idx]["position.x"])
            output_dataset.append(state_next["workpiece"][idx]["position.y"])
            output_dataset.append(state_next["workpiece"][idx]["position.z"])

        self.output_list.append(output_dataset)

        # Delete random element from the buffer if maximum size is exceeded
        if len(self.input_list) > self.buffer_size:
            idx = np.random.randint(self.buffer_size)
            self.input_list.pop(idx)
            self.output_list.pop(idx)



    def update_weights(self, weights):
        """
        Updates the weights of the model with the provided ones

        :param weights:    List of numpy array with weights for each layer
        """
        set_weights(self.model, weights)



    def create_network(self):
        """
        Loads or creates a network for coordinate prediction

        :return model:      Keras model object representing the compiled model
        :return optimizer:  Keras optimizer object
        :return los:        Keras loss object
        """

        # Get number of network inputs
        num_inputs = 3      # 3 coordinates to feed in
        for key, value in self.sensor_value_conf.items():
            if value is True:
                #num_inputs += 2     # For state t and state t+1
                num_inputs += 1  # For state t

        for key, value in self.action_value_conf.items():
            if value is True:
                num_inputs += 1

        # Add input
        input = tf.keras.Input(shape=(num_inputs,))

        # Add hidden layers
        x = input
        for num_neurons in self.model_architecture:
            x = tf.keras.layers.Dense(num_neurons,
                                      activation="relu")(x)

        # Add output
        output = tf.keras.layers.Dense(3,
                                       activation="linear")(x)

        # Create and compile model
        model = tf.keras.Model(inputs=input, outputs=output)

        # Compile model
        if self.store_training_data is True:
            optimizer = tf.keras.optimizers.Adam(learning_rate=self.learning_rate)
            loss = tf.keras.losses.mean_squared_error
            model.compile(optimizer=optimizer, loss=loss)
        else:
            optimizer=None
            loss = None

        return model, optimizer, loss



    def save_buffer_to_file(self):
        """
        Saves the current buffer with traning data to a file.
        """
        with h5py.File(self._buffer_path, "w") as file:
            file.create_dataset("input_list", data=self.input_list, compression="gzip")
            file.create_dataset("output_list", data=self.output_list, compression="gzip")



    def load_buffer_from_file(self):
        """
        Loads the buffer with training data from a file or creates a new empty one
        """
        try:
            with h5py.File(self._buffer_path, "r") as file:
                self.input_list = file["input_list"]
                self.output_list = file["output_list"]

        except:
            self.input_list = []
            self.output_list = []



    def prioritize_buffer(self):
        """
        Sorts the buffer according to the prediction errors from low to high

        """
        input = np.array(self.input_list)
        output = np.array(self.output_list)

        pred_values = self.model.predict(input)
        loss = (pred_values[:,0] - output[:,0])**2 + (pred_values[:,1] - output[:,1])**2 + (pred_values[:,2] - output[:,2])**2

        sorted_list = sorted(zip(loss, self.input_list, self.output_list), reverse=True)
        #self.input_list = [input for loss, input, output in sorted_list]
        #self.output_list = [output for loss, input, output in sorted_list]

        # Duplicate samples with high loss
        num_repeat = 3
        input_list = []
        output_list = []
        for idx, (loss, input, output) in enumerate(sorted_list):

            for i in range(num_repeat):
                input_list.append(input)
                output_list.append(output)

            if len(input_list)+num_repeat > self.buffer_size:
                break

        input_list.reverse()
        output_list.reverse()
        self.input_list = input_list
        self.output_list = output_list