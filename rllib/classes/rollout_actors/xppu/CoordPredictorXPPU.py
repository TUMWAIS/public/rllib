import tensorflow as tf
import json
import os
import numpy as np
import time

from rllib.functions.saving import save_to_file, load_from_file


class CoordinatePredictor():
    """
    Class responsible for predicting workpiece coordinates from sensor values.
    """

    def __init__(self,
                 actor_id,
                 coord_sensor_value_file,
                 coord_predictor_path,
                 coord_buffer_size=[25000],
                 coord_model_architecture=[500, 500, 500],
                 coord_train_epochs=10,
                 coord_val_split=0.1,
                 coord_num_wps=None,
                 store_training_data=True):

        # Create directory if not existing
        if not os.path.exists(coord_predictor_path):
            os.makedirs(coord_predictor_path)

        # Set paths
        self.coord_model_path = os.path.join(coord_predictor_path, "model")
        self.coord_buffer_file = os.path.join(coord_predictor_path, "coord_buffer_" + str(actor_id) + ".pkl")
        self.lock_file_path = os.path.join(coord_predictor_path, "model.lock")

        # Set training parameters
        self.coord_buffer_size = coord_buffer_size
        self.coord_model_architecture = coord_model_architecture
        self.coord_train_epochs = coord_train_epochs
        self.coord_val_split = coord_val_split
        self.coord_num_wps = coord_num_wps

        # Set flag to activate coordinate prediction
        self.use_predicted_coords = False
        self.store_training_data = store_training_data

        # Load config file
        with open(coord_sensor_value_file) as json_file:
            self.sensor_value_conf = json.load(json_file)

        # Load model from path
        self.build_network()
        self.update_weights()

        # Load buffer
        self.load_buffer()

        # Delete lock file if existing
        if os.path.exists(self.lock_file_path):
            os.remove(self.lock_file_path)



    def predict(self, sensor_values):
        """
        Returns predicted coordinates if coordinate prediction is activated. Otherwise, coordinates from
        the simulation are returned.

        :param sensor_values:   Dictionary with sensor values from simulation
        :return:                List with coordinates [x, y, z]
        """

        if self.use_predicted_coords is True:
            # Get network input from sensor values
            input = []
            for key, value in self.sensor_value_conf.items():
                if value is True:
                    input.append(sensor_values[key])
            input = np.array([input])

            # Predict coordinates
            y_pred = self.model(input)[0].numpy()

        else:
            x = sensor_values["workpiece"][0]["position.x"]
            y = sensor_values["workpiece"][0]["position.y"]
            z = sensor_values["workpiece"][0]["position.z"]
            y_pred = np.array([x, y, z])

        return y_pred



    def build_network(self):
        """
        Builds the network for coordinate prediction.
        """

        # Get number of network inputs
        num_inputs = 0
        for key, value in self.sensor_value_conf.items():
            if value is True:
                num_inputs += 1

        # Add input
        input = tf.keras.Input(shape=(num_inputs,))

        # Add hidden layers
        x = input
        for num_neurons in self.coord_model_architecture:
            x = tf.keras.layers.Dense(num_neurons,
                                      activation="relu")(x)

        # Add output
        output = tf.keras.layers.Dense(3,
                                       activation="linear")(x)

        # Create and compile model
        self.model = tf.keras.Model(inputs=input, outputs=output)
        self.model.compile(optimizer='adam',
                           loss='mean_absolute_error',
                           metrics=['mean_squared_error'])



    def train(self):
        """
        Train the coordinate prectiction model with data from the buffer. A .lock file is created in order to
        avoid the simulatanious training of the model by multiple actors. After training is completed the buffers are
        also saved to files for restart purposes.
        """

        # Load model from file and create lock file
        lock = True
        while lock is True:
            if os.path.exists(self.lock_file_path) is False:
                file = open(self.lock_file_path, "w")
                file.close()
                lock = False
            else:
                time.sleep(1)

        # Load latest model from file
        self.update_weights()

        # Train model
        hist = tf.keras.callbacks.History()
        early_stopping_monitor = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                                  min_delta=1e-3,
                                                                  mode='auto',
                                                                  verbose=0,
                                                                  patience=200,
                                                                  restore_best_weights=True)

        history = self.model.fit(np.array(self.input_list),
                                 np.array(self.output_list),
                                 epochs=self.coord_train_epochs,
                                 validation_split=self.coord_val_split,
                                 callbacks=[early_stopping_monitor, hist],
                                 verbose=2)


        # Save model weights and delete lock file
        save_to_file(self.coord_model_path, self.model.get_weights())
        os.remove(self.lock_file_path)

        # Save buffer
        self.save_buffer_to_file()



    def store_sensor_values(self, states):
        """
        Stores the provided sensor values in a buffer for training of the coordinate predictor. If the maximum buffer
        buffer size is reached random elements are deleted from the buffer.

        :param states:  Dictionary with states from the simulation
        """

        if self.store_training_data is True:

            # Store sensor values
            input_dataset = []
            for key, value in self.sensor_value_conf.items():
                if value is True:
                    input_dataset.append(states[key])

            self.input_list.append(input_dataset)

            # Get number of workpieces for which coordinates need to be stored
            if self.coord_num_wps is None:
                self.coord_num_wps = len(states["workpiece"])

            # Store coordinates
            output_dataset = []
            for idx in range(self.coord_num_wps):
                output_dataset.append(states["workpiece"][idx]["position.x"])
                output_dataset.append(states["workpiece"][idx]["position.y"])
                output_dataset.append(states["workpiece"][idx]["position.z"])

            self.output_list.append(output_dataset)

            # Delete random element from the buffer if maximum size is exceeded
            if len(self.input_list) > self.coord_buffer_size:
                idx = np.random.randint(self.coord_buffer_size)
                self.input_list.pop(idx)
                self.output_list.pop(idx)


    def update_weights(self):
        if os.path.exists(self.coord_model_path):
            weights = load_from_file(self.coord_model_path)
            self.model.set_weights(weights)
            return True
        else:
            return False


    def load_buffer(self):
        """
        Loads the buffer with training data from a file or creates a new empty one
        """

        buffer = load_from_file(self.coord_buffer_file)
        if buffer is not None:
            self.input_list = buffer["sensor_values"]
            self.output_list = buffer["coord_values"]
        else:
            self.input_list = []
            self.output_list = []



    def save_buffer_to_file(self):
        """
        Saves the current buffer with traning data to a file.
        """

        buffer = {"sensor_values": self.input_list,
                  "coord_values": self.output_list}

        save_to_file(self.coord_buffer_file, buffer)