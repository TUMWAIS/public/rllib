import numpy as np
import itertools
from copy import deepcopy

from rllib.classes.rollout_actors.RolloutActorHERBase import RolloutActorHERBase
from rllib.classes.miscellaneous.rollout_statistics.RolloutStatisticsCalculatorCoordPred import RolloutStatisticsCalculatorCoordPred


class RolloutActorXPPU(RolloutActorHERBase):
    """
    Standard rollout actor for the simulation of the Extended Pick and Place Unit (xPPU).
    The rollout actor supports universal value function approximation and workpiece coordinate prediction.
    """

    def __init__(self, worker_id, params, EnvironmentBuilder, Policy, render=False, force_cpu_use=False):
        super().__init__(worker_id, params, EnvironmentBuilder, Policy)

        # Create an instance of the environment
        if EnvironmentBuilder is not None:
            self.env_builder = EnvironmentBuilder()
            self.env = self.env_builder.create_gym_env(self.params, render)

        # Create coordinate predictor
        if self.params.predict_coordinates is True:
            self.CoordPredictor = self.env_builder.get_coordinate_predictor(worker_id, self.params)
            self.env.set_coord_predictor(self.CoordPredictor)

            if self.worker_id != 0:
                self.CoordPredictor.store_training_data = False


        # Create class for the calculation of rollout statistics
        self.stat_cal = RolloutStatisticsCalculatorCoordPred(self.params.gamma, self.env.goal.max_reward)

        # Initialize policy
        self.policy = Policy(self.params, force_cpu_use)

        # Initialize variables
        self.train_counter = None



    def run(self,
            episode,
            weights,
            policy_params,
            deployment_mode=False,
            rollout_state=None,
            avoid_env_reset=False):
        """
        Method for playing transitions in the environment. The method is modular to allow a simple extension.

        The returned trajectory list has the following structure:
            [
            [obs_1, action_1, obs_next_1, reward_1, done_1, desired_goal_1]
            [obs_2, action_2, obs_next_2, reward_2, done_2, desired_goal_1]
            ]

        :param episode:             Integer representing the current training episode 
        :param weights:             List with latest policy weights
        :param policy_params:       List with parameters for the policy
        :param rollout_state:       List containing the current state variables of the rollout actor (e.g., weights for coordinate prediction)
        :param avoid_env_reset:     Flag indicating whether the environment should be reseted before rollout
        :param deployment_mode:     Flag indicating whether the policy should be rolled out in deployment mode
        :return traj_list:          A List containing transitions for each played trajectory
        :return rollout_info:       A dictionary with additional data on the rollout.
        """
        super().run(episode, weights, policy_params, deployment_mode=deployment_mode, rollout_state=rollout_state)
        self.avoid_env_reset = avoid_env_reset

        num_transitions = 0

        self._prepare_sampling()

        # Play trajectories until a minimum number of transitions has been collected
        while num_transitions < self.params.num_transitions:
            reward, num_steps = self._sample_trajectory(self.policy_params, deploy=self.deployment_mode, avoid_env_reset=self.avoid_env_reset)
            num_transitions += num_steps

            if self.deployment_mode is True:
                break

        self._post_process()

        # Create list of transitions by aggregating trajectories and combining her with non-her data
        trans_list = list(itertools.chain.from_iterable(self.traj_list + self.traj_list_her))

        return trans_list, self.rollout_info



    def _prepare_sampling(self):
        """
        Executes all steps that need to be done before sampling starts.
        """
        self.traj_list = []
        self.traj_list_her = []
        self.traj_info = []
        self.rollout_info = {"traj_info": [],
                             "error_msg": None,
                             "done_msg": None}

        self.stat_cal.reset()

        # Initialize/update network weights
        self.policy.set_weights(self.weights)

        if self.params.predict_coordinates is True:
            self.CoordPredictor.update_weights(self.rollout_state[0])



    def _sample_trajectory(self,
                           policy_params,
                           desired_goal=None,
                           wp_conf=None,
                           position=None,
                           deploy = False,
                           render=None,
                           reset_fn_args=None,
                           avoid_env_reset=False,
                           store_only_successful=False,
                           max_steps=None):
        """
        Samples a single trajectory

        :param policy_params:       List with parameters for the policy
        :param desired_goal:        Optional numpy array overwriting the provided by the environment
        :param wp_conf:             Optional list of workpiece names overwriting the one provided by the environment
        :param position:            Optional position name overwriting the initialization position of the environment
        :param deploy:              Flag indicating whether the policy should be run in deployment mode
        :param render:              Flag to activate rendering
        :param reset_fn_args:       Arguments to be passed to the reset function of the environment
        :param avoid_env_reset:     Flag indicating whether the environment shall be reset before the rollout
        :return total_reward:       Total reward obtained during rollout
        :return num_steps:          Number of environment steps executed during rollout
        """
        num_steps = 0
        total_reward = 0
        trans = []
        trans_goals = []
        trans_info = []
        done_sampling = False

        # Setup coordinate prediction
        if self.params.predict_coordinates is True:

            # Initialize training counter
            if self.train_counter is None:
                self.train_counter = self.episode / self.params.coord_pred_train_freq

            # Train coordinate predictor if first worker
            if int((self.episode + self.worker_id) / self.params.coord_pred_train_freq ) > self.train_counter and self.worker_id == 0 and deploy is False and self.episode < self.params.coord_pred_train_end_episode:
                self.train_counter += 1                                     # Necessary since method is not called in every episode
                self.rollout_state[0] = self.CoordPredictor.train()
                self.rollout_info["rollout_state"] = self.rollout_state

            # Predict coordinates
            if self.episode > self.params.coord_pred_start_episode:
                self.CoordPredictor.use_predicted_coords = True
            else:
                self.CoordPredictor.use_predicted_coords = False

            # Predict on predicted coordinates for deployment
            if deploy is True:
                self.CoordPredictor.prediction_state_from_sim = False
            else:
                self.CoordPredictor.prediction_state_from_sim = True

        # Reset environment
        if avoid_env_reset is False:
            observation, info = self.env.reset(wp_conf, position, render, reset_fn_args)
        else:
            observation, info = self.env.get_observation()

        # Overwrite environment goal, if goal is provided
        if desired_goal is not None:
            self.env.goal.reset(observation["observation"], info, desired_goal)
            observation, info = self.env.get_observation()                         # Get updated observation with new achieved goal

        # Start new trajectory in statistics calculator
        self.stat_cal.start_new_traj(self.env.goal.max_reward)                     # Need to be called after environment and goal reset

        # Get value for initial state
        value_info = self.policy.get_value_estimate(obs=observation["observation"],
                                                    goal=observation["desired_goal"])
        if value_info is not None:
            self.rollout_info.update(value_info)


        # Main sampling loop
        while not done_sampling:

            # Sample action according to the policy
            action = self.policy.run_policy(obs=observation["observation"],
                                            goal=observation["desired_goal"],
                                            policy_params=policy_params,
                                            deploy=deploy)
            #print(self.env.action_space_names[action[0]])

            # Act in environment
            observation_next, reward, done, info = self.env.step(action)

            done_sampling = done
            num_steps += 1
            total_reward += reward

            # Break loop after reaching the maximum number of steps
            if num_steps >= self.params.max_env_steps:
                done_sampling = True
                info["done_msg"] = "Timeout"
                if self.params.done_for_timeout is True:
                    done = True
            elif max_steps is not None and num_steps >= max_steps:
                done_sampling = True


            if "store_transition" not in info or info["store_transition"] is True:

                # Save transition for training (and directly convert datatypes to save memory)
                trans.append([observation["observation"].astype(self.params.transfer_dtype),                    # observation
                              np.array(action).astype(self.params.transfer_dtype),                              # action
                              observation_next["observation"].astype(self.params.transfer_dtype),               # observation_next
                              np.array(reward).astype(self.params.transfer_dtype),                              # reward
                              np.array(done).astype("int8"),                                                    # done
                              np.array(observation["desired_goal"]).astype(self.params.transfer_dtype)          # goal
                             ])

                # Save additional data for her
                trans_goals.append([observation["achieved_goal"],                                               # achieved_goal
                                    observation_next["achieved_goal"],                                          # achieved_goal_next
                                    info                                                                        # info
                                   ])

                # Save info dicts (mostly for debugging)
                trans_info.append(info)


            # Calculate rollout statistics
            self.stat_cal.add_transition(reward, info)

            # Store reward and done message if trajectory has ended
            if done_sampling is True:
                self.rollout_info["traj_info"].append({"reward_msg": info["reward_msg"],
                                                       "done_msg": info["done_msg"],
                                                       "error_msg": info["error_msg"]})

            observation = observation_next


        # Create her samples
        if hasattr(self.params, "her_strategy") is True and self.params.her_strategy is not None and len(trans) > 0:
            self.traj_list_her.append(self.add_her_samples(trans, trans_goals))

        # Add transition list to trajectory list
        self.traj_list.append(trans)
        self.traj_info.append(trans_info)

        return total_reward, num_steps



    def _post_process(self):
        """
        Executes steps after all trajectories have been sampled.
        """

        # Calculate rollout statistics
        self.rollout_info["statistics"] = self.stat_cal.get_statistics()



    def get_rollout_state(self):
        """
        Returns a list with state variables of the rollout actor.
        In this implementation, the list contains the weights of the coordinate predictor.

        :return: List with state variables of the rollout actor
        """
        if self.params.predict_coordinates is True:
            weights = [self.CoordPredictor.model.get_weights()]
        else:
            weights = None

        return weights



    def deploy(self,
               episode,
               weights,
               policy_params,
               deployment_mode=False,
               rollout_state=None,
               avoid_env_reset=False):
        """
        Method for deploying a trained policy

        :param episode:             Integer representing the current training episode 
        :param weights:             List with latest policy weights
        :param policy_params:       List with parameters for the policy
        :param rollout_state:       List containing the current state variables of the rollout actor (e.g., weights for coordinate prediction)
        :param avoid_env_reset:     Flag indicating whether the environment should be reseted before rollout
        :param deployment_mode:     Flag indicating whether the policy should be rolled out in deployment mode
        :return traj_list:          A List containing transitions for each played trajectory
        :return rollout_info:       A dictionary with additional data on the rollout.
        """
        super().deploy(episode, weights, policy_params, deployment_mode=deployment_mode, rollout_state=rollout_state)
        self.avoid_env_reset = avoid_env_reset

        self._prepare_sampling()
        total_reward, num_steps = self._sample_trajectory(self.policy_params, render=True)
        print("Total Reward:", total_reward)
        self.env.render()

