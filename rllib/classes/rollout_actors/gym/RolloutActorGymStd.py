from rllib.classes.rollout_actors.gym.RolloutActorGym import RolloutActorGym
from rllib.functions.parallel import run_parallel


@run_parallel()
class RolloutActorGymStd(RolloutActorGym):
    pass