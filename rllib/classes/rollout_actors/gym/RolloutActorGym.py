import numpy as np
import itertools
from copy import deepcopy

from rllib.classes.rollout_actors.RolloutActorAbstract import RolloutActorAbstract
from rllib.classes.miscellaneous.rollout_statistics.RolloutStatisticsCalculatorBase import RolloutStatisticsCalculatorBase


class RolloutActorGym(RolloutActorAbstract):
    """
    Standard rollout actor for the classic control and Box2D OpenAI Gym environments.
    """

    def __init__(self, worker_id, params, EnvironmentBuilder, Policy, render=False, force_cpu_use=False):
        super().__init__(worker_id, params, EnvironmentBuilder, Policy)

        # Create an instance of the gym environment
        self.env_builder = EnvironmentBuilder()
        self.env = self.env_builder.create_gym_env(self.params, render)

        # Create class for the calculation of rollout statistics
        self.stat_cal = RolloutStatisticsCalculatorBase(self.params.gamma)

        # Initialize policy
        self.policy = Policy(self.params, force_cpu_use)

        # Initialize variables
        self.traj_list = []
        self.rollout_info = {}



    def run(self,
            episode,
            weights,
            policy_params,
            rollout_state=None,
            avoid_env_reset=False,
            deployment_mode=False):
        """
        Method for playing transitions in the environment. The method is modular to allow a simple extension.

        The returned trajectory list has the following structure:
            [
            [obs_1, action_1, obs_next_1, reward_1, done_1, desired_goal_1]
            [obs_2, action_2, obs_next_2, reward_2, done_2, desired_goal_1]
            ]

        :param episode:             Integer representing the current training episode 
        :param weights:             List with latest policy weights
        :param policy_params:       List with parameters for the policy
        :param rollout_state:       List containing the current state variables of the rollout actor (e.g., weights for coordinate prediction)
        :param avoid_env_reset:     Flag indicating whether the environment should be reseted before rollout
        :param deployment_mode:     Flag indicating whether the policy should be rolled out in deployment mode
        :return traj_list:          A List containing transitions for each played trajectory
        :return rollout_info:       A dictionary with additional data on the rollout.
        """
        super().run(episode, weights, policy_params, rollout_state, avoid_env_reset, deployment_mode)

        num_transitions = 0

        self._prepare_sampling()

        # Play trajectories until a minimum number of transitions has been collected
        while num_transitions < self.params.num_transitions:
            reward, num_steps = self._sample_trajectory(self.policy_params, deploy=self.deployment_mode)
            num_transitions += num_steps

            if self.deployment_mode is True:
                break

        self._post_process()

        # Create list of transitions by aggregating trajectories and combining her with non-her data
        trans_list = list(itertools.chain.from_iterable(self.traj_list))

        return trans_list, self.rollout_info



    def _prepare_sampling(self):
        """
        Executes all steps that need to be done before sampling starts.
        """
        self.traj_list = []
        self.rollout_info["traj_info"] = []

        self.stat_cal.reset()

        # Initialize/update network weights
        self.policy.set_weights(self.weights)



    def _sample_trajectory(self,
                           policy_params,
                           deploy=False,
                           render=None):
        """
        Samples a single trajectory

        :param policy_params:   List with parameters for the policy
        :param deploy:          Flag indicating whether the policy should be run in deployment mode
        :param render:          Flag to activate rendering
        :return total_reward:   Total reward obtained during rollout
        :return num_steps:      Number of environment steps executed during rollout
        """
        num_steps = 0
        total_reward = 0
        trans = []
        done_sampling = False

        # Reset environment
        observation = self.env.reset()

        # Start new trajectory in statistics calculator
        self.stat_cal.start_new_traj()

        # Get value for initial state
        value_info = self.policy.get_value_estimate(obs=observation)
        if value_info is not None:
            self.rollout_info.update(value_info)

        # Get maximum number of environment steps
        if self.params.max_env_steps is None:
            max_steps = self.env.spec.max_episode_steps
        else:
            max_steps = self.params.max_env_steps


        # Main sampling loop
        while not done_sampling:

            # Sample action according to the policy
            action = self.policy.run_policy(obs=observation,
                                            policy_params=policy_params,
                                            deploy=deploy)

            # Act in environment
            observation_next, reward, done, info = self.env.step(action[0])

            done_sampling = done
            num_steps += 1
            total_reward += reward

            # Break loop after reaching the maximum number of steps
            if self.params.limit_env_steps is True and num_steps >= max_steps:
                done_sampling = True
                if self.params.done_for_timeout is True:
                    done = True

            # Save transition
            trans.append([observation.astype(self.params.transfer_dtype),       # observation
                          np.array(action).astype(self.params.transfer_dtype),  # action            (Float to also support continious environments)
                          observation_next.astype(self.params.transfer_dtype),  # observation_next
                          np.array(reward).astype(self.params.transfer_dtype),  # reward
                          np.array(done).astype(np.int8),                       # done
                          np.array([])                                          # goal              (not in use)                                                
                         ])

            # Calculate rollout statistics
            self.stat_cal.add_transition(reward)

            # Create trajectory info entry in rollout_info dict
            if done_sampling is True:
                self.rollout_info["traj_info"].append({})

            # Render the last transition
            if render is True:
                self.env.render()
            
            observation = observation_next


        # Add transition list to trajectory list
        self.traj_list.append(trans)

        return total_reward, num_steps



    def _post_process(self):
        """
        Executes steps after all trajectories have been sampled.
        """
        
        # Calculate rollout statistics
        self.rollout_info["statistics"] = self.stat_cal.get_statistics()


    def deploy(self,
               episode,
               weights,
               policy_params,
               rollout_state=None,
               avoid_env_reset=False,
               deployment_mode=False):
        """
        Deploys a trained algorithm and renders the results

        :param episode:             Integer representing the current training episode 
        :param weights:             List with latest policy weights
        :param policy_params:       List with parameters for the policy
        :param rollout_state:       List containing the current state variables of the rollout actor (e.g., weights for coordinate prediction)
        :param avoid_env_reset:     Flag indicating whether the environment should be reseted before rollout
        :param deployment_mode:     Flag indicating whether the policy should be rolled out in deployment mode
        :return traj_list:          A List containing transitions for each played trajectory
        :return rollout_info:       A dictionary with additional data on the rollout.
        """
        super().deploy(episode, weights, policy_params, rollout_state, avoid_env_reset, deployment_mode)

        self._prepare_sampling()
        self._sample_trajectory(self.policy_params, deploy=self.deployment_mode, render=True)