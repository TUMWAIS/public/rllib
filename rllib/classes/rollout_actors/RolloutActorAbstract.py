from abc import ABC, abstractmethod
from copy import deepcopy

from rllib.functions.seeding import non_gpu_seeding
from rllib.functions.tensorflow import initialize_tf


class RolloutActorAbstract(ABC):
    """
    Abstract base class for rollout actors

    """
    @abstractmethod
    def __init__(self, worker_id, params, EnvironmentBuilder, Policy, *args, **kwargs):
        self.worker_id = worker_id
        self.params = deepcopy(params)

        # Initialize tensorflow
        initialize_tf(enable_xla=self.params.enable_xla)

        # Set random seeds
        if params.fixed_seed is not None:
            non_gpu_seeding(params.fixed_seed + worker_id)



    @abstractmethod
    def run(self,
            episode,
            weights,
            policy_params,
            *args,
            deployment_mode=False,
            rollout_state=None,
            **kwargs):
        """
        Method for playing transitions in the environment. The method is modular to allow a simple extension.

        The returned trajectory list need to has the following structure:
            [
            [obs_1, action_1, obs_next_1, reward_1, done_1, desired_goal_1]
            [obs_2, action_2, obs_next_2, reward_2, done_2, desired_goal_1]
            ]

        :param episode:             Integer representing the current training episode 
        :param weights:             List with latest policy weights
        :param policy_params:       List with parameters for the policy
        :param rollout_state:       List containing the current state variables of the rollout actor (e.g., weights for coordinate prediction)
        :param avoid_env_reset:     Flag indicating whether the environment should be reseted before rollout
        :param deployment_mode:     Flag indicating whether the policy should be rolled out in deployment mode
        :return traj_list:          A List containing transitions for each played trajectory
        :return rollout_info:       A dictionary with additional data on the rollout.
        """
        self.episode = episode
        self.weights = weights
        self.policy_params = policy_params
        self.rollout_state = rollout_state
        self.deployment_mode = deployment_mode



    def deploy(self,
               episode,
               weights,
               policy_params,
               *args,
               deployment_mode=False,
               rollout_state=None,
               **kwargs):
        """
        Abstract method for deploying a trained policy

        :param episode:             Integer representing the current training episode 
        :param weights:             List with latest policy weights
        :param policy_params:       List with parameters for the policy
        :param rollout_state:       List containing the current state variables of the rollout actor (e.g., weights for coordinate prediction)
        :param avoid_env_reset:     Flag indicating whether the environment should be reseted before rollout
        :param deployment_mode:     Flag indicating whether the policy should be rolled out in deployment mode
        :return traj_list:          A List containing transitions for each played trajectory
        :return rollout_info:       A dictionary with additional data on the rollout.
        """
        self.episode = episode
        self.weights = weights
        self.policy_params = policy_params
        self.deployment_mode = deployment_mode
        self.rollout_state=rollout_state



    def get_rollout_state(self, *args, **kwargs):
        """
        Returns a list with state variables of the rollout actor.
        In this implementation, the list contains the weights of the coordinate predictor.

        :return: List with state variables of the rollout actor
        """
        pass