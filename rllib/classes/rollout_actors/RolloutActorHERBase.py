import numpy as np

from rllib.classes.rollout_actors.RolloutActorAbstract import RolloutActorAbstract
from rllib.classes.errors.ConfigurationError import ConfigurationError


class RolloutActorHERBase(RolloutActorAbstract):
    """
    Extension of the abstract base class for rollout actors with HER
    """

    def add_her_samples(self, trans, trans_goals):
        """
        Creates HER transitions for a provided trajectory

        :param trans:       List of transitions to create HER samples for (obs, action, obs_next, reward, done, desired_goal)
        :param traj_data:   List of additional transition data (prev_achieved_goal, achieved_goal, info)
        :return:            List of HER transitions (obs, action, obs_next, reward, done, desired_goal)
        """
        her_data = []

        # Cycle through all transitions
        for i in range(len(trans)-1):
            for k in range(self.params.num_her_samples):
                # Sample goal according to the selected strategy
                if self.params.her_strategy == "final":
                    idx = -1                                    # Last transition in trajectory
                elif self.params.her_strategy == "future":
                    idx = np.random.randint(i, len(trans))      # Any trajectory between current and last
                elif self.params.her_strategy == "episode":
                    idx = np.random.randint(len(trans))         # Any trajectory
                elif self.params.her_strategy == "next":
                    idx = i                                     # Any trajectory
                else:
                    raise ConfigurationError(self.params.her_strategy, "HER strategy not found")
                goal = trans_goals[idx][1]    # Set goal to achieved_goal of respective transition

                # Recalculate reward based on new goal
                reward, done, info = self.env.goal.compute_reward(trans_goals[i][0],    # prev_achieved_goal
                                                                  trans_goals[i][1],    # achieved_goal
                                                                  goal,                 # desired_goal
                                                                  trans_goals[i][2])    # info

                # Store her transition (and convert new variables)
                her_data.append([trans[i][0],                                           # obs
                                 trans[i][1],                                           # action
                                 trans[i][2],                                           # obs_next
                                 np.array(reward).astype(np.float32),                   # reward
                                 np.array(done).astype(np.int32),                       # done
                                 np.array(goal).astype(np.float32)                      # desired goal
                                ])


        return her_data