from rllib.classes.errors.ErrorBase import ErrorBase


class ConfigurationError(ErrorBase):
    """
    Exception raised for errors in the configuration file
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message