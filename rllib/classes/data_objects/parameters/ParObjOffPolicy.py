import os
import psutil

from rllib.classes.data_objects.parameters.ParObjBase import ParObjBase

class ParObjOffPolicy(ParObjBase):
    """
    Object with configuration parameters for the rainbow DQN algorithm

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Mandatory parameters
        self.num_q_updates = self.parameter["training"]["num_q_updates"]       # Number of Q network updates per episode

        # Batch size for updating the Q-network
        if "batch_size_schedule" in self.parameter["training"] and self.parameter["training"]["batch_size_schedule"] is not None:
            self.batch_size_schedule = self.parameter["training"]["batch_size_schedule"]
            self.batch_size_boundaries = self.parameter["training"]["batch_size_boundaries"]        # Boundaries for piecewise linear decay
            self.batch_size_values = self.parameter["training"]["batch_size_values"]                # Batch size for piecewise linear decay
            self.batch_size = self.batch_size_values[0]                                             # Initial batch size
        else:
            self.batch_size_schedule = None
            self.batch_size = self.parameter["training"]["batch_size"]

        # Parameters for target network update
        if "soft_target_update" in self.parameter["training"]:
            self.soft_target_update = self.parameter["training"]["soft_target_update"]
        else:
            self.soft_target_update = False

        if self.soft_target_update is True:
            self.target_update_steps = self.parameter["training"]["target_update_steps"]
            self.tau = self.parameter["training"]["tau"]
        else:
            self.soft_target_update = False
            self.target_update_episodes = self.parameter["training"]["target_update_episodes"]


        # ----------------------------------------------------
        # Extensions
        # ----------------------------------------------------

        # Use Hindsight Experience Replay
        if "her_strategy" in self.parameter["extensions"]:
            self.her_strategy = self.parameter["extensions"]["her_strategy"]
        else:
            self.her_strategy = None

        if "num_her_samples" in self.parameter["extensions"]:
            self.num_her_samples = self.parameter["extensions"]["num_her_samples"]   # Number of HER samples
        else:
            self.num_her_samples = 1


        # ----------------------------------------------------
        # Exploration
        # ----------------------------------------------------

        # Mandatory parameters
        self.epsilon = self.parameter["exploration"]["epsilon_start"]  # Starting probability of random actions for exploration

        # Exploration schedule (null, Exponential, or PiecewiseLinear)
        if "exploration_schedule" in self.parameter["exploration"]:
            self.exploration_schedule = self.parameter["exploration"]["exploration_schedule"]

            if self.exploration_schedule == "Exponential":
                self.epsilon_decay = self.parameter["exploration"]["epsilon_decay"]             # Rate of decay for epsilon
                self.epsilon_min = self.parameter["exploration"]["epsilon_min"]                 # Lowest probability of random actions for exploration
            elif self.exploration_schedule == "PiecewiseLinear":
                self.epsilon_boundaries = self.parameter["exploration"]["epsilon_boundaries"]   # Boundaries for piecewise linear schedule
                self.epsilon_values = self.parameter["exploration"]["epsilon_values"]           # Values for piecewise linear schedule
                self.epsilon = self.epsilon_values[0]

        else:
            self.exploration_schedule = None


        # ----------------------------------------------------
        # Experience replay
        # ----------------------------------------------------

        # Mandatory parameters
        self.max_replay_buffer = self.parameter["experience_replay"]["max_replay_buffer"]         # Maximum size of replay buffer

        # Number of batches in buffer before training starts
        if "num_batches_in_buffer" in self.parameter["experience_replay"]:
            self.num_batches_in_buffer = self.parameter["experience_replay"]["num_batches_in_buffer"]
        else:
            self.num_batches_in_buffer = 1

        if "prefill_buffer_randomly" in self.parameter["experience_replay"]:
            self.prefill_buffer_randomly = self.parameter["experience_replay"]["prefill_buffer_randomly"]
        else:
            self.prefill_buffer_randomly = True

        if "compress_replay_buffer" in self.parameter["experience_replay"]:
            self.compress_replay_buffer = self.parameter["experience_replay"]["compress_replay_buffer"]
        else:
            self.compress_replay_buffer = True


        # ----------------------------------------------------
        # Other
        # ----------------------------------------------------

        # Number of main threads
        if "num_main_threads" in self.parameter["other"]:
            self.num_main_threads = self.parameter["other"]["num_main_threads"]
        else:
            self.num_main_threads = 1

        # Number of replay buffer actors
        if self.run_in_parallel is False:
            self.num_replay_buffer_workers = 1
            self.num_replay_buffer_threads = 0
        elif "num_replay_buffer_workers" in self.parameter["other"]:
            self.num_replay_buffer_workers = self.parameter["other"]["num_replay_buffer_workers"]
            self.num_replay_buffer_threads = self.num_replay_buffer_workers
        else:
            self.num_replay_buffer_workers = 1
            self.num_replay_buffer_threads = self.num_replay_buffer_workers

        # Number of training actors
        if self.run_in_parallel is False:
            self.num_training_workers = 1
            self.num_training_threads = 0
        elif "num_training_workers" in self.parameter["other"]:
            self.num_training_workers = self.parameter["other"]["num_training_workers"]
            self.num_training_threads = self.num_training_workers
        else:
            self.num_training_workers = 0
            self.num_training_threads = self.num_training_workers

        # Number of rollout actors
        try:
            num_cores = len(psutil.Process().cpu_affinity())
        except:
            num_cores = 1
        if self.run_in_parallel is False:
            self.num_sim_workers = 1
            self.num_sim_threads = 0
        elif "num_sim_workers" in self.parameter["other"] and self.parameter["other"]["num_sim_workers"] is not None:
            self.num_sim_workers = self.parameter["other"]["num_sim_workers"]
            self.num_sim_threads = self.num_sim_workers
        else:
            self.num_sim_workers = num_cores - self.num_main_threads - self.num_replay_buffer_threads
            self.num_sim_threads = self.num_sim_workers


        # ----------------------------------------------------
        # Paths (only auto-generated)
        # ----------------------------------------------------

        self.replay_buffer_dir = os.path.dirname(parameter_file) + "/replay_buffer/"
        self.weights_file = os.path.dirname(parameter_file) + "/model/weights"