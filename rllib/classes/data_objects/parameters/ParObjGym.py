from rllib.classes.data_objects.parameters.ParObjBase import ParObjBase


class ParObjGym(ParObjBase):
    """
    Parammeter object for the classic control and Box2D gym environments

    """
    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Environment
        # ----------------------------------------------------

        # Mandatory parameters
        self.gym_env_name = self.parameter["environment"]["gym_env_name"]

        # Set done flag for timeouts
        if "done_for_timeout" in self.parameter["environment"]:
            self.done_for_timeout = self.parameter["environment"]["done_for_timeout"]
        else:
            self.done_for_timeout = True

        # Initialize goal scaling factor
        if "limit_env_steps" in self.parameter["environment"]:
            self.limit_env_steps = self.parameter["environment"]["limit_env_steps"]
            self.max_env_steps = self.parameter["environment"]["max_env_steps"]
        else:
            self.limit_env_steps = True
            self.max_env_steps = None