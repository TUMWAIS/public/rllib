from rllib.classes.data_objects.parameters.ParObjOnPolicy import ParObjOnPolicy


class ParObjA2C(ParObjOnPolicy):
    """
    Object with configuration parameters for the Actor Critic Algorithm

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Set algorithm type
        self.algorithm = "A2C"

        # Lambda parameter for General Advantage Estimation
        if "lambda" in self.parameter["training"]:
            self.lambda_ = self.parameter["training"]["lambda"]
        else:
            self.lambda_ = 1

        # Discount Factor for critic loss
        if "critic_discount" in self.parameter["training"]:
            self.critic_discount = self.parameter["training"]["critic_discount"]
        else:
            self.critic_discount = 1

        # Coefficient for entropy term in loss function
        if "entropy_beta" in self.parameter["training"]:
            self.entropy_beta = self.parameter["training"]["entropy_beta"]
        else:
            self.entropy_beta = 0
            
        # Use single aggregated loss function instead of a separate loss for each head
        if "agreggrate_loss" in self.parameter["training"]:
            self.agreggrate_loss = self.parameter["training"]["agreggrate_loss"]
        else:
            self.agreggrate_loss = False