import os
import psutil
import math

from rllib.classes.data_objects.parameters.ParObjBase import ParObjBase


class ParObjOnPolicy(ParObjBase):
    """
    Object with configuration parameters for Policy Gradient type algorithms

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Set loss function
        self.loss_function = None   # Only custom loss

        # Mandatory parameters
        self.batch_size = self.parameter["training"]["batch_size"]

        # Psi normalization
        if "psi_scaling" in self.parameter["training"]:
            self.psi_scaling = self.parameter["training"]["psi_scaling"]
        else:
            self.psi_scaling = False

        # Use Baseline
        if "baseline" in self.parameter["training"]:
            self.baseline = self.parameter["training"]["baseline"]
        else:
            self.baseline = False

        # ----------------------------------------------------
        # Parallel execution
        # ----------------------------------------------------

        # Number of training actors
        if self.run_in_parallel is False:
            self.num_training_workers = 1
            self.num_training_threads = 0
        if "num_training_workers" in self.parameter["other"]:
            self.num_training_workers = self.parameter["other"]["num_training_workers"]
            self.num_training_threads = self.num_training_workers
        else:
            self.num_training_workers = 1
            self.num_training_threads = self.num_training_workers

        # Number of rollout actors
        try:
            num_cores = len(psutil.Process().cpu_affinity())
        except:
            num_cores = 1
        if self.run_in_parallel is False:
            self.num_sim_workers = 1
            self.num_sim_threads = 0
        elif "num_sim_workers" in self.parameter["other"] and self.parameter["other"]["num_sim_workers"] is not None:
            self.num_sim_workers = self.parameter["other"]["num_sim_workers"]
            self.num_sim_threads = self.num_sim_workers
        else:
            self.num_sim_workers = num_cores    # Main threat is waiting while workers are running
            self.num_sim_threads = self.num_sim_workers

        # Set number of minimum transitions per worker based on the simulation worker number
        self.num_transitions = max(int(math.ceil(self.batch_size / self.num_sim_workers)), self.parameter["training"]["num_transitions"])

        # ----------------------------------------------------
        # Paths (only auto-generated)
        # ----------------------------------------------------

        self.weights_file = os.path.dirname(parameter_file) + "/model/weights"