from rllib.classes.data_objects.parameters.ParObjOnPolicy import ParObjOnPolicy


class ParObjVPG(ParObjOnPolicy):
    """
    Object with configuration parameters for the Vanilla Policy Gradient Algorithm

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Set algorithm type
        self.algorithm = "VPG"