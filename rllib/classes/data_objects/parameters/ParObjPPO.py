from rllib.classes.data_objects.parameters.ParObjA2C import ParObjA2C


class ParObjPPO(ParObjA2C):
    """
    Object with configuration parameters for the A2C Algorithm

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Set algorithm type
        self.algorithm = "PPO"
        self.num_policy_updates = self.parameter["training"]["num_policy_updates"]
        self.minibatch_size = self.parameter["training"]["minibatch_size"]
        self.clip_val = self.parameter["training"]["clip_val"]