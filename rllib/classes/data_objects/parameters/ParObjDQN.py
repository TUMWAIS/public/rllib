from rllib.classes.data_objects.parameters.ParObjOffPolicy import ParObjOffPolicy

class ParObjDQN(ParObjOffPolicy):
    """
    Object with configuration parameters for the rainbow DQN algorithm

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Set algorithm type
        self.algorithm = "DQN"

        # ----------------------------------------------------
        # Network
        # ----------------------------------------------------

        # Loss function
        if "loss_function" in self.parameter["network"]:
            self.loss_function = self.parameter["network"]["loss_function"]
        else:
            self.loss_function = "MSE"

        # egreedy action scheme
        if "egreedy_per_action" in self.parameter["network"]:
            self.egreedy_per_action = self.parameter["network"]["egreedy_per_action"]
        else:
            self.egreedy_per_action = True

        # ----------------------------------------------------
        # Extensions
        # ----------------------------------------------------

        # Use double q learning
        if "double_q_learning" in self.parameter["extensions"]:
            self.double_q_learning = self.parameter["extensions"]["double_q_learning"]
        else:
            self.double_q_learning = False

        # Use duelling q networks
        if "duelling_q_networks" in self.parameter["extensions"]:
            self.duelling_q_networks = self.parameter["extensions"]["duelling_q_networks"]
        else:
            self.duelling_q_networks = False

        # Use Prioritized Experience Replay
        if "PER" in self.parameter["extensions"]:
            self.PER = self.parameter["extensions"]["PER"]
            self.beta = self.parameter["extensions"]["beta"]                         # Initial importance sampling exponent
            self.beta_increase = self.parameter["extensions"]["beta_increase"]       # Increase of impotance sampling exponent per iteration
            self.epsilon_per = self.parameter["extensions"]["epsilon_per"]           # Bias to avoid probabilities of zero
            self.alpha = self.parameter["extensions"]["alpha"]                       # Prioritization exponent (alpha = 0 correspinds to uniform sampling)
            self.td_error_clip = self.parameter["extensions"]["td_error_clip"]       # Clipping for TD errors
        else:
            self.PER = False
