from rllib.classes.data_objects.parameters.ParObjOffPolicy import ParObjOffPolicy


class ParObjSAC(ParObjOffPolicy):
    """
    Object with configuration parameters for the DQN Algorithm

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Mandatory parameters
        self.algorithm = "SAC"
        self.temp_alpha = self.parameter["training"]["temp_alpha"]



        # ----------------------------------------------------
        # Network
        # ----------------------------------------------------

        # Parameters for actor
        if "clip_actor_log_std" in self.parameter["network"]:
            self.clip_actor_log_std = self.parameter["network"]["clip_actor_log_std"]
        else:
            self.clip_actor_log_std = [-20, 2]

        # Offset of critic network to avoid initialization issues
        if "init_offset" in self.parameter["network"]:
            self.init_offset = self.parameter["network"]["init_offset"]
        else:
            self.init_offset = None

        # Clip Q-value
        if "clip_q_value" in self.parameter["network"]:
            self.clip_q_value = self.parameter["network"]["clip_q_value"]
        else:
            self.clip_q_value = None

        # Multiply actions with factor (standard -1 to 1)
        if "action_factor" in self.parameter["network"]:
            self.action_factor = self.parameter["network"]["action_factor"]
        else:
            self.action_factor = 1

        # Shift actions (standard -1 to 1)
        if "action_shift" in self.parameter["network"]:
            self.action_shift = self.parameter["network"]["action_shift"]
        else:
            self.action_shift = 0

        # Activate optimization of alpha
        if "optimize_alpha_temp" in self.parameter["training"]:
            self.optimize_alpha_temp = self.parameter["training"]["optimize_alpha_temp"]
        else:
            self.optimize_alpha_temp = False

        if self.optimize_alpha_temp is True:
            self.lr_alpha_temp = self.parameter["training"]["lr_alpha_temp"]
            self.target_entropy = self.parameter["training"]["target_entropy"]
            self.temp_alpha_max = self.parameter["training"]["temp_alpha_max"]
