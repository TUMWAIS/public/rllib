import os
import numpy as np

from rllib.classes.data_objects.parameters.ParObjBase import ParObjBase
from rllib.functions.logging import get_module_commit

class ParObjXPPU(ParObjBase):
    """
    Object with configuration parameters for the XPPU environment

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Environment
        # ----------------------------------------------------

        # Mandatory Parameters
        self.num_wps = self.parameter["environment"]["num_wps"]                                                               # Number of workpieces (maximum: 5)
        self.num_variants = self.parameter["environment"]["num_variants"]                                                     # Number of different workpiece combinations to choose from
        self.normalize_statespace = self.parameter["environment"]["normalize_statespace"]                                     # Activate normalization of state space (esp. coordinates)
        self.max_env_steps = self.parameter["environment"]["max_env_steps"]                                                   # Max. number of environment steps before timeout

        self.action_space_type = self.parameter["environment"]["action_space_type"]                                           # Native high-level functions or custom actions defined in /env/actionspace.py
        if self.parameter["environment"]["action_space_conf_path"] is not None:
            self.action_space_conf_path = os.path.join(self.root_path, self.parameter["environment"]["action_space_conf_path"])   # Name of JSON with action space configuration
        else:
            self.action_space_conf_path = None
        self.state_space_conf_path = os.path.join(self.root_path, self.parameter["environment"]["state_space_conf_path"])     # Name of JSON with state space configuration
        self.goal_class = self.parameter["environment"]["goal_class"]                                                         # Name of Goal class


        # ----------------------------------------------------
        # Environment
        # ----------------------------------------------------

        # Set done flag for timeouts
        if "done_for_timeout" in self.parameter["environment"]:
            self.done_for_timeout = self.parameter["environment"]["done_for_timeout"]
        else:
            self.done_for_timeout = True

        # List of workpiece positions for initialization (single workpiece configuration only)
        if "starting_wp_pos" in self.parameter["environment"]:
            self.starting_wp_pos = self.parameter["environment"]["starting_wp_pos"]
        else:
            self.starting_wp_pos = None

        # List with number of neurons per layer or null
        if "network_wp_layers" in self.parameter["network"]:
            self.network_wp_layers = self.parameter["network"]["network_wp_layers"]
        else:
            self.network_wp_layers = None

        # Sample random seed for workpiece configuration (ensures that each worker gets the same initial pool)
        rand_num_gen = np.random.RandomState(self.fixed_seed)   # Own seeded generator, since overall seeding happens later
        self.wp_pool_seed = rand_num_gen.randint(2**31-2)       # Draw from any possible integer

        # Initialize crane turn_only_extended flag
        if "crane_turn_only_extended" in self.parameter["environment"]:
            self.crane_turn_only_extended = self.parameter["environment"]["crane_turn_only_extended"]
        else:
            self.crane_turn_only_extended = True

        if "crane_move_at_pickpoint" in self.parameter["environment"]:
            self.crane_move_at_pickpoint = self.parameter["environment"]["crane_move_at_pickpoint"]
        else:
            self.crane_move_at_pickpoint = True

        # Initialize parameters for coordinate prediction
        if "coord_prediction" in self.parameter:
            self.predict_coordinates = self.parameter["coord_prediction"]["predict_coordinates"]

            if self.predict_coordinates is True:
                self.coord_sensor_value_file = os.path.join(self.root_path, self.parameter["coord_prediction"]["coord_sensor_value_file"])
                self.coord_buffer_size = self.parameter["coord_prediction"]["coord_buffer_size"]
                self.coord_prediction_model = self.parameter["coord_prediction"]["coord_prediction_model"]
                self.coord_batch_size = self.parameter["coord_prediction"]["coord_batch_size"]
                self.coord_num_epochs = self.parameter["coord_prediction"]["coord_num_epochs"]
                self.coord_learning_rate = self.parameter["coord_prediction"]["coord_learning_rate"]
                self.coord_pred_train_freq = self.parameter["coord_prediction"]["coord_pred_train_freq"]
                self.coord_pred_start_episode = self.parameter["coord_prediction"]["coord_pred_start_episode"]

                if "coord_round_values" in self.parameter["coord_prediction"]:
                    self.coord_round_values = self.parameter["coord_prediction"]["coord_round_values"]
                else:
                    self.coord_round_values = None

                if "coord_pred_train_end_episode" in self.parameter["coord_prediction"] and self.parameter["coord_prediction"]["coord_pred_train_end_episode"] is not None:
                    self.coord_pred_train_end_episode = self.parameter["coord_prediction"]["coord_pred_train_end_episode"]
                else:
                    self.coord_pred_train_end_episode = self.num_episodes

                if "coord_predictor_type" in self.parameter["coord_prediction"]:
                    self.coord_predictor_type = self.parameter["coord_prediction"]["coord_predictor_type"]
                else:
                    self.coord_predictor_type = "stateless"

                if "coord_action_value_file" in self.parameter["coord_prediction"] and self.parameter["coord_prediction"]["coord_action_value_file"] is not None:
                    self.coord_action_value_file = os.path.join(self.root_path, self.parameter["coord_prediction"]["coord_action_value_file"])
                else:
                    self.coord_action_value_file = None

        else:
            self.predict_coordinates = False


        # Play several trajectories in a row before reseting the environment
        if "reset_env_after_traj" in self.parameter["environment"]:
            self.reset_env_after_traj = self.parameter["environment"]["reset_env_after_traj"]
        else:
            self.reset_env_after_traj = True

        if "store_only_successful" in self.parameter["environment"]:
            self.store_only_successful = self.parameter["environment"]["store_only_successful"]
        else:
            self.store_only_successful = False

        # Use xppusim instead of OPC/UA in xppuphy
        if "debug_with_xppusim" in self.parameter["environment"]:
            self.debug_with_xppusim = self.parameter["environment"]["debug_with_xppusim"]
        else:
            self.debug_with_xppusim = False

        # PLC ip address for xppuphy environment
        if "plc_ip" in self.parameter["environment"]:
            self.plc_ip = self.parameter["environment"]["plc_ip"]
        else:
            self.plc_ip = "192.168.82.11"

        # PLC port for xppuphy environment
        if "plc_port" in self.parameter["environment"]:
            self.plc_port = self.parameter["environment"]["plc_port"]
        else:
            self.plc_port = 4840

        # Cycle time for xppuphy environment
        if "cycle_time" in self.parameter["environment"]:
            self.cycle_time = self.parameter["environment"]["cycle_time"]
        else:
            self.cycle_time = 60


        # ----------------------------------------------------
        # Paths (only auto-generated)
        # ----------------------------------------------------

        self.render_dir = os.path.join(os.path.dirname(os.path.abspath(parameter_file)) + "/renders/")


        # ----------------------------------------------------
        # Git commits (only auto-generated)
        # ----------------------------------------------------

        self.xppusim_commit, self.xppusim_commit_msg = get_module_commit("xppusim")