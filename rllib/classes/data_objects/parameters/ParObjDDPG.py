from rllib.classes.data_objects.parameters.ParObjOffPolicy import ParObjOffPolicy


class ParObjDDPG(ParObjOffPolicy):
    """
    Object with configuration parameters for the DQN Algorithm

    """

    def __init__(self, parameter_file):
        super().__init__(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Mandatory parameters
        self.algorithm = "DDPG"


        # ----------------------------------------------------
        # Network
        # ----------------------------------------------------

        # Loss function
        if "loss_function" in self.parameter["network"]:
            self.loss_function = self.parameter["network"]["loss_function"]
        else:
            self.loss_function = "MSE"

        # Parameters for actor
        if "clip_actor_log_std" in self.parameter["network"]:
            self.clip_actor_log_std = self.parameter["network"]["clip_actor_log_std"]
        else:
            self.clip_actor_log_std = [-20, 2]

        # Offset of critic network to avoid initialization issues
        if "init_offset" in self.parameter["network"]:
            self.init_offset = self.parameter["network"]["init_offset"]
        else:
            self.init_offset = None

        # Clip Q-value
        if "clip_q_value" in self.parameter["network"]:
            self.clip_q_value = self.parameter["network"]["clip_q_value"]
        else:
            self.clip_q_value = None

        # Multiply actions with factor (standard -1 to 1)
        if "action_bounds" in self.parameter["network"]:
            self.action_bounds = self.parameter["network"]["action_bounds"]
        else:
            self.action_bounds = None



        # ----------------------------------------------------
        # Extensions
        # ----------------------------------------------------

        # Use Prioritized Experience Replay
        if "PER" in self.parameter["extensions"]:
            self.PER = self.parameter["extensions"]["PER"]
            self.beta = self.parameter["extensions"]["beta"]                         # Initial importance sampling exponent
            self.beta_increase = self.parameter["extensions"]["beta_increase"]       # Increase of impotance sampling exponent per iteration
            self.epsilon_per = self.parameter["extensions"]["epsilon_per"]           # Bias to avoid probabilities of zero
            self.alpha = self.parameter["extensions"]["alpha"]                       # Prioritization exponent (alpha = 0 correspinds to uniform sampling)
            self.td_error_clip = self.parameter["extensions"]["td_error_clip"]       # Clipping for TD errors
        else:
            self.PER = False