from abc import ABC
import os
import psutil
import json
import __main__

from rllib.functions.parallel import initialize_parallel_exec
from rllib.functions.logging import get_git_commit_from_dir


class ParObjBase(ABC):
    """
    Base class for object containing all configuration parameters

    """

    def __init__(self, parameter_file):
        self.root_path = os.path.dirname(os.path.abspath(__main__.__file__))

        self.parameter = self.load_parameter(parameter_file)

        # ----------------------------------------------------
        # Training
        # ----------------------------------------------------

        # Mandatory parameters
        self.num_episodes = self.parameter["training"]["num_episodes"]                     # Number of training episodes
        self.num_transitions = self.parameter["training"]["num_transitions"]               # Minimum number of transition samples per episode
        self.learning_rate_initial = self.parameter["training"]["learning_rate_initial"]   # Initial learning rate

        # Discount rate for rewards
        if "gamma" in self.parameter["training"]:
            self.gamma = self.parameter["training"]["gamma"]
        else:
            self.gamma = 1

        # Schedule for learning rate decay
        if "learning_rate_schedule" in self.parameter["training"]:
            self.learning_rate_schedule = self.parameter["training"]["learning_rate_schedule"]

            if self.learning_rate_schedule == "ExponentialDecay":
                self.learning_rate_min = self.parameter["training"]["learning_rate_min"]                    # Minimum learning rate
                self.learning_rate_decay_rate = self.parameter["training"]["learning_rate_decay_rate"]      # Rate for exponential decay
            if self.learning_rate_schedule == "PiecewiseLinear":
                self.learning_rate_boundaries = self.parameter["training"]["learning_rate_boundaries"]      # Boundaries for piecewise constant decay
                self.learning_rate_values = self.parameter["training"]["learning_rate_values"]              # Learning rate for piecewise constant decay
                self.learning_rate_initial = self.learning_rate_values[0]
        else:
            self.learning_rate_schedule = None

        # Enable XLA
        if "enable_xla" in self.parameter["training"]:
            self.enable_xla = self.parameter["training"]["enable_xla"]
        else:
            self.enable_xla = False

        # ----------------------------------------------------
        # Network
        # ----------------------------------------------------

        # Mandatory parameters
        self.network_main_layers = self.parameter["network"]["network_main_layers"]    # List with number of neurons per layer

        # Activation function for hidden layer
        if "hidden_layer_activation" in self.parameter["network"]:
            self.hidden_layer_activation = self.parameter["network"]["hidden_layer_activation"]
        else:
            self.hidden_layer_activation = "relu"

        # Set floating point precision of network inputs, outputs and weights (only for transfer)
        if "transfer_dtype" in self.parameter["network"]:
            self.transfer_dtype = self.parameter["network"]["transfer_dtype"]
        else:
            self.transfer_dtype = "float32"

        # Activate mixed-precision training with FP16
        if "fp16_mixed_precision" in self.parameter["network"]:
            self.fp16_mixed_precision = self.parameter["network"]["fp16_mixed_precision"]
        else:
            self.fp16_mixed_precision = False

        # Scale loss to improve numerical stability
        if "scale_loss" in self.parameter["network"]:
            self.scale_loss = self.parameter["network"]["scale_loss"]
        else:
            self.scale_loss = False

        # ----------------------------------------------------
        # Extensions
        # ----------------------------------------------------

        # Use Universal Value Function Approximation
        if "uvfa" in self.parameter["extensions"]:
            self.uvfa = self.parameter["extensions"]["uvfa"]
        else:
            self.uvfa = False

        # Clip gradients "None, "ByValue" or "ByNorm"
        if "gradient_clipping" in self.parameter["extensions"]:
            self.gradient_clipping = ["gradient_clipping"]
            self.clip_value_min = self.parameter["extensions"]["clip_value_min"]     # Minimum gradient value (only in case of ByValue)
            self.clip_value_max = self.parameter["extensions"]["clip_value_max"]     # Maximum gradient value
        else:
            self.gradient_clipping = None

        # ----------------------------------------------------
        # Exploration
        # ----------------------------------------------------

        # Exploration bonus
        if "exploration" in self.parameter:
            if "exploration_bonus" in self.parameter["exploration"]:
                self.exploration_bonus = self.parameter["exploration"]["exploration_bonus"]

                if self.exploration_bonus is True:
                    self.hashing_granularity = self.parameter["exploration"]["hashing_granularity"]      # Hashing granularity for count-based exploration
                    self.bonus_coefficient = self.parameter["exploration"]["bonus_coefficient"]          # Coefficient for exploration bonus
            else:
                self.exploration_bonus = False
        else:
            self.exploration_bonus = False

        # ----------------------------------------------------
        # Logging
        # ----------------------------------------------------

        # Number of episodes after which the network weights and learning results are aved
        if "history_store_rate" in self.parameter["logging"]:
            self.history_store_rate = self.parameter["logging"]["history_store_rate"]
        else:
            self.history_store_rate = 250

        # Maximum number of history entries per file (not exactly matched)
        if "history_max_file_size" in self.parameter["logging"]:
            self.history_max_file_size = self.parameter["logging"]["history_max_file_size"]
        else:
            self.history_max_file_size = 10000

        # Activate timing for episodes
        if "timeit" in self.parameter["logging"]:
            self.timeit = self.parameter["logging"]["timeit"]
        else:
            self.timeit = True

        # Restore latest training state (if available)
        if "restore" in self.parameter["logging"]:
            self.restore = self.parameter["logging"]["restore"]
        else:
            self.restore = False

        # Print Warning and wait before deleting old files
        if "warn_before_deletion" in self.parameter["logging"]:
            self.warn_before_deletion = self.parameter["logging"]["warn_before_deletion"]
        else:
            self.warn_before_deletion = True

        # Plot training state if path to a plot configuration file is provided
        if "plot_conf_path" in self.parameter["logging"] and self.parameter["logging"]["plot_conf_path"] is not None:
            self.plot_conf_path = os.path.join(self.root_path, self.parameter["logging"]["plot_conf_path"])
        else:
            self.plot_conf_path = None


        # ----------------------------------------------------
        # Weights and Biases
        # ----------------------------------------------------

        if "wandb" in self.parameter:
            self.wandb_logging = self.parameter["wandb"]["wandb_logging"]
            self.wandb_key = self.parameter["wandb"]["wandb_key"]
            self.wandb_entity = self.parameter["wandb"]["wandb_entity"]

            if "wandb_run_name" in self.parameter["wandb"]:
                self.wandb_run_name = self.parameter["wandb"]["wandb_run_name"]
            else:
                self.wandb_run_name = None

            if "wandb_run_notes" in self.parameter["wandb"]:
                self.wandb_run_notes = self.parameter["wandb"]["wandb_run_notes"]
            else:
                self.wandb_run_notes = None

            if "wandb_project" in self.parameter["wandb"]:
                self.wandb_project = self.parameter["wandb"]["wandb_project"]
            else:
                self.wandb_project = None

            if "wandb_group" in self.parameter["wandb"]:
                self.wandb_group = self.parameter["wandb"]["wandb_group"]
            else:
                self.wandb_group = None

            if "wandb_get_tags_from_name" in self.parameter["wandb"]:
                self.wandb_get_tags_from_name = self.parameter["wandb"]["wandb_get_tags_from_name"]
            else:
                self.wandb_get_tags_from_name = False

            if self.wandb_get_tags_from_name is True:
                self.wandb_tags = self.wandb_run_name.split("/")[:-1]
            elif "wandb_tags" in self.parameter["wandb"]:
                self.wandb_tags = self.parameter["wandb"]["wandb_tags"]
            else:
                self.wandb_tags = None

            if "wandb_reinit" in self.parameter["wandb"]:
                self.wandb_reinit = self.parameter["wandb"]["wandb_reinit"]
            else:
                self.wandb_reinit = False

        else:
            self.log_to_wandb = False

        # ----------------------------------------------------
        # Other
        # ----------------------------------------------------

        # Fixed seed for all random values
        if "fixed_seed" in self.parameter["other"]:
            self.fixed_seed = self.parameter["other"]["fixed_seed"]
        else:
            self.fixed_seed = None

        # Fixed seed for parallelization
        if "fixed_execution_order" in self.parameter["other"]:
            self.fixed_execution_order = self.parameter["other"]["fixed_execution_order"]
        else:
            self.fixed_execution_order = True

        # Number of episodes after which the training progress is evaluated
        if "evaluate" in self.parameter["other"]:
            self.evaluate = self.parameter["other"]["evaluate"]
        else:
            self.evaluate = None


        # ----------------------------------------------------
        # Parallel execution
        # ----------------------------------------------------

        # Parallel execution
        self.run_in_parallel = initialize_parallel_exec(self.fixed_seed)

        # Number of rollout actors
        try:
            num_cores = len(psutil.Process().cpu_affinity())
        except:
            num_cores = 1
        if self.run_in_parallel is False:
            self.num_sim_workers = 1
            self.num_sim_threads = 0
        elif "num_sim_workers" in self.parameter["other"] and self.parameter["other"]["num_sim_workers"] is not None:
            self.num_sim_workers = self.parameter["other"]["num_sim_workers"]
            self.num_sim_threads = self.num_sim_workers
        else:
            self.num_sim_workers = num_cores - 1
            self.num_sim_threads = self.num_sim_workers


        # ----------------------------------------------------
        # Paths (only auto-generated)
        # ----------------------------------------------------

        self.training_dir = os.path.join(self.root_path, os.path.dirname(parameter_file))
        self.training_data = os.path.join(self.root_path, os.path.dirname(parameter_file), "trainings_data")
        self.plots_dir = os.path.join(self.root_path, os.path.dirname(parameter_file), "plots")
        self.weight_data = os.path.join(self.root_path, os.path.dirname(parameter_file), "weights")
        self.wandb_dir = os.path.join(self.root_path, os.path.dirname(parameter_file))

        # ----------------------------------------------------
        # Git commits (only auto-generated)
        # ----------------------------------------------------

        self.rllib_commit, self.rllib_commit_msg = get_git_commit_from_dir(__file__)
        self.main_commit, self.main_commit_msg = get_git_commit_from_dir(self.root_path)



    def get_config(self):
        """
        Returns the configuration parameters in a dictionary

        :return: Dictionary of parameters
        """

        # Get all variables of object
        config = self.__dict__.copy()

        # Remove internal variables
        del config["root_path"]
        del config["parameter"]

        return config



    def load_parameter(self, parameter_file):
        """
        Reads a JSON file with parameters and returns a parameter dictionary

        :param parameter_file:  String representing the path to the JSON file
        :return:                Dictionary containing the read parameters
        """
        with open(parameter_file) as json_file:
            parameter = json.load(json_file)

        return parameter