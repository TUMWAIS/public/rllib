import wandb

from rllib.classes.data_objects.variables.VarObjBase import VarObjBase


class VarObjOffPolicy(VarObjBase):
    """
    DQN-specific object to pass training states between functions
    """

    def __init__(self, params):

        # Variables for training state
        self.epsilon_counter = 0
        self.epsilon = params.epsilon
        self.q_value = 0
        self.t_value = 0
        self.batch_size = params.batch_size

        # Variables for the replay buffer
        self.replay_buffer_total_size = 0
        self.replay_buffer_sizes = []
        self.buffer_filled = False

        super().__init__(params)        # Needs to be called last



    def add_to_history(self):
        """
        Adds the current set of state variables to the history dictionary

        """
        super().add_to_history()

        if self._params.wandb_logging is True:
            wandb.log({"Parameter/epsilon": self.epsilon,
                       "Parameter/batch_size": self.batch_size,
                       "Reward/q_value": self.q_value,
                       "Reward/t_value": self.t_value,
                       "Reward/target_value": self.t_value,
                       "Other/replay_buffer_total_size": self.replay_buffer_total_size}, step=self.episode)



    def load_from_file(self):
        """
        Loads the last set of state variables from the history dictionary

        """
        super().load_from_file()
        self.epsilon_counter = self._history["epsilon_counter"][-1]
        self.epsilon = self._history["epsilon"][-1]
        #self.batch_size = self._history["batch_size"][-1]
        self.buffer_filled = self._history["buffer_filled"][0]