import wandb

from rllib.classes.data_objects.variables.VarObjBase import VarObjBase


class VarObjOnPolicy(VarObjBase):
    """
    DQN-specific object to pass training states between functions
    """

    def __init__(self, params):

        # Variables for training statistics
        self.batch_size = 0
        self.actor_loss = 0
        self.critic_loss = 0
        self.entropy_loss = 0
        self.value = 0

        super().__init__(params)        # Needs to be called last



    def add_to_history(self):
        """
        Adds the current set of state variables to the history dictionary

        """
        super().add_to_history()

        if self._params.wandb_logging is True:
            wandb.log({"Other/batch_size": self.batch_size,
                       "Other/actor_loss": self.actor_loss,
                       "Other/critic_loss": self.critic_loss,
                       "Other/entropy_loss": self.entropy_loss,
                       "Reward/value": self.value}, step=self.episode)