import wandb

from rllib.classes.data_objects.variables.VarObjOffPolicy import VarObjOffPolicy


class VarObjSAC(VarObjOffPolicy):
    """
    SAC-specific object to pass training states between functions
    """

    def __init__(self, params):

        # Variables for training statistics
        self.actor_loss = 0
        self.critic_1_loss = 0
        self.critic_2_loss = 0
        self.entropy_loss = 0
        self.alpha_temp = 0

        super().__init__(params)       # Needs to be called last



    def add_to_history(self):
        """
        Adds the current set of state variables to the history dictionary

        """
        super().add_to_history()

        if self._params.wandb_logging is True:
            wandb.log({"Other/actor_loss": self.actor_loss,
                       "Other/critic_1_loss": self.critic_1_loss,
                       "Other/critic_2_loss": self.critic_2_loss,
                       "Other/entropy_loss": self.entropy_loss,
                       "Other/alpha_temp": self.alpha_temp
                      }, step=self.episode)