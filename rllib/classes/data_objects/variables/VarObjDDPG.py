import wandb

from rllib.classes.data_objects.variables.VarObjOffPolicy import VarObjOffPolicy


class VarObjDDPG(VarObjOffPolicy):
    """
    SAC-specific object to pass training states between functions
    """

    def __init__(self, params):

        # Variables for training statistics
        self.actor_loss = 0
        self.critic_loss = 0
        self.td_error = 0


        super().__init__(params)       # Needs to be called last



    def add_to_history(self):
        """
        Adds the current set of state variables to the history dictionary

        """
        super().add_to_history()

        if self._params.wandb_logging is True:
            wandb.log({"Other/actor_loss": self.actor_loss,
                       "Other/critic_loss": self.critic_loss,
                       "Other/td_error": self.td_error}, step=self.episode)