from abc import ABC
import wandb
import os
import numpy as np

from rllib.functions.saving import save_history_to_multiple_files, load_latest_history_file

class VarObjBase(ABC):
    """
    Base class object to pass training state between different functions
    """

    def __init__(self, params):

        # Internal variables (not added to history)
        self._params = params                           # Not in line with other implementations, but neccessary since all public attributes are saved to history
        self._current_history_file_id = 0               # Variables for training history

        # Variables for training state
        self.episode = 0
        self.learning_rate = params.learning_rate_initial
        self.learning_rate_counter = 0
        self.mean_bonus = 0
        self.mean_pred_error = 0
        self.min_distance = 1
        self.mean_distance = 1

        # Variables for rollout statistics
        self.mean_steps = 0
        self.mean_reward = 0
        self.mean_dis_reward = 0
        self.mean_rel_reward = 0
        self.num_traj = 0

        # Variables for evaluation
        self.eval_mean_rel_reward = 0

        # Variables for timings
        self.sample_time = 0
        self.dataset_time = 0
        self.training_time = 0
        self.logging_time = 0
        self.episode_time = 0
        self.evaluation_time = 0
        self.overall_time = 0
        self.prev_overall_time = 0

        # Initialize history dict
        self._history = {}
        for key, value in self.__dict__.items():
            if key[0] != "_":
                self._history[key] = []



    def add_to_history(self):
        """
        Adds the current set of state variables to the history dictionary

        """

        # Add current variable values to history
        for key, value in self.__dict__.items():
            if key[0] != "_":
                self._history[key].append(value)

        # Log variables to wandb
        if self._params.wandb_logging is True:

            wandb.log({"Parameter/learning_rate": self.learning_rate,
                       "Reward/mean_bonus": self.mean_bonus,
                       "Environment/mean_steps": self.mean_steps,
                       "Environment/num_traj": self.num_traj,
                       "Reward/mean_reward": self.mean_reward,
                       "Reward/mean_dis_reward": self.mean_dis_reward,
                       "Reward/mean_rel_reward": self.mean_rel_reward,
                       "Timing/sample_time": self.sample_time,
                       "Timing/dataset_time": self.dataset_time,
                       "Timing/training_time": self.training_time,
                       "Timing/logging_time": self.logging_time,
                       "Timing/episode_time": self.episode_time,
                       "Timing/overall_time": self.overall_time,
                       "Other/mean_prediction_error": self.mean_pred_error,
                       "Environment/min_distance_to_goal": self.min_distance,
                       "Environment/mean_distance_to_goal": self.mean_distance}, step=self.episode)

            if self.eval_mean_rel_reward is not None:
                wandb.log({"Reward/eval_result": self.eval_mean_rel_reward}, step=self.episode)



    def load_from_file(self):
        """
        Loads the last set of state variables from the history dictionary
        """

        self.episode = self._history["episode"][-1] + 1
        self.learning_rate = self._history["learning_rate"][-1]
        self.learning_rate_counter = self._history["learning_rate_counter"][-1]
        self.prev_overall_time = self._history["overall_time"][-1]
        self.overall_time = self._history["overall_time"][-1]
        self.eval_mean_rel_reward = self._history["eval_mean_rel_reward"][-1]



    def save_history_to_file(self):
        """
        Saves the history object to a file and clears its content in order to save memory
        """

        self._current_history_file_id = save_history_to_multiple_files(os.path.join(self._params.training_data,"history/history"),
                                                                      self._history,
                                                                      self._params.history_max_file_size,
                                                                      self._current_history_file_id)



    def load_history_from_file(self):
        """
        Reads the history object from files

        """
        history_data, self._current_history_file_id = load_latest_history_file(os.path.join(self._params.training_data,"history/history"))

        if history_data is not None:
            for key in history_data:
                self._history[key].extend(history_data[key])
            self.load_from_file()
            return_value = True

        else:
            print("No files found to load. Start training from beginning.")
            return_value = False

        return return_value
