import wandb

from rllib.classes.data_objects.variables.VarObjOffPolicy import VarObjOffPolicy


class VarObjDQN(VarObjOffPolicy):
    """
    SAC-specific object to pass training states between functions
    """

    def __init__(self, params):

        # Variables for training statistics
        self.td_error = 0
        self.td_error_min = 0
        self.td_error_max = 0

        super().__init__(params)        # Needs to be called last



    def add_to_history(self):
        """
        Adds the current set of state variables to the history dictionary

        """
        super().add_to_history()

        if self._params.wandb_logging is True:
            wandb.log({"Other/td_error": self.td_error,
                       "Other/td_error_min": self.td_error_min,
                       "Other/td_error_max": self.td_error_max}, step=self.episode)