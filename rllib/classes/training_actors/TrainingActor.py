from rllib.functions.tensorflow import initialize_tf, activate_fp16_mixed_precision
from rllib.functions.seeding import gpu_seeding


class TrainingActor():
    """
    Training actor for parallel training of a policy object.
    The actual training algorithm is implemented by the policy.
    """

    def __init__(self,
                 worker_id,
                 params,
                 Policy):
        """
        Initializes the training actor

        :param worker_id:           ID of the trainer actor
        :param params:              Parameter object containing parameter for network architecture and training
        :param Policy:              Policy object to be trained
        """
        self.params = params
        self.worker_id = worker_id

        # Initialize tensorflow
        initialize_tf(enable_xla=params.enable_xla)

        # Activates mixed-precision calculations
        if params.fp16_mixed_precision is True:
            activate_fp16_mixed_precision()

        # Set random seeds
        if params.fixed_seed is not None:
            gpu_seeding(params.fixed_seed)

        self.policy = Policy(self.params)



    def train_policy(self,
                     weights,
                     *args,
                     **kwargs):
        """
        Trains the policy with the provided data

        :param minibatch:           Dictionary with data for the training of the policy
        :param weights:             List with weights for the networks
        :param learning_rate:       Optional learning rate for the training
        """

        # Set weights of policy
        self.policy.set_weights(weights)

        # Train policy
        training_info = self.policy.train_policy(*args, **kwargs)

        # Get updates weights
        weights = self.policy.get_weights()

        return weights, training_info



    def get_weights(self, restore=False):
        """
        Returns a list with network weights. The weights are either restored from a file or
        generated during the network initialization. The method is only used to initialize the weights variables
        in the main thread.

        :param restore:     Flag indicating whether the weights should be restored from a file
        :return:            List of weight for all networks
        """
        if restore is True:
            weights = self.policy.restore_weights()
        else:
            weights = self.policy.get_weights()

        return weights



    def store_weights(self, weights):
        """
        Stores network weights in a file

        :param weights: List of weights to be stored
        """

        self.policy.store_weights(weights)



