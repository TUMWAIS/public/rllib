from rllib.classes.training_actors.TrainingActor import TrainingActor

@run_parallel()
class TrainingActorStd(TrainingActor):
    """
    Training actor for parallel training of a policy object.
    The actual training algorithm is implemented by the policy.
    """
    pass