from abc import ABC, abstractmethod


class EnvBuilderAbstract(ABC):
    """
    Builder class that offers methods to initialize environments.
    """

    def __init__(self):
        pass



    @abstractmethod
    def create_gym_env(self, params, render=False):
        """
        Wrapper method that returns an environment object, which is initialized with the parameters provided in the
        params object.

        :param params:  Parameter object containing configuration data for the environment
        :param render:  Boolean flag to activate rendering (required by some environments)
        :return:        Initialized environment object
        """
        pass
