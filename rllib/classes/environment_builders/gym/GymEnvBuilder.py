import gym

from rllib.classes.environment_builders.EnvBuilderAbstract import EnvBuilderAbstract


class GymEnvBuilder(EnvBuilderAbstract):
    """
    Standard environment builder for the OpenAI Gym environments.
    """

    def create_gym_env(self, params, render=False):
        """
        Wrapper method that returns a gym environment object, which is initialized with the parameters provided in the
        params object.

        :param params:  Parameter object containing configuration data for the environment
        :param render:  Boolean flag to activate rendering
        :return:        Initialized XPPUSimEnv object
        """
        gym_env = gym.make(params.gym_env_name)

        if params.fixed_seed:
            gym_env.seed(params.fixed_seed)

        return gym_env