from abc import abstractmethod

from rllib.classes.environment_builders.EnvBuilderAbstract import EnvBuilderAbstract
from rllib.classes.rollout_actors.xppu.CoordPredictorStatelessXPPU import CoordPredictorStatelessXPPU
from rllib.classes.rollout_actors.xppu.CoordPredictorStatefulXPPU import CoordPredictorStatefulXPPU
from xppusim.gym.gym_env import XPPUSIMEnv
from rllib.classes.errors.ConfigurationError import ConfigurationError


class XPPUSimBuilderBase(EnvBuilderAbstract):
    """
    Builder class that offers methods to initialize the xppusim environment.
    """

    @abstractmethod
    def get_goal(self, params):
        """
        Initializes and returns the goal class to be used by the environment
        
        :param params:      Parameter object
        :return goal:       Goal class
        :return goal_args:  Arguments to be passed to the constructor of the goal class
        """
        pass


    def get_reset_function(self, params):
        """
        Initializes and returns reset_functions to be called, when the environment is reset.
        The function needs to take the following parameter:
            xppu:           Object representing the simulation environment
            wp_in_stack:    A list with workpieces in stack or none
            position:       A list with the workpiece position for initialization or none
            reset_fn_args:  A list with additional arguments passed by the rollout actor

        :param params:  Parameter object
        :return:        Handle to reset function
        """
        return None


    def create_gym_env(self, params, render=False):
        """
        Wrapper method that returns an xppusim_env object, which is initialized with the parameters provided in the
        params object.

        :param params:  Parameter object containing configuration data for the environment
        :param render:  Boolean flag to activate rendering
        :return:        Initialized XPPUSimEnv object
        """

        goal, goal_args = self.get_goal(params)

        xppusim_env = XPPUSIMEnv(state_space_conf_path=params.state_space_conf_path,
                                 action_space_conf_path=params.action_space_conf_path,
                                 goal=goal,
                                 goal_args=goal_args,
                                 wp_combination_pool_size=params.num_variants,
                                 wp_num_in_stack=params.num_wps,
                                 wp_pool_seed=params.wp_pool_seed,
                                 normalize_state_space=params.normalize_statespace,
                                 action_space_type=params.action_space_type,
                                 render=render,
                                 render_dir=params.render_dir,
                                 position_list = params.starting_wp_pos,
                                 crane_turn_only_extended=params.crane_turn_only_extended,
                                 crane_move_at_pickpoint=params.crane_move_at_pickpoint,
                                 reset_fn = self.get_reset_function(params)
                                 )

        return xppusim_env



    def get_coordinate_predictor(self, worker_id, params):
        """
        Returns an object for coordinate prediction to be passes to the environment

        :param worker_id:   ID of the worker for which the coordinate estimator is created
        :param params:      Parameter object
        :return:            Object for coordinate prediction
        """

        if params.coord_predictor_type == "stateless":
            CoordPredictor = CoordPredictorStatelessXPPU(params.coord_sensor_value_file,
                                                         params.training_data,
                                                         params.coord_prediction_model,
                                                         params.coord_buffer_size,
                                                         params.coord_batch_size,
                                                         params.coord_num_epochs,
                                                         params.coord_learning_rate,
                                                         params.coord_round_values,
                                                         params.num_wps)

        elif params.coord_predictor_type == "stateful":
            CoordPredictor = CoordPredictorStatefulXPPU(params.coord_sensor_value_file,
                                                        params.coord_action_value_file,
                                                        params.training_data,
                                                        params.coord_prediction_model,
                                                        params.coord_buffer_size,
                                                        params.coord_batch_size,
                                                        params.coord_num_epochs,
                                                        params.coord_learning_rate,
                                                        params.coord_round_values,
                                                        params.num_wps)
        else:
            raise ConfigurationError(params.coord_predictor_type, "Coordinate predictor type not found")


        return CoordPredictor