from abc import abstractmethod

from rllib.classes.environment_builders.EnvBuilderAbstract import EnvBuilderAbstract
from rllib.classes.rollout_actors.xppu.CoordPredictorStatelessXPPU import CoordPredictorStatelessXPPU
from rllib.classes.rollout_actors.xppu.CoordPredictorStatefulXPPU import CoordPredictorStatefulXPPU
from rllib.classes.errors.ConfigurationError import ConfigurationError


class XPPUPhyBuilderBase(EnvBuilderAbstract):
    """
    Builder class that offers methods to initialize the xppusim environment.
    """

    @abstractmethod
    def get_goal(self, params):
        """
        Initialized and returns the goal class to be used by the environment
        """
        pass


    def get_reset_function(self, params):
        return None


    def create_gym_env(self, params, render=False):
        """
        Wrapper method that returns an xppusim_env object, which is initialized with the parameters provided in the
        params object.

        :param params:  Parameter object containing configuration data for the environment
        :param render:  Boolean flag to activate rendering
        :return:        Initialized XPPUSimEnv object
        """
        from xppuphy.gym.gym_env import XPPUPHYEnv      # Make sure that it is only imported if it is actually needed

        goal, goal_args = self.get_goal(params)

        xppuphy_env = XPPUPHYEnv(state_space_conf_path=params.state_space_conf_path,
                                 action_space_conf_path=params.action_space_conf_path,
                                 plc_ip=params.plc_ip,
                                 plc_port=params.plc_port,
                                 cycle_time=params.cycle_time,
                                 goal=goal,
                                 goal_args=goal_args,
                                 wp_combination_pool_size=params.num_variants,
                                 wp_num_in_stack=params.num_wps,
                                 wp_pool_seed=params.wp_pool_seed,
                                 normalize_state_space=params.normalize_statespace,
                                 action_space_type=params.action_space_type,
                                 )

        return xppuphy_env



    def get_coordinate_predictor(self, worker_id, params):
        """
        Returns an object for coordinate prediction to be passes to the environment

        :param worker_id:   ID of the worker for which the coordinate estimator is created
        :param params:      Parameter object
        :return:            Object for coordinate prediction
        """

        if params.coord_predictor_type == "stateless":
            CoordPredictor = CoordPredictorStatelessXPPU(params.coord_sensor_value_file,
                                                         params.training_data,
                                                         params.coord_prediction_model,
                                                         params.coord_buffer_size,
                                                         params.coord_batch_size,
                                                         params.coord_num_epochs,
                                                         params.coord_learning_rate,
                                                         params.coord_round_values,
                                                         params.num_wps)

        elif params.coord_predictor_type == "stateful":
            CoordPredictor = CoordPredictorStatefulXPPU(worker_id,
                                                         params.coord_sensor_value_file,
                                                         params.coord_action_value_file,
                                                         params.coord_model_path,
                                                         params.coord_buffer_size,
                                                         params.coord_prediction_model,
                                                         params.coord_train_epochs,
                                                         params.coord_val_split,
                                                         params.num_wps)
        else:
            raise ConfigurationError(params.coord_predictor_type, "Coordinate predictor type not found")


        return CoordPredictor