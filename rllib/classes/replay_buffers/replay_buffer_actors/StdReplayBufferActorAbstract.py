from abc import ABC, abstractmethod
import os


from rllib.functions.seeding import non_gpu_seeding


class StdReplayBufferActorAbstract(ABC):
    """
    Abstract class for parallel replay buffer actors
    """
    def __init__(self, worker_id, params, replay_buffer_file=None):
        self.worker_id = worker_id
        self.params = params

        # Set random seeds
        if self.params.fixed_seed is not None:
            non_gpu_seeding(params.fixed_seed + worker_id)

        if replay_buffer_file is None:
            self.replay_buffer_file = os.path.join(self.params.training_data, "replay_buffer/replay_buffer_" + str(self.worker_id) + ".pkl")
        else:
            self.replay_buffer_file = replay_buffer_file

    @abstractmethod
    def save_to_buffer(self, traj_data):
        """
        # Abstract method for adding new data to the replay buffer. The data is passed via args.

        :param args: List of arguments to be passed to the method. Should include the data to save in the buffer.
        """

        pass


    @abstractmethod
    def get_minibatch(self, batch_size):
        """
        Abstract method for retrieving minibatches from the replay buffer.

        :param args: List of arguments to be passed to the method
        :return worker_id: Integer representing the ID of the worker, which returns the data
        :return minibatch_list: List of minibatches retrieved from the buffer
        :return buffer_size: The current size of the replay buffer
        """
        pass


    @abstractmethod
    def store_replay_buffer(self):
        """
        Abstract method for writing the replay buffer into a file

        :param args: List of arguments to be passed to the method
        """
        pass



