import numpy as np
import h5py
import os
from collections import deque
from copy import deepcopy

from rllib.classes.replay_buffers.replay_buffer_actors.StdReplayBufferActorAbstract import StdReplayBufferActorAbstract


class StdReplayBufferActor(StdReplayBufferActorAbstract):
    """
    Replay Buffer Actor representing a standard replay buffer.
    """

    def __init__(self, worker_id, params, replay_buffer_file=None):
        super().__init__(worker_id, params, replay_buffer_file)

        # Initializes replay buffer
        buffer = None
        if self.params.restore is True:
            buffer = self.load_from_hdf(self.replay_buffer_file)
            if buffer is None:
                print("No files found to load. Start training from beginning.")

        if buffer is not None:
            self.replay_buffer = deque(buffer, maxlen=int(self.params.max_replay_buffer / self.params.num_replay_buffer_workers))
        else:
            self.replay_buffer = deque(maxlen=int(self.params.max_replay_buffer / self.params.num_replay_buffer_workers))



    def save_to_buffer(self, traj_data):
        """
        Stores new data in the replay buffer.

        :param traj_data: A list containing transitions to be stored in the replay buffer
        """
        self.replay_buffer.extend(deepcopy(traj_data))      # Allocate memory at time of storage and not retrieval



    def get_minibatch(self, batch_size):
        """
        Retrieves minibatches from the replay buffer as numpy arrays.

        :param batch_size:      Batch size of the minibatches to be retrieved
        :return minibatch:      List of minibatches retrieved from the buffer
        :return info:           Dictionary containing the worker_id and current buffer_size
        """

        buffer_len = len(self.replay_buffer)

        # Check if replay buffer contains enough samples
        if batch_size * self.params.num_q_updates * self.params.num_batches_in_buffer > len(self.replay_buffer):
            info = {"worker_id": self.worker_id,
                    "buffer_size": buffer_len}

            return None, info
        else:
            sample_size = batch_size * self.params.num_q_updates

        # Sample minibatch (hard coding significantly faster than list comprehension)
        sample_idx = np.random.randint(len(self.replay_buffer), size=sample_size)
        sample = np.array(self.replay_buffer)[sample_idx]

        minibatch = []
        minibatch.append(np.stack(sample[:, 0]))        # obs
        minibatch.append(np.stack(sample[:, 1]))        # action
        minibatch.append(np.stack(sample[:, 2]))        # obs_next
        minibatch.append(np.stack(sample[:, 3]))        # reward
        minibatch.append(np.stack(sample[:, 4]))        # done
        if np.shape(sample)[1] == 6:
            minibatch.append(np.stack(sample[:, 5]))    # goal
        else:
            minibatch.append(None)

        info = {"worker_id": self.worker_id,
                "buffer_size": buffer_len}

        return minibatch, info



    def store_replay_buffer(self):
        """
        Saves the replay buffer to a file

        :param args: Not used
        """
        self.save_to_hdf(self.replay_buffer_file, self.replay_buffer)



    def save_to_hdf(self, file, object):
        """
        Writes the replay buffer to a file in the hdf5 format.
        Compared to pickle, the read and write process is much less memory intensive.

        :param file:        Path to HDF5 file
        :param object:      Object containing the replay buffer
        """
        sample = np.array(object)

        folder = os.path.dirname(file)
        if not os.path.exists(folder):
            try:
                os.makedirs(folder)                                # Can cause errors if multiple workers create folder at the same time
            except:
                pass

        with h5py.File(file, "w") as file:
            for i in range(sample.shape[1]):
                if self.params.compress_replay_buffer is True:
                    file.create_dataset("column_" + str(i), data=np.stack(sample[:, i]), compression="gzip")
                else:
                    file.create_dataset("column_" + str(i), data=np.stack(sample[:, i]))



    def load_from_hdf(self, file):
        """
        Reads the replay buffer from a file in the hdf5 format.

        :param file:        Path to HDF5 file
        :return:            Object with the replay buffer
        """
        try:
            # Read column data from file
            column_data = []
            with h5py.File(file, "r") as file:
                for i in range(len(file)):
                    column_data.append(file["column_" + str(i)][:])

            # Reshape columns into transition datasets
            buffer = []
            for i in range(len(column_data[0])):
                line = []
                for column in column_data:
                    line.append(column[i])
                buffer.append(line)

            return buffer

        except:
            return None