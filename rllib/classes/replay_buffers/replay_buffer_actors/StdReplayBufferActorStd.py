from rllib.classes.replay_buffers.replay_buffer_actors.StdReplayBufferActor import StdReplayBufferActor
from rllib.functions.parallel import run_parallel


@run_parallel()
class StdReplayBufferActorStd(StdReplayBufferActor):
    """
    Paralell actor representing a standard replay buffer.
    """
    pass