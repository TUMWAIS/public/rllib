from rllib.classes.replay_buffers.replay_buffer_actors.PrioritizedReplayBufferActor import PrioritizedReplayBufferActor
from rllib.functions.parallel import run_parallel



@run_parallel()
class PrioritizedReplayBufferActorStd(PrioritizedReplayBufferActor):
    """
    Paralell actor representing a prioritized replay buffer.
    """
    pass