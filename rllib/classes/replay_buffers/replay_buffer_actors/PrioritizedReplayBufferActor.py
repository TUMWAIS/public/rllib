import numpy as np
from copy import deepcopy
import h5py
import os

from rllib.classes.replay_buffers.replay_buffer_actors.StdReplayBufferActorAbstract import StdReplayBufferActorAbstract



class PrioritizedReplayBufferActor(StdReplayBufferActorAbstract):
    """
    Actor representing a prioritized replay buffer.
    """

    def __init__(self, worker_id, params, replay_buffer_file=None):
        super().__init__(worker_id, params, replay_buffer_file)

        # Initializes replay buffer
        self.replay_buffer = SumTree(int(self.params.max_replay_buffer / self.params.num_replay_buffer_workers))

        if self.params.restore is True:
            return_value = self.replay_buffer.load_from_hdf(self.replay_buffer_file)
            if return_value is False:
                print("No files found to load. Start training from beginning.")

        # Set parameter
        self.beta = self.params.beta



    def save_to_buffer(self, traj_data):
        """
        Stores new data in the replay buffer.

        :param traj_data: A list containing transitions to be stored in the replay buffer
        """

        max_priority = np.max(self.replay_buffer.priority[-self.replay_buffer.buffer_size:])
        if max_priority == 0:
            max_priority = self.params.epsilon_per ** self.params.alpha

        for trans in deepcopy(traj_data):                   # Allocate memory at time of storage and not retrieval
            self.replay_buffer.add(max_priority, trans)



    def get_minibatch(self, batch_size):
        """
        Retrieves minibatches from the replay buffer as numpy arrays.

        :param batch_size:      Batch size of the minibatches to be retrieved
        :return minibatch:      List of minibatches retrieved from the buffer
        :return info:           Dictionary containing the worker_id and current buffer_size
        """

        # Check if replay buffer contains enough samples
        if batch_size * self.params.num_q_updates * self.params.num_batches_in_buffer > self.replay_buffer.current_size:
            info = {"worker_id": self.worker_id,
                    "buffer_size": self.replay_buffer.current_size}
            return None, info

        # Increase beta
        self.beta = np.min([1, self.beta + self.params.beta_increase])  # increase until 1

        # Calculate p_min and w_max
        if self.replay_buffer.buffer_size > self.replay_buffer.current_size:
            p_min = np.min(self.replay_buffer.priority[-self.replay_buffer.buffer_size:-self.replay_buffer.buffer_size+self.replay_buffer.current_size]) / self.replay_buffer.total_priority()
        else:
            p_min = np.min(self.replay_buffer.priority[-self.replay_buffer.buffer_size:]) / self.replay_buffer.total_priority()
        w_max = (1 / (p_min * self.replay_buffer.current_size)) ** self.beta

        # Sample multiple minibatches
        minibatch = [[] for i in range(8)]
        for i in range(self.params.num_q_updates):
            priority_segment = self.replay_buffer.total_priority() / batch_size

            # Sample single transition
            for j in range(batch_size):
                lower_bound = priority_segment * j
                upper_bound = priority_segment * (j + 1)
                value = np.random.uniform(lower_bound, upper_bound)

                index, priority, data = self.replay_buffer.get_leaf(value)

                priority_norm = priority / self.replay_buffer.total_priority()
                weight = ((1 / (priority_norm * self.replay_buffer.current_size)) ** self.beta) / w_max

                # Add transition data to list (hard coding significantly faster than list comprehension)
                minibatch[0].append(data[0])        # obs
                minibatch[1].append(data[1])        # action
                minibatch[2].append(data[2])        # obs_next
                minibatch[3].append(data[3])        # reward
                minibatch[4].append(data[4])        # done
                if len(data) == 6:
                    minibatch[5].append(data[5])    # goal
                minibatch[6].append(weight)         # weight
                minibatch[7].append(index)          # index

        # Convert to Numpy (indices do not need to be converted)
        minibatch[0] = np.array(minibatch[0])
        minibatch[1] = np.array(minibatch[1])
        minibatch[2] = np.array(minibatch[2])
        minibatch[3] = np.array(minibatch[3])
        minibatch[4] = np.array(minibatch[4])
        if len(data) == 6:
            minibatch[5] = np.array(minibatch[5])
        minibatch[6] = np.array(minibatch[6]).astype(self.params.transfer_dtype)    # Weights not yet converted
        if len(data) != 6:
            minibatch[5] = None

        info = {"worker_id": self.worker_id,
                "buffer_size": self.replay_buffer.current_size}

        return minibatch, info



    def update_priorities(self, training_info):
        """
        Updates the priorities of transitions in the replay buffer based on provided TD errors.

        :param training_info: Dictionary containing a list of TD errors (td_error_list) and a list of the indices of the transitions in
                              the replay buffer that shall be updated (td_error_indices)
        """
        td_errors = np.array(training_info["td_error_list"])
        indices = training_info["td_error_indices"]

        td_errors = td_errors + self.params.epsilon_per           # Unclear if deepcopy is needed
        #td_errors = np.minimum(td_errors, self.params.td_error_clip)
        priorities = td_errors ** self.params.alpha

        for index, priority in zip(indices, priorities):
            self.replay_buffer.update(index, priority)


    def store_replay_buffer(self):
        """
        Saves the replay buffer to a file
        """
        self.replay_buffer.save_to_hdf(self.replay_buffer_file, self.params.compress_replay_buffer)




class SumTree():
    """
    SumTree for storing the prioritized replay buffer

    """
    def __init__(self, buffer_size):
        self.buffer_size = buffer_size                   # Number of leaf nodes
        self.priority = np.zeros(2 * buffer_size - 1)    # Array containing flattend tree with priorities
        self.data = np.zeros(buffer_size, dtype=object)  # Array containing flattend tree with transitions

        self.data_pointer = 0                            # Current position in the tree
        self.current_size = 0



    def add(self, priority, data):
        """
        Adds new transitions to the tree

        :param priority:    Float representing the priority
        :param data:        Transition data to be added to the tree
        """

        priority_idx = self.data_pointer + self.buffer_size - 1

        self.data[self.data_pointer] = data
        self.update(priority_idx, priority)  # Update priorities in parent nodes

        self.data_pointer += 1  # Shift data pointer
        if self.data_pointer >= self.buffer_size:  # Overwrite first element if buffer is full
            self.data_pointer = 0

        if self.current_size < self.buffer_size:
            self.current_size += 1



    def update(self, tree_idx, priority):
        """
        Updates leaf priority and propagates it through the tree

        :param tree_idx:    Index of transition data of which the priority should be updated
        :param priority:    Float representing the new priority
        """

        change = priority - self.priority[tree_idx]
        self.priority[tree_idx] = priority

        while tree_idx != 0:
            tree_idx = (tree_idx - 1) // 2  # Floor devision
            self.priority[tree_idx] += change



    def get_leaf(self, value):
        """
        Retrieve a transition including its priority

        :param value:       Value for identifying the transition data
        :return leaf_idx:   Index of returned transition
        :return priority:   Priority of returned transition
        :return data:       Transition data
        """
        parent_idx = 0

        while True:
            left_child_idx = 2 * parent_idx + 1
            right_child_idx = left_child_idx + 1

            if left_child_idx >= len(self.priority):
                leaf_idx = parent_idx
                break

            else:
                if value <= self.priority[left_child_idx]:
                    parent_idx = left_child_idx

                else:
                    value -= self.priority[left_child_idx]
                    parent_idx = right_child_idx

        data_idx = leaf_idx - self.buffer_size + 1
        return leaf_idx, self.priority[leaf_idx], self.data[data_idx]



    def total_priority(self):
        """
        Returns the total priority of the tree

        :return: Float representing the total priority
        """
        return self.priority[0]



    def save_to_hdf(self, file, compress=False):
        """
        Writes the replay buffer to a file in the hdf5 format.

        :param file:         Path to HDF5 file
        :param compress:     Boolean flag indicating whether the transition data shall be compressed
        """

        folder = os.path.dirname(file)
        if not os.path.exists(folder):
            try:                                # Can cause errors if multiple workers create folder at the same time
                os.makedirs(folder)
            except:
                pass

        with h5py.File(file, "w") as file:

            # Store transition data
            sample = np.stack(self.data[:self.current_size])  # Get part of array that is filled with data
            for i in range(sample.shape[1]):
                if compress is True:
                    file.create_dataset("data_column_" + str(i), data=np.stack(sample[:, i]), compression="gzip")
                else:
                    file.create_dataset("data_column_" + str(i), data=np.stack(sample[:, i]))

            # Store priorities and state variables
            file.create_dataset("priority", data=self.priority)
            file.create_dataset("data_pointer", data=[self.data_pointer])  # Data needs to be stored as list/array
            file.create_dataset("current_size", data=[self.current_size])  # Data needs to be stored as list/array



    def load_from_hdf(self, file):
        """
        Reads the replay buffer from a file in the hdf5 format.

        :param file:    Path to HDF5 file
        :return:        Boolean flag indicating whether reading was sucessful
        """
        try:
            with h5py.File(file, "r") as file:

                # Read transition data
                column_data = []
                for i in range(len(file)-3):
                    column_data.append(file["data_column_" + str(i)][:])

                for i in range(len(column_data[0])):
                    self.data[i] = []
                    for column in column_data:
                        self.data[i].append(column[i])

                # Read priorities and state variables
                self.priority = file["priority"][:]
                self.data_pointer = file["data_pointer"][0]  # Data is stored in first list entry
                self.current_size = file["current_size"][0]  # Data is stored in first list entry

                return True

        except:
            return False