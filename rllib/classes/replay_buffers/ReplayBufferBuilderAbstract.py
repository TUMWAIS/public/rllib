from abc import ABC, abstractmethod


class ReplayBufferBuilderAbstract(ABC):
    """
    Abstract builder class that returns the handle to a replay buffer actor class
    """

    def __init__(self, params):
        self.params = params


    @abstractmethod
    def get_replay_buffer(self):
        """
        Returns a replay buffer actor class

        :param params:  Parameter object
        :return:        Handle to replay buffer actor
        """
        pass