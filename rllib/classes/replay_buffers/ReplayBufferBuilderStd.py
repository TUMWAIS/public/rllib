from rllib.classes.replay_buffers.ReplayBufferBuilderAbstract import ReplayBufferBuilderAbstract
from rllib.classes.replay_buffers.replay_buffer_actors.StdReplayBufferActorStd import StdReplayBufferActorStd
from rllib.classes.replay_buffers.replay_buffer_actors.PrioritizedReplayBufferActorStd import PrioritizedReplayBufferActorStd


class ReplayBufferBuilderStd(ReplayBufferBuilderAbstract):
    """
    Standard builder class providing a replay buffer actor for the current configuration
    """

    def get_replay_buffer(self):
        """
        Returns a replay buffer actor class

        :param params:  Parameter object
        :return:        Handle to replay buffer actor
        """
        if hasattr(self.params, "PER") and self.params.PER is True:     # Attribute not necessarily in parameter object
            return PrioritizedReplayBufferActorStd
        else:
            return StdReplayBufferActorStd