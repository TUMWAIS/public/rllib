from abc import abstractmethod
import tensorflow as tf
import numpy as np

from rllib.classes.policies.PolicyAbstract import PolicyAbstract
from rllib.functions.tensorflow import device_placement, get_weights, set_weights


class PolicyVPGBase(PolicyAbstract):
    """
    Base class for VPG policies. The class implements the basic training and rollout functionality.
    The same network is used for the actor and critic.
    The network architecture need to be added before use.
    """

    def __init__(self, params, *args, force_cpu_use=False, **kwargs):
        """
        Initialize policy
        
        :param params:          Parameter object
        :param force_cpu_use:   Flag indicating whether the policy should be force onto the CPU
        """
        super().__init__(params)

        # Set variables
        self.force_cpu_use=force_cpu_use

        # Create optimizer
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.learning_rate_initial)
        if self.params.scale_loss is True:
            self.optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.optimizer, "dynamic")

        # Create networks and initilize weight variables
        self.init_networks()



    def init_networks(self, *args, **kwargs):
        """
        Creates neural networks and initializes all weight variables
        """

        # Create network
        self.network = device_placement(self.force_cpu_use, self.build_network)

        # Setup weight list
        weights = get_weights(self.network)
        self.network_weights_dtype = weights[0].dtype           # Get data type of network weights
        self.weights = [weights]



    def get_weights(self, *args, **kwargs):
        """
        Returns a list of all network weights.
        Weights can be converted to lower precision datatype in order to reduce transfer times and memory requirements

        :return: List of all network weights
        """
        super().get_weights()
        network_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[0]]

        return [network_weights]



    def set_weights(self, weights, *args, **kwargs):
        """
        Sets all network weights to the ones provided in a list
        Weights can be converted to lower precision datatype in order to reduce transfer times and memory requirements

        :param weights: List of network weights
        """
        super().set_weights(weights)

        network_weights = [tf.cast(w, self.network_weights_dtype) for w in weights[0]]
        self.weights = [network_weights]

        set_weights(self.network, network_weights)



    def actor_predict(self, obs, *args, goal=None, **kwargs):
        """
        Executes a forward pass through the actor and critic network and returns logits and values

        :param obs:     Numpy array of observations
        :param goal:    Optional numpy array with goal to achieve
        :return logits: Numpy array with output of the actor network
        """
        feed_obs = self.get_feed_dict(obs, goal)
        logits = device_placement(self.force_cpu_use, self.predict, self.network, feed_obs)

        return logits



    def run_policy(self, obs, *args, goal=None, policy_params=[], deploy=False, **kwargs):
        """
        Runs the policy with the provided parameters and returns a list of selected actions

        :param obs:             Numpy array of observations
        :param goal:            Numpy array with goal to achieve or None
        :param policy_params:   List of parameters for the policy (e.g., epsilon)
        :param deploy:          Flag indicating whether the policy should be run in deployment mode
        :return:                List of actions chosen by the policy
        """
        super().run_policy(obs, goal, policy_params)

        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        action_dist = self.actor_predict(obs, goal).numpy()

        # Chose random action from distribution
        action = [np.random.choice(len(action_dist[0]), p=action_dist[0])]

        return action



    def get_value_estimate(self, obs, *args, goal=None, **kwargs):
        """
        Returns estimates for the value/action-value function at the provided observation

        :param obs:     Numpy array of observations
        :param goal:    Numpy array with goal to achieve or None
        :return:        List of value estimates
        """
        pass



    def train_policy(self,
                     episode,
                     minibatch,
                     *args,
                     learning_rate=None,
                     **kwargs):
        """
        Trains the policy with the provided samples

        :param episode:         Current episode in which training function is called
        :param minibatch:       List with data for the training of the policy    [obs, action, obs_next, reward, done, goal]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """
        super().train_policy(minibatch, learning_rate)

        # Read minibatch
        obs = minibatch[0]
        action = minibatch[1]
        obs_next = minibatch[2]
        reward = minibatch[3]
        done = minibatch[4]

        if len(minibatch) > 5:
            goal = minibatch[5]
        else:
            goal = None

        feed_obs = self.get_feed_dict(obs=obs, goal=goal)
        action = np.squeeze(action).astype("int32")

        # Get montecarlo psi
        psi = self.get_montecarlo_psi(reward,
                                      done,
                                      self.params.gamma,
                                      self.params.baseline,
                                      self.params.psi_scaling,
                                      self.params.transfer_dtype)

        # Update policy
        actor_loss = self.update_policy_vpg(feed_obs,
                                            action,
                                            psi)

        training_info = {"actor_loss": actor_loss}

        self.weights[0] = get_weights(self.network)

        return training_info



    def get_montecarlo_psi(self,
                           rewards, 
                           done,
                           *args,
                           gamma=1, 
                           baseline=True, 
                           psi_scaling=False, 
                           type="float32",
                           **kwargs):
        """
        Calculates the reward-to-go and optionally subtracts a baseline.

        :param rewards:     A list of rewards
        :param done:        A list of flags whether the trajectory is finished
        :param gamma:       Discount parameter to calculate the mean discounted rewards
        :param baseline:    Boolean flag indicating wheter a baseline shall be substracted
        :param type:        Numpy data type of return values
        :return:            List of rewards to go
        """
        dis_cum_reward = 0
        psi = []
        for i in reversed(range(len(rewards))):
            dis_cum_reward = (dis_cum_reward * gamma * (1 - done[i])) + rewards[i]
            psi.append(dis_cum_reward)

        psi.reverse()
        psi = np.array(psi).astype(type)

        if baseline is True:
            psi = psi - np.mean(psi)

        if psi_scaling is True:
            psi = psi / np.std(psi)

        return psi.astype(type)



    @abstractmethod
    def build_network(self, *args, **kwargs):
        """
        Abstract method that returns the neural network architecture for the q- and target network

        :return: Tensorflow/Keras network object
        """
        pass



    @abstractmethod
    def get_actionspace_dim(self, feed_obs, *args, **kwargs):
        """
        Abstract method that returns an integer value representing the actionspace dimension.
        Used by the run_policy method.

        :return: Integer representing the actionspace dimension
        """
        pass



    @abstractmethod
    def get_feed_dict(self, obs, *args, goal=None, **kwargs):
        """
        Abstract method that converts numpy arrays for observation and goal into dictionaries for each network input.

        :param obs:     Numpy array with observations
        :param goal:    Optional numpy array with goal
        :return:        Dictionary with key for each network input
        """
        pass



    @tf.function
    def predict(self, model, input):
        """
        Custom predict function.
        The Keras function has high overhead and can cause memory leaks.
        Direct calling of the model seems to be slower and can also cause memory leaks.

        :param model:   Model to create prediction from
        :param input:   Numpy array or feed dict for prediction
        :return:        Tensor with prediction
        """
        output = model(input, training=False)

        return output



    @tf.function(experimental_relax_shapes=True)
    def update_policy_vpg(self,
                          input_obs,
                          actions,
                          psi):
        """
        Conducts a single update of the policy network. This function is converted to a tensorflow graph to
        increase efficiency.

        :param input_obs:        Dict containing numpy arrays of observations after transition for each network input
        :param actions:          Numpy array containing the chosen actions
        :param psi:              Numpy array with weights
        :return loss:            Float representing the loss
        """
        with tf.GradientTape() as tape:
            action_dist = self.network(input_obs)

            idx = tf.range(tf.shape(actions)[0], dtype=tf.int32)
            neg_loglikelihoods = -tf.math.log(tf.gather_nd(action_dist, tf.stack([idx, actions], axis=1)))
            loss_value = tf.math.reduce_mean(neg_loglikelihoods * psi)

            # Scale loss to avoid numerical issued on FP16
            if self.params.scale_loss is True:
                loss_value = self.optimizer.get_scaled_loss(loss_value)

        grads = tape.gradient(loss_value, self.network.trainable_variables)

        # Unscale gradients
        if self.params.scale_loss is True:
            grads = self.optimizer.get_unscaled_gradients(grads)

        # Clip gradients
        if self.params.gradient_clipping == "ByValue":
            grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in grads]
        elif self.params.gradient_clipping == "ByNorm":
            grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in grads]
        elif self.params.gradient_clipping == "ByGlobalNorm":
            grads, _ = tf.clip_by_global_norm(grads, self.params.clip_value_max)

        self.optimizer.apply_gradients(zip(grads, self.network.trainable_variables))

        return loss_value