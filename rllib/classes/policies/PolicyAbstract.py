from abc import ABC, abstractmethod
import os

from rllib.classes.errors.RestoreError import RestoreError
from rllib.functions.saving import load_weight_list_from_file, save_weight_list_to_file



class PolicyAbstract(ABC):
    """
    Abstract policy class
    """

    @abstractmethod
    def __init__(self, params, *args, **kwargs):
        """
        Initialize policy

        :param params:          Parameter object
        """
        self.params = params



    @abstractmethod
    def get_weights(self, *args, **kwargs):
        """
        Returns a list of all network weights.

        :return: List of all network weights
        """
        pass



    @abstractmethod
    def set_weights(self, weights, *args, **kwargs):
        """
        Sets all network weights to the ones provided in a list

        :param weights: List of network weights
        """
        pass



    @abstractmethod
    def run_policy(self, obs, goal, policy_params, *args, deploy=False, **kwargs):
        """
        Runs the policy with the provided parameters and returns a list of selected actions

        :param obs:             Numpy array of observations
        :param goal:            Numpy array with goal to achieve or None
        :param policy_params:   List of parameters for the policy (e.g., epsilon)
        :param deploy:          Flag indicating whether the policy should be run in deployment mode
        :return:                List of actions chosen by the policy
        """
        pass



    def get_value_estimate(self, obs, goal, *args, **kwargs):
        """
        Returns estimates for the value/action-value function at the provided observation

        :param obs:     Numpy array of observations
        :param goal:    Numpy array with goal to achieve or None
        :return:        List of estimates
        """
        pass



    @abstractmethod
    def train_policy(self, episode, minibatch, *args, learning_rate=None, **kwargs):
        """
        Trains the policy with the provided samples

        :param episode:         Current episode in which training function is called
        :param minibatch:       List with data for the training of the policy    [obs, action, obs_next, reward, done, goal]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """

        # Set learning rate if provided
        if learning_rate is not None:
            self.optimizer.lr.assign(learning_rate)



    def restore_weights(self, *args, **kwargs):
        """
        Restores network weights from a file

        :return: List with network weights
        """
        weights = load_weight_list_from_file(os.path.join(self.params.weight_data, "weights"))
        if weights is None:
            raise RestoreError(os.path.join(self.params.weight_data, "weights"),
                               "Network weights could not be restored")

        return weights



    def store_weights(self, weights, *args, **kwargs):
        """
        Stores network weights in a file

        :param weights: List of weights to be stored
        """

        save_weight_list_to_file(os.path.join(self.params.weight_data, "weights"), weights)
