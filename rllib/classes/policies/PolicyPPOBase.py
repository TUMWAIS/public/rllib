import tensorflow as tf
import numpy as np

from rllib.classes.policies.PolicyA2CBase import PolicyA2CBase
from rllib.classes.policies.PolicyAbstract import PolicyAbstract
from rllib.functions.tensorflow import get_weights



class PolicyPPOBase(PolicyA2CBase):
    """
    Base class for PPO policies. The class implements the basic training and rollout functionality.
    The same network is used for the actor and critic.
    The network architecture need to be added before use.
    """

    def train_policy(self,
                     episode,
                     minibatch,
                     *args,
                     learning_rate=None,
                     **kwargs):
        """
        Trains the policy with the provided samples

        :param episode:         Current episode in which training function is called
        :param minibatch:       List with data for the training of the policy    [obs, action, obs_next, reward, done, goal]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """
        PolicyAbstract.train_policy(self, episode, minibatch, learning_rate)  # Call method of abstract class

        # Read minibatch
        obs = minibatch[0]
        action = minibatch[1]
        obs_next = minibatch[2]
        reward = minibatch[3]
        done = minibatch[4]

        if len(minibatch) > 5:
            goal = minibatch[5]
        else:
            goal = None

        # Get logits and values
        action_dist, values = self.actor_critic_predict(obs, goal)
        values_next = self.critic_predict(obs_next, goal)

        values = values.numpy().flatten()
        values_next = values_next.numpy().flatten()

        # Get Generalized Advantage Estimator and target values
        psi, target_values = self.get_gae_psi(values,
                                              values_next,
                                              reward,
                                              done,
                                              self.params.gamma,
                                              self.params.lambda_,
                                              self.params.baseline,
                                              self.params.psi_scaling,
                                              self.params.transfer_dtype
                                             )

        action = np.squeeze(action).astype("int32")
        idx = range(len(action))
        loglikelihoods_oldpolicy = np.log(action_dist.numpy()[idx, action])

        #action = tf.squeeze(tf.cast(action, "int32"))
        #idx = tf.range(tf.shape(action)[0], dtype=tf.int32)
        #loglikelihoods_oldpolicy = tf.math.log(tf.gather_nd(action_dist, tf.stack([idx, action], axis=1)))

        # Update policy for several epochs
        actor_loss_list = []
        critic_loss_list = []
        entropy_loss_list = []
        for k in range(self.params.num_policy_updates):

            # Sample random minibatch
            indices = np.random.choice(range(len(obs)), self.params.minibatch_size, replace=False)

            if len(minibatch) > 5:
                feed_goal = goal[indices, :]
            else:
                feed_goal = None

            feed_obs = self.get_feed_dict(obs=obs[indices, :], goal=feed_goal)

            # Update policy
            actor_loss, critic_loss, entropy_loss = self.update_policy_ppo(feed_obs,
                                                                           action[indices],
                                                                           psi[indices],
                                                                           target_values[indices],
                                                                           loglikelihoods_oldpolicy[indices])

            actor_loss_list.append(actor_loss)
            critic_loss_list.append(critic_loss)
            entropy_loss_list.append(entropy_loss)

        training_info = {"actor_loss": np.mean(actor_loss_list),
                         "critic_loss": np.mean(critic_loss_list),
                         "entropy_loss": np.mean(entropy_loss_list)}

        self.weights[0] = get_weights(self.network)

        return training_info



    @tf.function
    def update_policy_ppo(self,
                          input_obs,
                          actions,
                          psi,
                          target_values,
                          loglikelihoods_oldpolicy):
        """
        Executes one update of the actor-critic network with the clipped surrogate objective

        :param input_obs:                   Dict containing numpy arrays with the observations for each network input
        :param actions:                     Numpy array containing the chosen actions
        :param psi:                         Numpy array containing the generalized advantage weights
        :param target_values:               Numpy array containing the target values for the critic head
        :param loglikelihoods_old_policy:   Tensorflow object containing the loglikelihoods of the policy before the first update
        """

        with tf.GradientTape() as tape:
            action_dist, values = self.network(input_obs)

            idx = tf.range(tf.shape(actions)[0], dtype=tf.int32)
            loglikelihoods_newpolicy = tf.math.log(tf.gather_nd(action_dist, tf.stack([idx, actions], axis=1)))

            # Calculate clipped actor loss
            ratio = tf.math.exp(loglikelihoods_newpolicy - tf.stop_gradient(loglikelihoods_oldpolicy))
            p1 = ratio * psi
            p2 = tf.clip_by_value(ratio, clip_value_min=1 - self.params.clip_val,
                                  clip_value_max=1 + self.params.clip_val) * psi
            actor_loss = -tf.math.reduce_mean(tf.math.minimum(p1, p2))

            # Calculate critic loss
            critic_error = tf.math.squared_difference(values, tf.expand_dims(target_values, -1))
            critic_loss = tf.math.reduce_mean(critic_error) * self.params.critic_discount

            # Calculate entropy loss (to improve exploration)
            entropy_loss = -tf.math.reduce_mean(
                -tf.math.exp(loglikelihoods_newpolicy) * loglikelihoods_newpolicy) * self.params.entropy_beta

            # Aggregate loss
            if self.params.agreggrate_loss is False:
                loss_value = {"actor_head": actor_loss + entropy_loss, "critic_head": critic_loss}
            else:
                loss_value = actor_loss + critic_loss + entropy_loss

            # Scale loss to avoid numerical issued on FP16
            if self.params.scale_loss is True:
                loss_value = self.optimizer.get_scaled_loss(loss_value)

        grads = tape.gradient(loss_value, self.network.trainable_variables)

        # Unscale gradients
        if self.params.scale_loss is True:
            grads = self.optimizer.get_unscaled_gradients(grads)

        # Clip gradients
        if self.params.gradient_clipping == "ByValue":
            grads = [
                None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max)
                for grad in grads]
        elif self.params.gradient_clipping == "ByNorm":
            grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in grads]
        elif self.params.gradient_clipping == "ByGlobalNorm":
            grads, _ = tf.clip_by_global_norm(grads, self.params.clip_value_max)

        self.optimizer.apply_gradients(zip(grads, self.network.trainable_variables))

        return actor_loss, critic_loss, entropy_loss