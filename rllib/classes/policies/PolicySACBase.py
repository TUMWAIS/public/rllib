import tensorflow as tf
import numpy as np
from copy import deepcopy

from rllib.classes.policies.PolicyAbstract import PolicyAbstract
from rllib.functions.tensorflow import device_placement, get_weights, set_weights, soft_update_target_network



class PolicySACBase(PolicyAbstract):
    """
    Base class for SAC policies. The class implements the basic training and rollout functionality.
    The network architecture need to be added before use.
    """

    def __init__(self, params, force_cpu_use=False):
        """
        Initialize policy

        :param params:          Parameter object
        :param force_cpu_use:   Flag indicating whether the policy should be force onto the CPU
        """
        super().__init__(params)

        # Set variables
        self.force_cpu_use=force_cpu_use

        self.actor_optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.learning_rate_initial)
        self.critic_1_optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.learning_rate_initial)
        self.critic_2_optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.learning_rate_initial)

        if self.params.scale_loss is True:
            self.actor_optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.actor_optimizer, "dynamic")
            self.critic_1_optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.critic_1_optimizer, "dynamic")
            self.critic_2_optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.critic_2_optimizer, "dynamic")


        # Create variable and optimizer for learning of alpha
        self.alpha = np.array(self.params.temp_alpha, dtype=self.params.transfer_dtype)
        self.log_alpha = tf.Variable(tf.math.log(self.alpha), dtype=self.params.transfer_dtype)
        if self.params.optimize_alpha_temp is True:
            self.alpha_optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.lr_alpha_temp)
        if self.params.scale_loss is True:
            self.alpha_optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.alpha_optimizer, "dynamic")

        # Create networks and initilize weight variables
        self.init_networks()




    def init_networks(self, *args, **kwargs):
        """
        Creates neural networks and initilizes all weight variables
        """
        # Create network
        self.actor_network = device_placement(force_cpu_use, self.build_actor_network)
        self.critic_network_1 = device_placement(force_cpu_use, self.build_critic_network)
        self.critic_network_2 = device_placement(force_cpu_use, self.build_critic_network)
        self.critic_target_network_1 = device_placement(force_cpu_use, self.build_critic_network)
        self.critic_target_network_2 = device_placement(force_cpu_use, self.build_critic_network)

        actor_weights = get_weights(self.actor_network)
        critic_1_weights = get_weights(self.critic_network_1)
        critic_2_weights = get_weights(self.critic_network_2)

        self.actor_weights_dtype = actor_weights[0].dtype             # Get data type of network weights
        self.critic_weights_dtype = critic_1_weights[0].dtype         # Get data type of network weights

        self.weights = [actor_weights, critic_1_weights, critic_2_weights, critic_1_weights, critic_2_weights]



    def get_weights(self):
        """
        Returns a list of all network weights.

        :return: List of all network weights
        """
        super().get_weights()

        actor_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[0]]
        critic_1_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[1]]
        critic_2_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[2]]
        critic_1_target_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[3]]
        critic_2_target_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[4]]

        return [actor_weights, critic_1_weights, critic_2_weights, critic_1_target_weights, critic_2_target_weights]



    def set_weights(self, weights, name=""):
        """
        Sets all network weights to the ones provided in a list

        :param weights: List of network weights
        """
        super().set_weights(weights)

        actor_weights = [tf.cast(w, self.actor_weights_dtype) for w in weights[0]]
        critic_1_weights = [tf.cast(w, self.critic_weights_dtype) for w in weights[1]]
        critic_2_weights = [tf.cast(w, self.critic_weights_dtype) for w in weights[2]]
        critic_1_target_weights = [tf.cast(w, self.critic_weights_dtype) for w in weights[3]]
        critic_2_target_weights = [tf.cast(w, self.critic_weights_dtype) for w in weights[4]]
        self.weights = [actor_weights, critic_1_weights, critic_2_weights, critic_1_target_weights, critic_2_target_weights]

        set_weights(self.actor_network, actor_weights)
        set_weights(self.critic_network_1, critic_1_weights)
        set_weights(self.critic_network_2, critic_2_weights)
        set_weights(self.critic_target_network_1, critic_1_target_weights)
        set_weights(self.critic_target_network_2, critic_2_target_weights)



    def run_policy(self, obs, goal=None, policy_params=[], deploy=False):
        """
        Runs the policy with the provided parameters and returns a list of selected actions

        :param obs:             Numpy array of observations
        :param goal:            Numpy array with goal to achieve or None
        :param policy_params:   List of parameters for the policy (e.g., epsilon)
        :param deploy:          Flag indicating whether the policy should be run in deployment mode
        :return:                List of actions chosen by the policy
        """
        super().run_policy(obs, goal, policy_params, deploy)

        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        feed_obs = self.get_feed_dict(obs, goal)
        actions, mean, _ = device_placement(self.force_cpu_use, self.actor_network, feed_obs)

        if deploy is True:
            actions = mean

            #obs = np.tile(obs, [self.params.action_buffer_size, 1])
            #actions = np.array([[(a - self.params.action_shift) / self.params.action_factor] for a in range(self.params.action_buffer_size)])
            #feed_obs = self.get_feed_dict(obs)

            #q_values = device_placement(self.force_cpu_use, self.critic_network_1, dict(feed_obs, **{"action": actions}))
            #actions = np.array([[np.argmax(q_values)]])
            #actions =  (actions - self.params.action_shift) / self.params.action_factor

        elif deploy is False and np.random.rand() <= policy_params[0]:
            actions = self.add_exploration(actions, mean)
        else:
            actions = actions

        return actions



    def add_exploration(self, actions, mean):
        """
        Add exploration for epsilon-greedy policy.
        The standard SAC does not use an epsilon-greedy policy.

        :param actions:     # Stochastic action from network
        :param mean:        # Deterministic action from network
        :return:            # Action with added exploration
        """
        return actions



    def get_value_estimate(self, obs, goal=None):
        """
        Returns estimates for the value/action-value function at the provided observation

        :param obs:     Numpy array of observations
        :param goal:    Optional numpy array with goal to achieve or None
        :return:        List of estimates
        """
        super().get_value_estimate(obs, goal)

        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        # Play forward pass through critic and get feed dict
        feed_obs = self.get_feed_dict(obs, goal)
        actions, means, log_stds = device_placement(self.force_cpu_use, self.actor_network, feed_obs)
        # Get critic values
        critic_1_value = device_placement(self.force_cpu_use, self.critic_network_1, dict(feed_obs, **{"action": means}))
        #critic_2_value = device_placement(self.force_cpu_use, self.critic_network_2, dict(feed_obs, **{"action": means}))
        #critic_mean_value = (critic_1_value.numpy() + critic_2_value.numpy()) / 2

        # Get target critic values
        critic_1_value_target = device_placement(self.force_cpu_use, self.critic_target_network_1, dict(feed_obs, **{"action": means}))
        #critic_2_value_target = device_placement(self.force_cpu_use, self.critic_target_network_2, dict(feed_obs, **{"action": means}))
        #critic_mean_value_target = (critic_1_value_target.numpy() + critic_2_value_target.numpy()) / 2

        info = {"q_values": critic_1_value,
                "t_values": critic_1_value_target}

        return info



    def train_policy(self,
                     episode,
                     minibatch,
                     learning_rate=None):
        """
        Trains the policy with the provided samples

        :param episode:         Current training episode (for hard target network update)
        :param minibatch:       List with data for the training of the policy     [obs, action, obs_next, reward, done, goal]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """

        actor_loss_list = []
        critic_1_loss_list = []
        critic_2_loss_list = []
        entropy_loss_list = []
        alpha_list = []
        for k in range(self.params.num_q_updates):
            lb = self.params.batch_size * k
            ub = self.params.batch_size * (k+1)

            if minibatch[5] is not None:
                goal = minibatch[5][lb:ub]
            else:
                goal = None

            feed_obs = self.get_feed_dict(obs=minibatch[0][lb:ub], goal=goal)
            feed_obs_next = self.get_feed_dict(obs=minibatch[2][lb:ub], goal=goal)

            alpha, \
            actor_loss, \
            critic_1_loss, \
            critic_2_loss, \
            entropy = self.update_policy_sac(feed_obs,
                                             minibatch[1][lb:ub],
                                             feed_obs_next,
                                             minibatch[3][lb:ub],
                                             minibatch[4][lb:ub],
                                             self.alpha)

            self.alpha = alpha.numpy()
            alpha_list.append(self.alpha)
            critic_1_loss_list.append(critic_1_loss)
            critic_2_loss_list.append(critic_2_loss)
            actor_loss_list.append(actor_loss)
            entropy_loss_list.append(entropy)


            # Update target networks (soft)
            if self.params.soft_target_update is True and k % self.params.target_update_steps == 0:
                soft_update_target_network(self.params, self.critic_network_1, self.critic_target_network_1)
                soft_update_target_network(self.params, self.critic_network_2, self.critic_target_network_2)


        training_info = {"actor_loss": np.mean(actor_loss_list),
                         "critic_1_loss": np.mean(critic_1_loss_list),
                         "critic_2_loss": np.mean(critic_2_loss_list),
                         "entropy_loss": np.mean(entropy_loss_list),
                         "alpha_temp": np.mean(alpha_list)}

        # Get updates weights from networks
        self.weights[0] = get_weights(self.actor_network)
        self.weights[1] = get_weights(self.critic_network_1)
        self.weights[2] = get_weights(self.critic_network_2)
        self.weights[3] = get_weights(self.critic_target_network_1)
        self.weights[4] = get_weights(self.critic_target_network_2)

        # Update target networks (hard)
        if self.params.soft_target_update is False and episode % self.params.target_update_episodes == 0:
            self.weights[3] = deepcopy(self.weights[1])
            self.weights[4] = deepcopy(self.weights[2])

        return training_info



    def get_actionspace_dim(self):
        """
        Abstract method that returns an integer value representing the actionspace dimension.
        Necessary for the sampling of random actions by the e-greedy policy.

        :return: Integer representing the actionspace dimension
        """
        pass



    def get_feed_dict(self, obs, goal=None):
        """
        Abstract method that converts numpy arrays for observation and goal into dictionaries for each network input.

        :param obs:     Numpy array with observations
        :param goal:    Optional numpy array with goal
        :return:        Dictionary with key for each network input
        """
        pass


    def build_actor_network(self):
        """
        Abstract method that returns the neural network architecture for the actor

        :return: Tensorflow/Keras network object
        """

        # Create network input
        pass


    def build_critic_network(self):
        """
        Abstract method that returns the neural network architecture for the critic

        :return: Tensorflow/Keras network object
        """

        # Create network input
        pass


    @tf.function
    def update_policy_sac(self,
                          input_obs,
                          action,
                          input_obs_next,
                          reward,
                          done,
                          alpha):
        """
        Conducts a single update of the actor and critic networks. This function is converted to a tensorflow graph to
        increase efficiency.

        :param input_obs:               Dict containing numpy arrays of observations for each network input before the transition
        :param actions:                 Numpy array containing the chosen actions
        :param input_obs_next:          Dict containing numpy arrays of observations for each network input after the transition
        :param reward:                  Numpy array containing the rewards for each transition
        :param done:                    Numpy array containing the done flags for each transition
        :param alpha:                   Float representing the current value of alpha
        :return alpha:                  Float representing the updates value of alpha
        :return actor_loss_value:       Float representing the actor loss
        :return critic_1_loss_value:    Float representing the loss of critic 1
        :return critic_2_loss_value:    Float representing the loss of critic 1
        :return entropy:                Float representing the entropy loss
        """

        # Reshape actions into 2 dimensional tensor
        action = tf.squeeze(action, axis=1)

        with tf.GradientTape(persistent=True) as tape:

            # Play forward passes through critic with action from replay buffer
            critic_1 = self.critic_network_1(dict(input_obs, **{"action": action}))
            critic_2 = self.critic_network_2(dict(input_obs, **{"action": action}))

            # Play forward pass through critic target
            action_next, means_next, log_probs_next = self.actor_network(input_obs_next)
            critic_1_target_next = self.critic_target_network_1(dict(input_obs_next, **{"action": action_next}))
            critic_2_target_next = self.critic_target_network_2(dict(input_obs_next, **{"action": action_next}))
            critic_min_target_next = tf.math.minimum(critic_1_target_next, critic_2_target_next)

            # Calculate target values for critic
            target_critic = tf.expand_dims(reward, 1) + \
                            (critic_min_target_next - alpha * log_probs_next) * \
                            self.params.gamma * tf.cast(tf.expand_dims(1 - done, 1), reward.dtype)
            # Calculate critic loss
            critic_1_loss_value = tf.reduce_mean(tf.math.squared_difference(tf.stop_gradient(target_critic), critic_1))
            critic_2_loss_value = tf.reduce_mean(tf.math.squared_difference(tf.stop_gradient(target_critic), critic_2))

            # Play forward pass through actor
            action, means, log_probs = self.actor_network(input_obs)
            critic_1 = self.critic_network_1(dict(input_obs, **{"action": action}))
            critic_2 = self.critic_network_2(dict(input_obs, **{"action": action}))
            critic_min = tf.math.minimum(critic_1, critic_2)

            # Calculate actor loss
            actor_loss_value = tf.reduce_mean(alpha * log_probs - critic_min)

            # Calculate alpha loss
            if self.params.optimize_alpha_temp is True:
                alpha_loss = -tf.reduce_mean(self.log_alpha * tf.stop_gradient(log_probs_next - self.params.target_entropy))

            # Scale loss to avoid numerical issued on FP16
            if self.params.scale_loss is True:
                critic_1_loss_value = self.critic_1_optimizer.get_scaled_loss(critic_1_loss_value)
                critic_2_loss_value = self.critic_2_optimizer.get_scaled_loss(critic_2_loss_value)
                actor_loss_value = self.actor_optimizer.get_scaled_loss(actor_loss_value)
                alpha_loss = self.alpha_optimizer.get_scaled_loss(alpha_loss)

        # Get critic gradients
        critic_1_grads = tape.gradient(critic_1_loss_value, self.critic_network_1.trainable_variables)
        critic_2_grads = tape.gradient(critic_2_loss_value, self.critic_network_2.trainable_variables)
        actor_grads = tape.gradient(actor_loss_value, self.actor_network.trainable_variables)

        # Unscale gradients
        if self.params.scale_loss is True:
            critic_1_grads = self.critic_1_optimizer.get_unscaled_gradients(critic_1_grads)
            critic_2_grads = self.critic_1_optimizer.get_unscaled_gradients(critic_2_grads)
            actor_grads = self.actor_optimizer.get_unscaled_gradients(actor_grads)

        # Clip gradients
        if self.params.gradient_clipping == "ByValue":
            critic_1_grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in critic_1_grads]
            critic_2_grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in critic_2_grads]
            actor_grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in actor_grads]
        elif self.params.gradient_clipping == "ByNorm":
            critic_1_grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in critic_1_grads]
            critic_2_grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in critic_2_grads]
            actor_grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in actor_grads]
        elif self.params.gradient_clipping == "ByGlobalNorm":
            critic_1_grads, _ = tf.clip_by_global_norm(critic_1_grads, self.params.clip_value_max)
            critic_2_grads, _ = tf.clip_by_global_norm(critic_2_grads, self.params.clip_value_max)
            actor_grads, _ = tf.clip_by_global_norm(actor_grads, self.params.clip_value_max)

        # Apply gradients
        self.critic_1_optimizer.apply_gradients(zip(critic_1_grads, self.critic_network_1.trainable_variables))
        self.critic_2_optimizer.apply_gradients(zip(critic_2_grads, self.critic_network_2.trainable_variables))
        self.actor_optimizer.apply_gradients(zip(actor_grads, self.actor_network.trainable_variables))

        # Optimize alpha
        if self.params.optimize_alpha_temp is True:
            alpha_grad = tape.gradient(alpha_loss, [self.log_alpha])

            # Unscale gradients
            if self.params.scale_loss is True:
                alpha_grad = self.alpha_optimizer.get_unscaled_gradients(alpha_grad)

            self.alpha_optimizer.apply_gradients(zip(alpha_grad, [self.log_alpha]))
            alpha = tf.math.exp(self.log_alpha)

            if self.params.temp_alpha_max is not None:
                alpha = tf.minimum(alpha, self.params.temp_alpha_max)

        # Calculate metric for olicy entropy
        entropy = tf.reduce_mean(log_probs_next)

        return alpha, actor_loss_value, critic_1_loss_value, critic_2_loss_value, entropy
