from abc import abstractmethod
import tensorflow as tf
import numpy as np
from copy import deepcopy

from rllib.classes.policies.PolicyAbstract import PolicyAbstract
from rllib.functions.tensorflow import device_placement, get_weights, set_weights, soft_update_target_network
from rllib.functions.tensorflow import WeightedMSELoss
from rllib.classes.errors.ConfigurationError import ConfigurationError


class PolicyDDPGBase(PolicyAbstract):
    """
    Base class for DQN-based policies. The class implements the basic training and rollout functionality.
    The same network is used for the target and q-value function.
    The network architecture need to be added before use.
    """

    def __init__(self, params, *args, force_cpu_use=False, **kwargs):
        """
        Initialize policy

        :param params:          Parameter object
        :param force_cpu_use:   Flag indicating whether the policy should be force onto the CPU
        """
        super().__init__(params)

        # Set variables
        self.force_cpu_use=force_cpu_use

        # Create optimizer
        self.actor_optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.learning_rate_initial)
        self.critic_optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.learning_rate_initial)
        if self.params.scale_loss is True:
            self.actor_optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.actor_optimizer, "dynamic")
            self.critic_optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.critic_optimizer, "dynamic")

        # Create critic loss function
        if self.params.loss_function == "Huber":
            self.critic_loss = tf.keras.losses.Huber()
        elif self.params.loss_function == "MSE":
            self.critic_loss = WeightedMSELoss()
        else:
            raise ConfigurationError(self.params.loss_function, "Loss function not found")

        # Create networks and initilize weight variables
        self.init_networks()



    def init_networks(self, *args, **kwargs):
        """
        Creates neural networks and initilizes all weight variables
        """

        # Create network
        self.actor_network = device_placement(self.force_cpu_use, self.build_actor_network)
        self.critic_network = device_placement(self.force_cpu_use, self.build_critic_network)
        self.actor_target_network = device_placement(self.force_cpu_use, self.build_actor_network)
        self.critic_target_network = device_placement(self.force_cpu_use, self.build_critic_network)

        # Setup weight list
        actor_weights = get_weights(self.actor_network)
        critic_weights = get_weights(self.critic_network)

        self.actor_weights_dtype = actor_weights[0].dtype             # Get data type of network weights
        self.critic_weights_dtype = critic_weights[0].dtype           # Get data type of network weights

        self.weights = [deepcopy(actor_weights), deepcopy(critic_weights), deepcopy(actor_weights), deepcopy(critic_weights)]



    def get_weights(self, *args, **kwargs):
        """
        Returns a list of all network weights.

        :return: List of all network weights
        """
        super().get_weights()

        actor_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[0]]
        critic_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[1]]
        actor_target_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[2]]
        critic_target_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[3]]

        return [actor_weights, critic_weights, actor_target_weights, critic_target_weights]



    def set_weights(self, weights, *args, **kwargs):
        """
        Sets all network weights to the ones provided in a list

        :param weights: List of network weights
        """
        super().set_weights(weights)

        actor_weights = [tf.cast(w, self.actor_weights_dtype) for w in weights[0]]
        critic_weights = [tf.cast(w, self.critic_weights_dtype) for w in weights[1]]
        actor_target_weights = [tf.cast(w, self.actor_weights_dtype) for w in weights[2]]
        critic_target_weights = [tf.cast(w, self.critic_weights_dtype) for w in weights[3]]
        self.weights = [actor_weights, critic_weights, actor_target_weights, critic_target_weights]

        set_weights(self.actor_network, actor_weights)
        set_weights(self.critic_network, critic_weights)
        set_weights(self.actor_target_network, actor_target_weights)
        set_weights(self.critic_target_network, critic_target_weights)



    def run_policy(self, obs, *args, goal=None, policy_params=[], deploy=False, **kwargs):
        """
        Runs the policy with the provided parameters and returns a list of selected actions

        :param obs:             Numpy array of observations
        :param goal:            Numpy array with goal to achieve or None
        :param policy_params:   List of parameters for the policy (e.g., epsilon)
        :param deploy:          Flag indicating whether the policy should be run in deployment mode
        :return:                List of actions chosen by the policy
        """
        super().run_policy(obs, goal, policy_params, deploy)

        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        # Play forward pass
        feed_obs = self.get_feed_dict(obs, goal)
        actions = device_placement(self.force_cpu_use, self.actor_network, feed_obs)
        actions = actions.numpy()

        # Add gaussion noise for exploration
        if deploy is False:
            if np.random.rand() <= policy_params[0]:
                actions = np.random.randint(self.params.action_bounds[0], self.params.action_bounds[1], size=(1,1))

            #scale = (self.params.action_bounds[0] + self.params.action_bounds[1]) / 2
            #actions += tf.random.normal(shape=actions.shape, mean=0, stddev=scale*policy_params[0], dtype=actions.dtype)
            #tf.clip_by_value(actions, self.params.action_bounds[0], self.params.action_bounds[1])

        return actions



    def get_value_estimate(self, obs, *args, goal=None, **kwargs):
        """
        Returns estimates for the value/action-value function at the provided observation
        Only the q-network is used to reduce run time.

        :param obs:     Numpy array of observations
        :param goal:    Optional numpy array with goal to achieve or None
        :return:        List of estimates
        """
        super().get_value_estimate(obs, goal)

        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        # Play forward pass
        feed_obs = self.get_feed_dict(obs, goal)
        actions = device_placement(self.force_cpu_use, self.actor_network, feed_obs)

        critic_value = device_placement(self.force_cpu_use, self.critic_network, dict(**feed_obs, **{"action": actions}))
        #actions_target = device_placement(self.force_cpu_use, self.actor_target_network, feed_obs)
        #critic_value_target = device_placement(self.force_cpu_use, self.critic_target_network, dict(feed_obs, **{"action": actions_target}))

        info = {"q_values": critic_value,
                "t_values": critic_value}

        return info



    def train_policy(self,
                     episode,
                     minibatch,
                     *args,
                     learning_rate=None,
                     **kwargs):
        """
        Trains the policy with the provided samples

        :param episode:         Current episode in which training function is called
        :param minibatch:       List with data for the training of the policy     [obs, action, obs_next, reward, done, goal, weights, indices]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """
        super().train_policy(episode, minibatch, learning_rate)

        td_error_list = []
        actor_loss_list = []
        critic_loss_list = []
        index_list = []
        for k in range(self.params.num_q_updates):
            lb = self.params.batch_size * k
            ub = self.params.batch_size * (k+1)

            if minibatch[5] is not None:
                goal = minibatch[5][lb:ub]
            else:
                goal = None

            feed_obs = self.get_feed_dict(obs=minibatch[0][lb:ub], goal=goal)
            feed_obs_next = self.get_feed_dict(obs=minibatch[2][lb:ub], goal=goal)

            if self.params.PER is True:
                index_list.extend(minibatch[-1][lb:ub])
                weights = minibatch[-2][lb:ub]

            else:
                weights = None

            actor_loss_value,\
            critic_loss_value,\
            td_errors = self.update_policy_ddpg(feed_obs,
                                                minibatch[1][lb:ub],
                                                feed_obs_next,
                                                minibatch[3][lb:ub],
                                                minibatch[4][lb:ub],
                                                weights)

            td_error_list.extend(list(td_errors.numpy()))
            actor_loss_list.append(actor_loss_value)
            critic_loss_list.append(critic_loss_value)

            # Update target networks (soft)
            if self.params.soft_target_update is True and k % self.params.target_update_steps == 0:
                soft_update_target_network(self.params, self.actor_network, self.actor_target_network)
                soft_update_target_network(self.params, self.critic_network, self.critic_target_network)


        training_info = {"td_error_list": td_error_list,
                         "td_error_indices": index_list,
                         "td_error": np.mean(td_error_list),
                         "actor_loss": np.mean(actor_loss_list),
                         "critic_loss": np.mean(critic_loss_list)}

        print(np.mean(td_error_list), np.mean(actor_loss_list), np.mean(critic_loss_list))

        self.weights[0] = get_weights(self.actor_network)
        self.weights[1] = get_weights(self.critic_network)
        self.weights[2] = get_weights(self.actor_target_network)
        self.weights[3] = get_weights(self.critic_target_network)

        return training_info



    def build_actor_network(self):
        """
        Abstract method that returns the neural network architecture for the actor

        :return: Tensorflow/Keras network object
        """

        # Create network input
        pass



    def build_critic_network(self):
        """
        Abstract method that returns the neural network architecture for the critic

        :return: Tensorflow/Keras network object
        """

        # Create network input
        pass



    @abstractmethod
    def get_actionspace_dim(self, *args, **kwargs):
        """
        Abstract method that returns an integer value representing the actionspace dimension.
        Used by the run_policy method.

        :return: Integer representing the actionspace dimension
        """
        pass



    @abstractmethod
    def get_feed_dict(self, obs, *args, goal=None, **kwargs):
        """
        Abstract method that converts numpy arrays for observation and goal into dictionaries for each network input.

        :param obs:     Numpy array with observations
        :param goal:    Optional numpy array with goal
        :return:        Dictionary with key for each network input
        """
        pass



    @tf.function
    def update_policy_ddpg(self,
                           input_obs,
                           action,
                           input_obs_next,
                           reward,
                           done,
                           weights):
        """
        Conducts a single update of the actor and critic networks. This function is converted to a tensorflow graph to
        increase efficiency.

        :param input_obs:               Dict containing numpy arrays of observations for each network input before the transition
        :param actions:                 Numpy array containing the chosen actions
        :param input_obs_next:          Dict containing numpy arrays of observations for each network input after the transition
        :param reward:                  Numpy array containing the rewards for each transition
        :param done:                    Numpy array containing the done flags for each transition
        :return actor_loss_value:       Float representing the actor loss
        :return critic_loss_value:      Float representing the loss of critic
        :return td_errors:              Numpy array of td errors
        """

        # Reshape actions into 2 dimensional tensor
        action = tf.squeeze(action, axis=1)

        # Critic optimization
        with tf.GradientTape() as tape:
            critic = self.critic_network(dict(input_obs, **{"action": action}))

            action_target_next = self.actor_target_network(input_obs_next)
            critic_target_next = self.critic_target_network(dict(input_obs_next, **{"action": action_target_next}))
            target_critic = tf.expand_dims(reward, 1) + self.params.gamma * critic_target_next * tf.cast(tf.expand_dims(1 - done, 1), reward.dtype)

            td_errors = target_critic - critic
            critic_loss_value = tf.reduce_mean(tf.math.squared_difference(tf.stop_gradient(target_critic), critic))

        # Get critic gradients
        critic_grads = tape.gradient(critic_loss_value, self.critic_network.trainable_variables)

        # Unscale gradients
        if self.params.scale_loss is True:
            critic_grads = self.critic_optimizer.get_unscaled_gradients(critic_grads)

        # Clip gradients
        if self.params.gradient_clipping == "ByValue":
            critic_grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in critic_grads]
        elif self.params.gradient_clipping == "ByNorm":
            critic_grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in critic_grads]
        elif self.params.gradient_clipping == "ByGlobalNorm":
            critic_grads, _ = tf.clip_by_global_norm(critic_grads, self.params.clip_value_max)

        # Apply gradients
        self.critic_optimizer.apply_gradients(zip(critic_grads, self.critic_network.trainable_variables))


        # Actor optimization
        with tf.GradientTape() as tape:
            action_actor = self.actor_network(input_obs)
            critic = self.critic_network(dict(input_obs, **{"action": action_actor}))
            actor_loss_value = - tf.reduce_mean(critic)

        # Get actor gradients
        actor_grads = tape.gradient(actor_loss_value, self.actor_network.trainable_variables)

        # Unscale gradients
        if self.params.scale_loss is True:
            actor_grads = self.actor_optimizer.get_unscaled_gradients(actor_grads)

        # Clip gradients
        if self.params.gradient_clipping == "ByValue":
            actor_grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in actor_grads]
        elif self.params.gradient_clipping == "ByNorm":
            actor_grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in actor_grads]
        elif self.params.gradient_clipping == "ByGlobalNorm":
            actor_grads, _ = tf.clip_by_global_norm(actor_grads, self.params.clip_value_max)

        # Apply gradients
        self.actor_optimizer.apply_gradients(zip(actor_grads, self.actor_network.trainable_variables))

        return actor_loss_value, critic_loss_value, td_errors