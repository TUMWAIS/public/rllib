from abc import abstractmethod
import tensorflow as tf
import numpy as np
from copy import deepcopy

from rllib.classes.policies.PolicyAbstract import PolicyAbstract
from rllib.functions.tensorflow import device_placement, get_weights, set_weights
from rllib.functions.tensorflow import WeightedMSELoss
from rllib.classes.errors.ConfigurationError import ConfigurationError


class PolicyDQNBase(PolicyAbstract):
    """
    Base class for DQN-based policies. The class implements the basic training and rollout functionality.
    The same network is used for the target and q-value function.
    The network architecture need to be added before use.
    """

    def __init__(self, params, *args, force_cpu_use=False, **kwargs):
        """
        Initialize policy

        :param params:          Parameter object
        :param force_cpu_use:   Flag indicating whether the policy should be force onto the CPU
        """
        super().__init__(params)

        # Set variables
        self.force_cpu_use=force_cpu_use

        # Create optimizer
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.params.learning_rate_initial)
        if self.params.scale_loss is True:
            self.optimizer = tf.keras.mixed_precision.experimental.LossScaleOptimizer(self.optimizer, "dynamic")

        # Create loss function
        if self.params.loss_function == "Huber":
            self.loss = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.NONE)
        elif self.params.loss_function == "MSE":
            self.loss = WeightedMSELoss()
        else:
            raise ConfigurationError(self.params.loss_function, "Loss function not found")

        # Create networks and initilize weight variables
        self.init_networks()



    def init_networks(self, *args, **kwargs):
        """
        Creates neural networks and initializes all weight variables
        """

        # Create network
        self.q_network = device_placement(self.force_cpu_use, self.build_network)

        # Setup weight list
        weights = get_weights(self.q_network)
        self.network_weights_dtype = weights[0].dtype           # Get data type of network weights
        self.weights = [deepcopy(weights), deepcopy(weights)]



    def get_weights(self, *args, **kwargs):
        """
        Returns a list of all network weights.
        Weights can be converted to lower precision datatype in order to reduce transfer times and memory requirements

        :return: List of all network weights
        """
        super().get_weights()
        q_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[0]]
        t_weights = [tf.cast(w, self.params.transfer_dtype) for w in self.weights[1]]

        return [q_weights, t_weights]



    def set_weights(self, weights, *args, **kwargs):
        """
        Sets all network weights to the ones provided in a list
        Weights can be converted to lower precision datatype in order to reduce transfer times and memory requirements

        :param weights: List of network weights
        """
        super().set_weights(weights)

        q_weights = [tf.cast(w, self.network_weights_dtype) for w in weights[0]]
        t_weights = [tf.cast(w, self.network_weights_dtype) for w in weights[1]]
        self.weights = [q_weights, t_weights]

        set_weights(self.q_network, q_weights)



    def run_policy(self, obs, *args, goal=None, policy_params=None, deploy=False, debug=False, **kwargs):
        """
        Runs the policy with the provided parameters and returns a list of selected actions

        :param obs:             Numpy array of observations
        :param goal:            Numpy array with goal to achieve or None
        :param policy_params:   List of parameters for the policy (e.g., epsilon)
        :param deploy:          Flag indicating whether the policy should be run in deployment mode
        :param deploy:          Flag indicating whether to print debug information
        :return:                List of actions chosen by the policy
        """
        super().run_policy(obs, goal, policy_params, deploy)

        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        # Play forward pass
        feed_obs = self.get_feed_dict(obs, goal)
        q_values = device_placement(self.force_cpu_use, self.predict, self.q_network, feed_obs)

        # Create list with actions and q-values
        actions = self.policy(q_values)

        # Print debug infos
        if debug is True:
            print("--- Q Values ---")
            print(q_values)
            print("--- Actions ---")
            print(actions)

        # Replace action with probability of epsilon
        output_actions = []
        if self.params.egreedy_per_action is False:
            if deploy is False and policy_params is not None and np.random.rand() <= policy_params[0]:
                random_action = np.random.randint(self.get_actionspace_dim(feed_obs))
                for _ in actions:
                    output_actions.append(random_action)
            else:
                for a in actions:
                    output_actions.append(a[0])

        else:
            for a in actions:
                if deploy is False and policy_params is not None and np.random.rand() <= policy_params[0]:
                    output_actions.append(np.random.randint(self.get_actionspace_dim(feed_obs)))
                else:
                    output_actions.append(a[0])

        return output_actions



    def get_value_estimate(self, obs, *args, goal=None, include_t_value=False, **kwargs):
        """
        Returns estimates for the value/action-value function at the provided observation
        Only the q-network is used to reduce run time.

        :param obs:                 Numpy array of observations
        :param goal:                Optional numpy array with goal to achieve or None
        :param include_t_value:     Optional flag indicating whether to also get q-value estimates from the target network
        :return:                    Dictionary with q_values (and optional t_values)
        """
        super().get_value_estimate(obs, goal)

        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        # Get q values
        feed_obs = self.get_feed_dict(obs, goal)
        q_values = device_placement(self.force_cpu_use, self.predict, self.q_network, feed_obs)
        info = {"q_values": q_values.numpy()}

        # Get t values
        if include_t_value is True:
            set_weights(self.q_network, self.weights[1])
            t_values = device_placement(self.force_cpu_use, self.q_network, feed_obs)
            set_weights(self.q_network, self.weights[0])
        #info["t_values"] = q_values.numpy()[1]

        return info



    def train_policy(self,
                     episode,
                     minibatch,
                     *args,
                     learning_rate=None,
                     **kwargs):
        """
        Trains the policy with the provided samples

        :param episode:         Current episode in which training function is called
        :param minibatch:       List with data for the training of the policy     [obs, action, obs_next, reward, done, goal, weights, indices]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """
        super().train_policy(episode, minibatch, learning_rate)

        # Get actions from target network (avoids the need for a target network on the GPU)
        set_weights(self.q_network, self.weights[1])

        action_target = []
        feed_obs = []
        feed_obs_next = []
        for k in range(self.params.num_q_updates):
            lb = self.params.batch_size * k
            ub = self.params.batch_size * (k+1)

            if self.params.uvfa is True:
                goal = minibatch[5][lb:ub]
            else:
                goal = None

            feed_obs.append(self.get_feed_dict(obs=minibatch[0][lb:ub], goal=goal))
            feed_obs_next.append(self.get_feed_dict(obs=minibatch[2][lb:ub], goal=goal))
            action_target.append(self.predict(self.q_network, feed_obs_next[k]))

        set_weights(self.q_network, self.weights[0])


        # Update q-network
        td_error_list = []
        index_list = []
        for k in range(self.params.num_q_updates):
            lb = self.params.batch_size * k
            ub = self.params.batch_size * (k+1)

            if self.params.PER is True:
                index_list.extend(minibatch[-1][lb:ub])
                weights = minibatch[-2][lb:ub]
            else:
                weights = None

            td_error = self.update_q_network(feed_obs[k],
                                            minibatch[1][lb:ub],
                                            feed_obs_next[k],
                                            minibatch[3][lb:ub],
                                            minibatch[4][lb:ub],
                                            weights,
                                            action_target[k])

            td_error_list.append(td_error)              # Don't convert from tf to numpy in loop due to overhead

        td_error_list = np.array(td_error_list).flatten()

        training_info = {"td_error_list": td_error_list,        # For PER
                         "td_error_indices": index_list,        # For PER
                         "td_error": np.mean(td_error_list),
                         "td_error_min": np.min(td_error_list),
                         "td_error_max": np.max(td_error_list)}

        self.weights[0] = get_weights(self.q_network)

        # Hard target update
        if self.params.soft_target_update is False and episode % self.params.target_update_episodes == 0:
            self.weights[1] = deepcopy(self.weights[0])

        return training_info



    @abstractmethod
    def build_network(self, *args, **kwargs):
        """
        Abstract method that returns the neural network architecture for the q- and target network

        :return: Tensorflow/Keras network object
        """
        pass



    @abstractmethod
    def get_actionspace_dim(self, feed_obs, *args, **kwargs):
        """
        Abstract method that returns an integer value representing the actionspace dimension.
        Used by the run_policy method.

        :return: Integer representing the actionspace dimension
        """
        pass



    @abstractmethod
    def get_feed_dict(self, obs, *args, goal=None, **kwargs):
        """
        Abstract method that converts numpy arrays for observation and goal into dictionaries for each network input.

        :param obs:     Numpy array with observations
        :param goal:    Optional numpy array with goal
        :return:        Dictionary with key for each network input
        """
        pass



    @tf.function
    def predict(self, model, input):
        """
        Custom predict function.
        The Keras function has high overhead and can cause memory leaks.
        Direct calling of the model seems to be slower and can also cause memory leaks.

        :param model:   Model to create prediction from
        :param input:   Numpy array or feed dict for prediction
        :return:        Tensor with prediction
        """
        output = model(input, training=False)

        for o in output:
            if len(o.shape) < 3:
                o = tf.expand_dims(o, axis=0)

        return output



    @tf.function
    def policy(self, q_values):
        """
        Core policy function, which is used for rollout and training.

        :param q_values:    Numpy array with q values
        :return:            Numpy array with actions
        """
        action = tf.math.argmax(q_values,
                                axis=-1,
                                output_type=tf.int32)
        return action



    @tf.function
    def update_q_network(self,
                         input_obs,
                         action,
                         input_obs_next,
                         reward,
                         done,
                         weights,
                         action_target):
        """
        Function for updating a single and multi-output Q-network.
        Function is converted into a tensorflow graph in order to improve GPU acceleration

        :param input_obs:       Dict containing numpy arrays of observations before transition for each network input
        :param action:          Numpy array of actions
        :param input_obs_next:  Dict containing numpy arrays of observations after transition for each network input
        :param reward:          Numpy array of reward for transition
        :param done:            Numpy array of flags indication of trajectory has been completed
        :param weights:         Numpy array of importance sampling weights for PER
        :param action_target:   Numpy array of with the target values for the q-function
        :return:                Numpy array of TD_Errors
        """

        # Calculate next q values
        if self.params.double_q_learning is True:
            q_values_next = self.q_network(input_obs_next)
        else:
            q_values_next = action_target

        # Add third dimension in case of single output network
        if len(tf.shape(q_values_next)) < 3:
            q_values_next = tf.expand_dims(q_values_next, 0)

        # Calculate next action
        action_from_network = self.policy(q_values_next)

        # Reshape matrices from [batch_size, num_outputs] to [num_outputs, batch_size]
        action = tf.cast(tf.transpose(action), tf.int32)
        reward = tf.transpose(reward, [1, 0])
        done = tf.expand_dims(done, axis=0)

        # Get target value
        target = tf.cast(reward, q_values_next.dtype) + self.params.gamma * tf.gather_nd(action_target, tf.expand_dims(action_from_network, -1), batch_dims=2) * tf.cast((1 - done), q_values_next.dtype)

        # Insert target q-values into return of q-network
        with tf.GradientTape() as tape:
            # Get logits
            q_values = self.q_network(input_obs)
            if len(tf.shape(q_values)) < 3:                                                                 # Add third dimension in case of single output network
                q_values = tf.expand_dims(q_values, 0)

            # Copy weights for each action in a transition
            if weights is not None:
                num_outputs = [tf.shape(target)[0]]
                weight_matrix = tf.reshape(tf.tile(weights, num_outputs), [num_outputs[0], tf.shape(weights)[0]])
            else:
                weight_matrix = None

            # Calculate gradients
            q_value = tf.gather_nd(q_values, tf.expand_dims(action, -1), batch_dims=2)
            loss_value = self.loss(tf.stop_gradient(target), q_value, weight_matrix)

            # Scale loss to avoid numerical issued on FP16
            if self.params.scale_loss is True:
                loss_value = self.optimizer.get_scaled_loss(loss_value)

        grads = tape.gradient(loss_value, self.q_network.trainable_variables)

        # Unscale gradients
        if self.params.scale_loss is True:
            grads = self.optimizer.get_unscaled_gradients(grads)

        # Calculate td errors (e.g. for Per)
        td_error = tf.reduce_sum(tf.abs(target - q_value), axis=0)

        # Clip gradients
        if self.params.gradient_clipping == "ByValue":
            grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in grads]
        elif self.params.gradient_clipping == "ByNorm":
            grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in grads]
        elif self.params.gradient_clipping == "ByGlobalNorm":
            grads, _ = tf.clip_by_global_norm(grads, self.params.clip_value_max)

        # Update Q network with gradients
        self.optimizer.apply_gradients(zip(grads, self.q_network.trainable_variables))

        return td_error