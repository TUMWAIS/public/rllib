import tensorflow as tf
import numpy as np

from rllib.classes.policies.PolicyAbstract import PolicyAbstract
from rllib.classes.policies.PolicyVPGBase import PolicyVPGBase
from rllib.functions.tensorflow import device_placement, get_weights


class PolicyA2CBase(PolicyVPGBase):
    """
    Base class for A2C policies. The class implements the basic training and rollout functionality.
    The same network is used for the actor and critic.
    The network architecture need to be added before use.
    """

    def actor_critic_predict(self, obs, *args, goal=None, **kwargs):
        """
        Executes a forward pass through the actor and critic network and returns logits and values

        :param obs:     Numpy array of observations
        :param goal:    Optional numpy array with goal to achieve
        :return logits: Numpy array with output of the actor network
        :return values: Numpy array with output of the critic network
        """

        # Create dictionary with observations for each neural network input
        feed_obs = self.get_feed_dict(obs, goal)

        logits, values = device_placement(self.force_cpu_use, self.predict, self.network, feed_obs)

        return logits, values



    def actor_predict(self, obs, *args, goal=None, **kwargs):
        """
        Executes a forward pass through the actor network and returns logits

        :param obs:     Numpy array of observations
        :param goal:    Optional numpy array with goal to achieve
        :return logits: Numpy array with output of the actor network
        """

        logits, values = self.actor_critic_predict(obs, goal)

        return logits



    def critic_predict(self, obs, *args, goal=None, **kwargs):
        """
        Executes a forward pass through the critic network and returns values

        :param obs:     Numpy array of observations
        :param goal:    Optional numpy array with goal to achieve
        :return values: Numpy array with output of the critic network
        """

        logits, values = self.actor_critic_predict(obs, goal)

        return values



    def get_value_estimate(self, obs, *args, goal=None, **kwargs):
        """
        Returns estimates for the value/action-value function at the provided observation

        :param obs:     Numpy array of observations
        :param goal:    Numpy array with goal to achieve or None
        :return:        Dictionary with values
        """
        PolicyAbstract.get_value_estimate(self, obs, goal)  # Call grandparent without parent
        
        # Check if observation/goal has shape [1, num_obs_vars], i.e, observation variables are columns
        if obs.ndim == 1:
            obs = obs.reshape(1, obs.shape[0])

        if goal is not None and goal.ndim == 1:
            goal = goal.reshape(1, goal.shape[0])

        info = {"values": self.critic_predict(obs, goal)}

        return info



    def train_policy(self, 
                     episode,
                     minibatch,
                     *args,
                     learning_rate=None,
                     **kwargs):
        """
        Trains the policy with the provided samples

        :param episode:         Current episode in which training function is called
        :param minibatch:       List with data for the training of the policy    [obs, action, obs_next, reward, done, goal]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """
        PolicyAbstract.train_policy(self, episode, minibatch, learning_rate)  # Call grandparent without parent

        # Read minibatch
        obs = minibatch[0]
        action = minibatch[1]
        obs_next = minibatch[2]
        reward = minibatch[3]
        done = minibatch[4]

        if len(minibatch) > 5:
            goal = minibatch[5]
        else:
            goal = None

        feed_obs = self.get_feed_dict(obs=obs, goal=goal)
        action = np.squeeze(action).astype("int32")

        # Get logits and values
        logits, values = self.actor_critic_predict(obs, goal)
        values_next = self.critic_predict(obs_next, goal)       # Not efficient, but simple to implement

        values = values.numpy().flatten()
        values_next = values_next.numpy().flatten()

        # Get Generalized Advantage Estimator and target values
        psi, target_values = self.get_gae_psi(values,
                                              values_next,
                                              reward,
                                              done,
                                              self.params.gamma,
                                              self.params.lambda_,
                                              self.params.baseline,
                                              self.params.psi_scaling,
                                              self.params.transfer_dtype)

        # Update policy
        actor_loss, critic_loss, entropy_loss = self.update_policy_a2c(feed_obs,
                                                                       action,
                                                                       psi,
                                                                       target_values)

        training_info = {"actor_loss": actor_loss,
                         "critic_loss": critic_loss,
                         "entropy_loss": entropy_loss}

        self.weights[0] = get_weights(self.network)

        return training_info



    def get_gae_psi(self,
                    values,
                    values_next,
                    rewards,
                    done,
                    gamma,
                    lamda_,
                    *args,
                    baseline=False,
                    psi_scaling=False,
                    type="float32",
                    **kwargs):
        """
        Calculates the Generalized Advantages,

        :param values:          A list of Values at time step t
        :param values_next:     A list of Valutes at time step t+1
        :param rewards:         A list of rewards
        :param done:            A list of flags whether the trajectory is finished
        :param gamma:           Discount parameter to calculate the mean discounted rewards
        :param lamda_:          Lamda parameter for GAE
        :param type:            Data type of return values
        :return:                List of generalized advantages
        """
        psi = []
        gae = 0
        for i in reversed(range(len(values))):
            delta = rewards[i] + gamma * values_next[i] * (1 - done[i]) - values[i]
            gae = (gae * gamma * lamda_ * (1 - done[i])) + delta
            psi.append(gae)

        psi.reverse()
        psi = np.array(psi).astype(type)
        value_targets = psi + values

        if baseline is True:
            psi -= np.mean(psi)

        if psi_scaling is True:
            psi /= np.std(psi)

        return psi, value_targets



    @tf.function(experimental_relax_shapes=True)
    def update_policy_a2c(self,
                          input_obs,
                          actions,
                          psi,
                          target_values):
        """
        Conducts a single update of the actor-critic network. This function is converted to a tensorflow graph to
        increase efficiency.

        :param input_obs:          Dict containing numpy arrays of observations after transition for each network input
        :param actions:            Numpy array containing the chosen actions
        :param psi:                Numpy array containing the generalized advantage weights
        :param target_values:      Numpy array containing the target values for the critic head
        :return actor_loss:        Float representing the actor loss
        :return critic_loss:       Float representing the critic loss
        :return entropy_loss:      Float representing the entropy loss
        """

        with tf.GradientTape() as tape:
            action_dist, values = self.network(input_obs)

            # Calculate actor loss
            idx = tf.range(tf.shape(actions)[0], dtype=tf.int32)
            neg_loglikelihoods = -tf.math.log(tf.gather_nd(action_dist, tf.stack([idx, actions], axis=1)))
            actor_loss = tf.math.reduce_mean(neg_loglikelihoods * psi)

            # Calculate critic loss
            critic_error = tf.math.square(values - target_values)
            critic_loss = tf.math.reduce_mean(critic_error) * self.params.critic_discount

            # Calculate entropy loss (to improve exploration)
            entropy_loss = -tf.math.reduce_mean(-tf.math.exp(-neg_loglikelihoods) * -neg_loglikelihoods) * self.params.entropy_beta

            # Aggregate loss
            if self.params.agreggrate_loss is False:
                loss_value = {"actor_head": actor_loss + entropy_loss, "critic_head": critic_loss}
            else:
                loss_value = actor_loss + critic_loss + entropy_loss

            # Scale loss to avoid numerical issued on FP16
            if self.params.scale_loss is True:
                loss_value = self.optimizer.get_scaled_loss(loss_value)

        grads = tape.gradient(loss_value, self.network.trainable_variables)

        # Unscale gradients
        if self.params.scale_loss is True:
            grads = self.optimizer.get_unscaled_gradients(grads)

        # Clip gradients
        if self.params.gradient_clipping == "ByValue":
            grads = [None if grad is None else tf.clip_by_value(grad, self.params.clip_value_min, self.params.clip_value_max) for grad in grads]
        elif self.params.gradient_clipping == "ByNorm":
            grads = [None if grad is None else tf.clip_by_norm(grad, self.params.clip_value_max) for grad in grads]
        elif self.params.gradient_clipping == "ByGlobalNorm":
            grads, _ = tf.clip_by_global_norm(grads, self.params.clip_value_max)

        self.optimizer.apply_gradients(zip(grads, self.network.trainable_variables))

        return actor_loss, critic_loss, entropy_loss

