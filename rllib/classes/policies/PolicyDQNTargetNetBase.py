import numpy as np
from copy import deepcopy

from rllib.classes.policies.PolicyDQNBase import PolicyDQNBase
from rllib.classes.policies.PolicyAbstract import PolicyAbstract
from rllib.functions.tensorflow import device_placement, get_weights, set_weights, soft_update_target_network


class PolicyDQNTargetNetBase(PolicyDQNBase):
    """
    Base class for DQN-based policies. The class implements separate networks for the
    target and q-value function.
    """


    def init_networks(self, *args, **kwargs):
        """
        Creates neural networks and initilizes all weight variables
        """
        super().init_networks(*args, **kwargs)

        # Create additional target network
        self.target_network = device_placement(self.force_cpu_use, self.build_network)
        set_weights(self.target_network, self.weights[1])



    def set_weights(self, weights, *args, **kwargs):
        """
        Sets all network weights to the ones provided in a list

        :param weights: List of network weights
        """
        super().set_weights(weights)

        set_weights(self.target_network, self.weights[1])



    def train_policy(self,
                     episode,
                     minibatch,
                     *args,
                     learning_rate=None,
                     **kwargs):
        """
        Trains the policy with the provided samples

        :param episode:         Current episode in which training function is called
        :param minibatch:       List with data for the training of the policy     [obs, action, obs_next, reward, done, goal, weights, indices]
        :param learning_rate:   Optional learning rate for the training
        :return training_info:  Dictionary containing training statistics
        """
        PolicyAbstract.train_policy(self, episode, minibatch, learning_rate)  # Call method of abstract class

        # Create feed dicts
        feed_obs = []
        feed_obs_next = []
        for k in range(self.params.num_q_updates):
            lb = self.params.batch_size * k
            ub = self.params.batch_size * (k+1)

            if self.params.uvfa is True:
                goal = minibatch[5][lb:ub]
            else:
                goal = None

            feed_obs.append(self.get_feed_dict(obs=minibatch[0][lb:ub], goal=goal))
            feed_obs_next.append(self.get_feed_dict(obs=minibatch[2][lb:ub], goal=goal))


        # Update q-network
        td_error_list = []
        index_list = []
        for k in range(self.params.num_q_updates):
            lb = self.params.batch_size * k
            ub = self.params.batch_size * (k+1)

            if self.params.uvfa is True:
                goal = minibatch[5][lb:ub]
            else:
                goal = None

            if self.params.PER is True:
                index_list.extend(minibatch[-1][lb:ub])
                weights = minibatch[-2][lb:ub]
            else:
                weights = None

            feed_obs = self.get_feed_dict(obs=minibatch[0][lb:ub], goal=goal)
            feed_obs_next = self.get_feed_dict(obs=minibatch[2][lb:ub], goal=goal)
            action_target = self.predict(self.target_network, feed_obs_next)

            td_error = self.update_q_network(feed_obs,
                                             minibatch[1][lb:ub],
                                             feed_obs_next,
                                             minibatch[3][lb:ub],
                                             minibatch[4][lb:ub],
                                             weights,
                                             action_target)

            td_error_list.append(td_error)              # Don't convert from tf to numpy in loop due to overhead

            # Soft target network update
            if self.params.soft_target_update is True and (k+1) % self.params.target_update_steps == 0:
                soft_update_target_network(self.params, self.q_network, self.target_network)

        td_error_list = np.array(td_error_list).flatten()

        training_info = {"td_error_list": td_error_list,        # For PER
                         "td_error_indices": index_list,        # For PER
                         "td_error": np.mean(td_error_list),
                         "td_error_min": np.min(td_error_list),
                         "td_error_max": np.max(td_error_list)}

        self.weights[0] = get_weights(self.q_network)
        self.weights[1] = get_weights(self.target_network)

        # Hard target update
        if self.params.soft_target_update is False and episode % self.params.target_update_episodes == 0:
            self.weights[1] = deepcopy(self.weights[0])

        return training_info