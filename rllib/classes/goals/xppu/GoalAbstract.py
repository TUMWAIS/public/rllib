from abc import ABC, abstractmethod


class GoalAbstract(ABC):
    """
    Abstract goal class for xppusim
    """

    def __init__(self,
                 state_space_mapping,
                 info):
        """
        Initializes the goal object

        :param state_space_mapping: Dictionary containing the state space mapping of the environment
        :param info:   Info dict of the environment after initialization
        """
        self.max_reward = 0
        self.desired_goal = None
        self.achieved_goal = None

        self.state_space_mapping = state_space_mapping
        self.goal_space_mapping, self.shape = self._get_goal_space_mapping(info)



    @abstractmethod
    def reset(self, observation, info):
        """
        Resets the goal object before a new trajectory is started.
        Can be used to set the desired goal and maximum achievable reward.

        :param observation:     Numpy array representing the current environment state
        :param info:            Info dict of the environment
        :return achieved_goal:  Numpy array representing the currently achieved goal
        :return desired_goal:   Numpy array representing the desired goal
        """
        pass



    @abstractmethod
    def update(self, observation, info):
        """
        Updates the goal object after each transition.
        Can be used to update the achieved goal

        :param observation:     Observation vector for the current simulation state
        :param info:            Info dictionary for the current simulation state
        :return achieved_goal:  Numpy array with the achieved goal
        :return desired_goal:   Numpy array representing the desired goal
        """
        pass



    @abstractmethod
    def compute_reward(self, prev_achieved_goal, achieved_goal, desired_goal, info):
        """
        Returns the reward for the transition defined by prev_achieved_goal, achieved_goal, and desired_goal

        :param prev_achieved_goal:  Numpy array of achieved goal before transition
        :param achieved_goal:       Numpy array of achieved goal after transition
        :param desired_goal:        Numpy array of desired goal
        :param info:                Info dictionary after transition
        :return reward:             Integer represention reward for the transition
        :return done:               Boolean value indicating whether the trajectory is finished
        :return info:               Updated info dict (reward_msg, done_msg)
        """
        pass



    @abstractmethod
    def _get_goal_space_mapping(self, info):
        """
        Returns a dictionary that maps the indices of the goal vector into goal groups as well as a
        tuple describing the shape of the goal.

        ["system"]
            [Goal variable]: [Index in the goal vector]
        ["workpiece"]
            [Workpiece index]
                [Goal variable]: [Index in the goal vector]


        :return goal_space_mapping:     Dictionary containing the mapping
        :return shape:                  Tuple containing the shape of the goal
        """
        goal_space_mapping = {}
        goal_space_mapping["workpiece"] = []
        goal_space_mapping["system"] = {}
        shape = None

        return goal_space_mapping, shape



    def get_reward_state(self):
        """
        Returns the state of the reward function if it is stateful.

        :return: Float representing the reward state
        """
        pass