from rllib.classes.goals.xppu.DistanceGoal import DistanceGoal


class DistanceSparseGoal(DistanceGoal):
    """
    Sparse reward based on coordinates.

    The class is initialized with a list of position names as goals. If
    multiple goals are provided, a new one is sampled at the beginning of each
    trajectory.

    This goal class only works with a single workpiece!
    """

    def get_onetime_reward(self, prev_achieved_goal, achieved_goal, desired_goal, reward, info):
        """
        Calculate the final reward for achieving the goal

        :param prev_achieved_goal:  Numpy array of achieved goal before transition
        :param achieved_goal: Numpy array representing the achieved goal
        :param desired_goal:  Numpy array representing the desired goal
        :param reward:        Current reward
        :param info:          Info dictionary
        :return reward:       Reward including the final reward
        :return done:         Boolean flag indicating whether the desired goal has ben achieved
        :return info:         Info dictionary including the done_msg
        """
        done = False

        if self.goal_achieved(achieved_goal, desired_goal) is True:
            reward += self._final_reward + self._distance_reward
            done = True
            info["done_msg"] = "Task solved"

        elif info["error_msg"] is not None:
            reward -= self._error_penalty
            done = True
            info["done_msg"] = info["error_msg"]

        return reward, done, info



    def get_distance_reward(self, prev_achieved_goal, achieved_goal, desired_goal):
        """
        Calculates the distance reward. A reward is given for reducing the euclidean distance to the goal. The reward
        for a given distance is only received once. There is no penalty for increasing the distance

        :param prev_achieved_goal:  Numpy array representing the achieved goal from the previous timestep
        :param achieved_goal:       Numpy array representing the achieved goal
        :param desired_goal:        Numpy array representing the desired goal
        :return:                    Reward for current distance
        """
        reward = 0

        return reward