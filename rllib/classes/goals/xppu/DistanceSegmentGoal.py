import numpy as np

from rllib.classes.goals.xppu.DistanceGoal import DistanceGoal


class DistanceSegmentGoal(DistanceGoal):
    """
    Coordinate-based goal with distance reward (0.5) and finale reward for
    reaching the goal (0.5). The distance-based reward is calculated with
    the euclidean norm and allocated once the distance to the goal is reduced.
    Increasing the distance (again) does not lead to negative reward, but the
    already obtained reward cannot be obtained again.

    The distance reward depends on previous visited states and breaks the main
    assumption of MDPs (It still works). As a result, this goal cannot be used
    with HER.

    The class is initialized with a list of position names as goals. If
    multiple goals are provided, a new one is sampled at the beginning of each
    trajectory.

    This goal class only works with a single workpiece!
    """

    def __init__(self,
                 state_space_mapping,
                 info,
                 location_str_list,
                 scaling_factor = 1,
                 error_penalty=0,
                 action_penalty_type=None,
                 action_penalty=None,
                 unnormalized_compare=False,
                 add_reward_as_input=False,
                 tolerance = 0.002):
        """
        Initializes the goal class

        :param state_space_mapping:     Dictionary containing the state space mapping of the environment
        :param info:                    Info dict of the environment after initialization
        :param location_str_list:       List of position names as goals. Need to be part of env._info[system_positions]
        :param scaling_factor:          Integer for scaling the reward. If not provided, the max. reward is 1
        :param action_penalty_type:     String indicating the type of penalty the agent gets for certain actions (non_idle, first_time, non_idle_with_time)
        :param action_penalty:          Integer representing the penalty for certain actions
        :param error_penalty:           Integer representing the penalty for errors
        :param Tolerance:               Tolerance for comparing workpiece position with the goal
        """
        super().__init__(state_space_mapping, info, location_str_list, scaling_factor, error_penalty, action_penalty_type, action_penalty, unnormalized_compare, tolerance)

        # Set custom parameters
        self._distance_segment = 1
        self._add_reward_as_input = add_reward_as_input



    def reset(self, observation, info, goal=None):
        """
        Resets the goal class before a new trajectory is started.
        In particular, the desired goal and the maximum possible reward is initialized.
        """
        super().reset(observation, info, goal)

        # Reset distance segment and calculate total distance based on initial workpiece position
        self._distance_segment = 1

        return np.array(self.achieved_goal), np.array(self.desired_goal)



    def get_distance_reward(self, prev_achieved_goal, achieved_goal, desired_goal):
        """
        Calculates the distance reward. A reward is given for reducing the euclidean distance to the goal. The reward
        for a given distance is only received once. There is no penalty for increasing the distance

        :param distance:    Current distance to the goal
        :return:            Reward for current distance
        """
        distance = self.calculate_distance(achieved_goal, desired_goal)
        norm_distance = distance / self._total_distance

        reward = 0
        if self._distance_segment - norm_distance > 0:
            improvement = self._distance_segment - norm_distance
            reward = improvement * self._distance_reward
            self._distance_segment -= improvement

        return reward



    def get_reward_state(self):
        """
        Returns the current state of the achievment-based distance goal to be used as network input.

        :return: Float representing the reward state
        """
        if self._add_reward_as_input is True:
            return self._distance_segment
        else:
            return None