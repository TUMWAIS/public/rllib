import numpy as np
from math import sqrt

from rllib.classes.goals.xppu.GoalAbstract import GoalAbstract


class DistanceGoal(GoalAbstract):
    """
    Coordinate-based goal with distance reward (0.5) and finale reward for
    reaching the goal (0.5). The distance based reward is proportional to the
    distance change of the transition.

    The class is initialized with a list of position names as goals. If
    multiple goals are provided, a new one is sampled at the beginning of each
    trajectory.

    This goal class only works with a single workpiece!
    """

    def __init__(self,
                 state_space_mapping,
                 info,
                 location_str_list,
                 scaling_factor = 1,
                 error_penalty=0,
                 action_penalty_type=None,
                 action_penalty=None,
                 unnormalized_compare=False,
                 tolerance = 0.002
                 ):
        """
        Initializes the goal class

        :param state_space_mapping:     Dictionary containing the state space mapping of the environment
        :param info:                    Info dict of the environment after initialization
        :param location_str_list:       List of position names as goals. Need to be part of env._info[system_positions]
        :param scaling_factor:          Integer for scaling the reward. If not provided, the max. reward is 1
        :param action_penalty_type:     String indicating the type of penalty the agent gets for certain actions (non_idle, first_time, non_idle_with_time)
        :param action_penalty:          Integer representing the penalty for certain actions
        :param error_penalty:           Integer representing the penalty for errors
        :param Tolerance:               Tolerance for comparing workpiece position with the goal
        """
        super().__init__(state_space_mapping, info)

        # Set custom parameters
        self._distance_reward = 0.5
        self._final_reward = 0.5
        self._scaling_factor = scaling_factor
        self._action_penalty_type = action_penalty_type
        self._action_penalty = action_penalty
        self._error_penalty = error_penalty
        self._TOLERANCE = tolerance
        self._unnormalized_compare = unnormalized_compare

        self._total_distance = 0
        self.desired_goal_name = None

        # Create goal space to sample from
        self.goal_space = {}
        system_goals = self.get_system_goals()
        for location_str in location_str_list:
            if len(system_goals) > 0:
                for system_goal_name, system_goal in system_goals.items():
                    goal = info["system_positions"][location_str].copy()
                    for system_var_name in self.goal_space_mapping["system"].keys():
                        goal.append(system_goal[system_var_name])
                    self.goal_space[location_str + "_" + system_goal_name] = np.array(goal)
            else:
                goal = info["system_positions"][location_str].copy()
                self.goal_space[location_str] = np.array(goal)



    def get_system_goals(self):
        """
        Returns a dictionary of system goals that are represented as dictionaries mapping goal_variables to values. The
        names of the goal variables need to match the ones from the system part of the goal_space_mapping.
        {System_Goal_1: {GoalVariableName_1: Value}, System_Goal_2: {GoalVariableName_2: Value}}

        :return:    Dictionary of system goals
        """
        system_goals = {}

        return system_goals



    def reset(self, observation, info, goal=None):
        """
        Resets the goal class before a new trajectory is started.
        In particular, the desired goal and the maximum possible reward is initialized.

        :param observation:     Observation vector for the current simulation state
        :param info:            Info dictionary for the current simulation state
        :param goal:            Optional numpy array representing the goal to be achieved
        :return achieved_goal:  Numpy array with the achieved goal
        :return desired_goal:   Numpy array representing the desired goal
        """
        super().reset(observation, info)

        # Set provided goal or sample new goals until goal != initial position
        if goal is None:
            while True:
                self.desired_goal_name = np.random.choice(list(self.goal_space.keys()))
                self.desired_goal = self.goal_space[self.desired_goal_name]

                self.achieved_goal, self.desired_goal = self.update(observation, info)

                if self.goal_achieved(self.achieved_goal, self.desired_goal) is False:
                    break
        else:
            self.desired_goal = goal
            self.achieved_goal, self.desired_goal = self.update(observation, info)

        self.max_reward = (self._distance_reward + self._final_reward) * self._scaling_factor

        # Calculate total distance based on initial workpiece position
        self._total_distance = self.calculate_distance(self.achieved_goal, self.desired_goal)

        return np.array(self.achieved_goal), np.array(self.desired_goal)



    def update(self, observation, info):
        """
        Updates the achieved goal after each transition

        :param observation:     Observation vector for the current simulation state
        :param info:            Info dictionary for the current simulation state
        :return achieved_goal:  Numpy array with the achieved goal
        :return desired_goal:   Numpy array representing the desired goal
        """
        super().update(observation, info)

        self.achieved_goal = self.get_achieved_goal(observation, info)

        return np.array(self.achieved_goal), np.array(self.desired_goal)



    def compute_reward(self, prev_achieved_goal, achieved_goal, desired_goal, info):
        """
        Returns the reward for the transition defined by prev_achieved_goal, achieved_goal, and desired_goal

        :param prev_achieved_goal:  Numpy array of achieved goal before transition
        :param achieved_goal:       Numpy array of achieved goal after transition
        :param desired_goal:        Numpy array of desired goal
        :param info:                Info dictionary after transition
        :return reward:             Integer represention reward for the transition
        :return done:               Boolean value indicating whether the trajectory is finished
        :return info:               Updated info dict (reward_msg, done_msg, max_reward)
        """
        reward = 0
        info = info.copy()  # Don't change original dict (necessary for HER)
        info["done_msg"] = ""

        # Calculate distance-based reward
        reward += self.get_distance_reward(prev_achieved_goal, achieved_goal, desired_goal)

        # Add final reward and set done flag
        reward, done, info =  self.get_onetime_reward(prev_achieved_goal, achieved_goal, desired_goal, reward, info)

        # Add goal-specific metric to info dict
        distance = self.calculate_distance(achieved_goal, desired_goal)
        info["distance"] = max(distance / self._total_distance, 0)
        info["max_reward"] = self.max_reward

        # Add penalty for executing certain actions
        if self._action_penalty_type == "non_idle":
            if info["action_name"] != "DO_Nothing":
                reward -= self._action_penalty

        elif self._action_penalty_type == "first_time":
            if isinstance(info["action_name"], str):
                if info["action_name"] != info["prev_action_name"]:
                    reward -= self._action_penalty
            else:
                if (np.array(info["action_name"]) == np.array(info["prev_action_name"])).all():     # True/False does not work here
                    reward -= self._action_penalty

        elif self._action_penalty_type == "non_idle_with_time":
            if info["action_name"] != "DO_Nothing":
                reward -= self._action_penalty[0]
            else:
                reward -= self._action_penalty[1]

        # Scale reward
        reward *= self._scaling_factor

        return reward, done, info



    def _get_goal_space_mapping(self, info):
        """
        Returns a dictionary that maps the indices of the goal vector into goal groups as well as a
        tuple describing the shape of the goal.

        ["system"]
            [Goal variable]: [Index in the goal vector]
        ["workpiece"]
            [Workpiece index]
                [Goal variable]: [Index in the goal vector]


        :return goal_space_mapping:     Dictionary containing the mapping
        :return shape:                  Tuple containing the shape of the goal
        """
        goal_space_mapping, shape = super()._get_goal_space_mapping(info)

        goal_space_mapping["workpiece"].append({"x": 0,
                                                "y": 1,
                                                "z": 2})
        shape = (3,)

        return goal_space_mapping, shape



    def get_goal_space(self):
        """
        Returns the goal space

        :return:    Dictionary with potential goals to be sampled
        """
        return self.goal_space



    def get_system_goal_values(self):
        """
        Returns a dictionary with the values for the desired system goals.

        :return: Dictionary with values for system goals
        """
        system_goal_values = {}

        return system_goal_values



    def get_onetime_reward(self, prev_achieved_goal, achieved_goal, desired_goal, reward, info):
        """
        Calculate the final reward for achieving the goal

        :param prev_achieved_goal:  Numpy array representing the achieved goal from the previous timestep
        :param achieved_goal: Numpy array representing the achieved goal
        :param desired_goal:  Numpy array representing the desired goal
        :param reward:        Current reward
        :param info:          Info dictionary
        :return reward:       Reward including the final reward
        :return done:         Boolean flag indicating whether the desired goal has ben achieved
        :return info:         Info dictionary including the done_msg
        """
        done = False

        if self.goal_achieved(achieved_goal, desired_goal) is True:
            reward += self._final_reward
            done = True
            info["done_msg"] = "Task solved"

        elif info["error_msg"] is not None:
            reward -= self._error_penalty
            done = True
            info["done_msg"] = info["error_msg"]
        else:
            info["done_msg"] = ""

        return reward, done, info



    def get_distance_reward(self, prev_achieved_goal, achieved_goal, desired_goal):
        """
        Calculates the distance reward. A reward is given for reducing the euclidean distance to the goal. The reward
        for a given distance is only received once. There is no penalty for increasing the distance

        :param prev_achieved_goal:  Numpy array representing the achieved goal from the previous timestep
        :param achieved_goal: Numpy array representing the achieved goal
        :param desired_goal:  Numpy array representing the desired goal
        :return:            Reward for current distance
        """
        distance = self.calculate_distance(achieved_goal, desired_goal)
        prev_distance = self.calculate_distance(prev_achieved_goal, desired_goal)

        if self._total_distance > 0:
            reward = self._distance_reward * ((prev_distance - distance) / self._total_distance)
        else:
            reward = self._distance_reward

        return reward



    def calculate_distance(self, achieved_goal, desired_goal):
        """
        Calculates the euclidean distance between a provided achieved goal and
        a provided desired goal

        :param achieved_goal:   Numpy array representing the achieved goal
        :param desired_goal:    Numpy array representing the desired goal
        :return:                Float representing the euclidean distance
        """
        if self._unnormalized_compare is True:
            distance = sqrt((achieved_goal[self.goal_space_mapping["workpiece"][0]["x"]]*130 - desired_goal[self.goal_space_mapping["workpiece"][0]["x"]]*130) ** 2 +
                            (achieved_goal[self.goal_space_mapping["workpiece"][0]["y"]]*130 - desired_goal[self.goal_space_mapping["workpiece"][0]["y"]]*130) ** 2 +
                            (achieved_goal[self.goal_space_mapping["workpiece"][0]["z"]]*30 - desired_goal[self.goal_space_mapping["workpiece"][0]["z"]]*30) ** 2)
        else:
            distance = sqrt((achieved_goal[self.goal_space_mapping["workpiece"][0]["x"]] - desired_goal[self.goal_space_mapping["workpiece"][0]["x"]]) ** 2 +
                            (achieved_goal[self.goal_space_mapping["workpiece"][0]["y"]] - desired_goal[self.goal_space_mapping["workpiece"][0]["y"]]) ** 2 +
                            (achieved_goal[self.goal_space_mapping["workpiece"][0]["z"]] - desired_goal[self.goal_space_mapping["workpiece"][0]["z"]]) ** 2)

        return distance



    def get_achieved_goal(self, observation, info):
        """
        Returns the current achieved goal from the observation and info dict.
        For the achieved goal the actual workpiece position is used instead of the predicted one.

        :param observation:     Numpy array representing the current environment state
        :param info:            Info dictionary
        :return:    Numpy array representing the current achieved goal
        """
        achieved_goal = info["workpiece_info"][0]["position_real"].copy()
        return achieved_goal



    def goal_achieved(self, achieved_goal, desired_goal):
        """
        Checks whether the desired goal has been achieved.

        :param achieved_goal:   Numpy array representing the achieved goal
        :param desired_goal:    Numpy array representing the desired goal
        :return:                Boolean flag that is true if the desired goal has been achieved
        """
        if self.position_goal_achieved(achieved_goal, desired_goal) is  True:
            if len(self.goal_space_mapping["system"]) > 0:
                if (np.array(achieved_goal)[list(self.goal_space_mapping["system"].values())] ==
                    np.array(desired_goal)[list(self.goal_space_mapping["system"].values())]).all():    # True/False does not work here
                    return True
                else:
                    return False
            else:
                return True
        else:
            return False



    def position_goal_achieved(self, achieved_goal, desired_goal):
        """
        Checks whether the positional goal has been reached. 
        Each dimension needs to be checked separately.

        :param achieved_goal:   Numpy array representing the achieved goal
        :param desired_goal:    Numpy array representing the desired goal
        :return:                Boolean flag that is true if the positional goal has been achieved
        """
        if (abs(achieved_goal[self.goal_space_mapping["workpiece"][0]["x"]] * 130 - desired_goal[self.goal_space_mapping["workpiece"][0]["x"]] * 130) < self._TOLERANCE * 130 and
            abs(achieved_goal[self.goal_space_mapping["workpiece"][0]["y"]] * 130 - desired_goal[self.goal_space_mapping["workpiece"][0]["y"]] * 130) < self._TOLERANCE * 130 and
            abs(achieved_goal[self.goal_space_mapping["workpiece"][0]["z"]] * 30 - desired_goal[self.goal_space_mapping["workpiece"][0]["z"]] * 30) < self._TOLERANCE * 30):

            return True

        else:
            return False



    def get_reward_state(self):
        """
        Returns the current state of the achievment-based distance goal to be used as network input.

        :return: Float representing the reward state
        """
        pass