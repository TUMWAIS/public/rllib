import os
import numpy as np
import psutil
from copy import deepcopy
import random

from rllib.classes.algorithms.DRLAbstract import DRLAbstract
from rllib.classes.replay_buffers.ReplayBufferBuilderStd import ReplayBufferBuilderStd
from rllib.classes.training_actors.TrainingActor import TrainingActor
from rllib.classes.exploration.HashingBonusEvaluator import HashingBonusEvaluator
from rllib.functions.parallel import create_worker_instances, get_return_from_pool, submit_to_pool, call_pool_method, preload_pool, update_pools
from rllib.functions.training import reduce_learning_rate, adjust_batch_size, reduce_epsilon
from rllib.functions.saving import save_to_file, load_from_file, save_weight_list_to_file, load_weight_list_from_file
from rllib.functions.plotting import plot



class OffPolicyBase(DRLAbstract):
    """
    Standard implementation for value-based algorithms. Can be used with the Deep Q-Learning (DQN) and the
    Deep Deterministic Policy Gradients (DDPG) algorithm
    """

    def __init__(self,
                 parameter_file,
                 ParObj,
                 VarObj,
                 EnvironmentBuilder,
                 Policy,
                 RolloutActor,
                 EvaluationActor,
                 ReplayBufferBuilder=ReplayBufferBuilderStd,
                 TrainingActor=TrainingActor,
                 ExpBonusEvaluator=HashingBonusEvaluator,
                 RolloutActorArgs=None,
                 EvaluationActorArgs=None,
                 ReplayBufferBuilderArgs=None,
                 TrainingActorArgs=None):

        super().__init__(parameter_file, ParObj, VarObj)

        # Create trainer pool (Needs to be created first in order to manage GPU)
        if TrainingActorArgs is None:
            TrainingActorArgs = []
        self.trainer_pool = create_worker_instances(self.params.num_training_workers,
                                                    self.params.fixed_execution_order,
                                                    TrainingActor,
                                                    self.params,
                                                    Policy,
                                                    *TrainingActorArgs)

        # Restore or get network weights
        self.weights = call_pool_method(self.trainer_pool, "get_weights", [self.params.restore])

        # Create simulation agent pool
        if RolloutActorArgs is None:
            RolloutActorArgs = []
        self.simulation_agent_pool = create_worker_instances(self.params.num_sim_workers,
                                                             self.params.fixed_execution_order,
                                                             RolloutActor,
                                                             self.params,
                                                             EnvironmentBuilder,
                                                             Policy,
                                                             *RolloutActorArgs)

        # Restore state of rollout workers
        if self.params.restore is True:
            self.rollout_state = load_from_file(os.path.join(self.params.training_data, "rollout_state"))
        else:
            self.rollout_state = call_pool_method(self.simulation_agent_pool,
                                                  "get_rollout_state" )

        # Create replay buffer agent pool
        if ReplayBufferBuilderArgs is None:
            ReplayBufferBuilderArgs = []
        replay_buffer_builder = ReplayBufferBuilder(self.params)
        self.replay_buffer_actor_pool = create_worker_instances(self.params.num_replay_buffer_workers,
                                                                self.params.fixed_execution_order,
                                                                replay_buffer_builder.get_replay_buffer(),
                                                                self.params,
                                                                *ReplayBufferBuilderArgs)

        # Create ExplorationBonusEvaluator for Count-based exploration
        if self.params.exploration_bonus is True:
            self.exploration_bonus_evaluator = ExpBonusEvaluator(self.params, EnvironmentBuilder)

        # Create evaluation actor
        if self.params.evaluate is not None:
            if EvaluationActorArgs is None:
                EvaluationActorArgs = []
            self.evaluation_agent_pool = create_worker_instances(1,
                                                                 self.params.fixed_execution_order,
                                                                 EvaluationActor,
                                                                 self.params,
                                                                 EnvironmentBuilder,
                                                                 Policy,
                                                                 *EvaluationActorArgs,
                                                                 force_cpu_use=True)

        print("- - - - - - - - - - - - - - - - - - - - - - - - - - -")
        print("Number of available cores:       {0:1.0f}".format(len(psutil.Process().cpu_affinity())))
        print("Number of Control Threads:       {0:1.0f}".format(self.params.num_main_threads))
        print("Number of Simulation Workers:    {0:1.0f}".format(self.params.num_sim_threads))
        print("Number of Replay Buffer Workers: {0:1.0f}".format(self.params.num_replay_buffer_threads))
        print("Number of Training Workers:      {0:1.0f}".format(self.params.num_training_threads))
        print("- - - - - - - - - - - - - - - - - - - - - - - - - - -")



    def _initialize_training(self, rollout_args=[], rollout_kwargs={}, buffer_args=[], buffer_kwargs={}):
        """
        Submits simulation tasks and sampling tasks to the rollout worker and buffer pools. The main training loop
        assumes that pools are completely filled
        """

        # Initialize list of replay buffer sizes (all agents are idle when executed)
        self.vars.replay_buffer_sizes.extend([0 for i in range(self.params.num_replay_buffer_workers)])

        # Submit tasks to each worker in the pool
        args = [self.vars.episode, self.weights, [self.vars.epsilon], *rollout_args]
        kwargs = {"rollout_state": self.rollout_state, **rollout_kwargs}
        preload_pool(self.simulation_agent_pool,
                     "run",
                     args,
                     kwargs)

        args = [self.vars.batch_size, *buffer_args]
        kwargs = {**buffer_kwargs}
        preload_pool(self.replay_buffer_actor_pool,
                     "get_minibatch",
                     args,
                     kwargs)



    def _sample_trajectories(self, rollout_args=[], rollout_kwargs={}):
        """
        Samples/retrieves trajectories from the rollout workers and submits them to the replay buffer.
        """
        # Update pools
        update_pools([self.simulation_agent_pool, self.replay_buffer_actor_pool])

        # Sample trajectories in the environment
        if self.params.prefill_buffer_randomly is True and self.vars.buffer_filled is False:
            epsilon = 1
        else:
            epsilon = self.vars.epsilon

        args = [self.vars.episode, self.weights, [epsilon], *rollout_args]
        kwargs = {"rollout_state": self.rollout_state, **rollout_kwargs}
        return_values = get_return_from_pool(self.simulation_agent_pool,
                                             "run",
                                             args,
                                             kwargs)

        self.traj_list = deepcopy(return_values[0][0])
        self.rollout_info = deepcopy(return_values[0][1])



    def _process_sampled_trajectories(self, buffer_args=[], buffer_kwargs={}):
        """
        Processes the rollout statistics and stores the retrieved samples in the replay buffer
        """

        # Store rollout actor state
        if "rollout_state" in self.rollout_info:
            self.rollout_state = self.rollout_info["rollout_state"]

        # Store Q value in variable object
        if "q_values" in self.rollout_info:
            self.vars.q_value = np.sum(np.amax(self.rollout_info["q_values"], axis=1))   # Max accross values for action and sum accross actions
        if "t_values" in self.rollout_info:
            self.vars.t_value = np.sum(np.amax(self.rollout_info["t_values"], axis=1))   # Max accross values for action and sum accross actions

        # Store rollout statistics in variable object
        for key, item in self.rollout_info["statistics"].items():
            if key in self.vars.__dict__:
                self.vars.__dict__[key] = item

        # Add exploration bonus
        if self.params.exploration_bonus is True:
            self.traj_list, self.vars.mean_bonus = self.exploration_bonus_evaluator.add_exploration_bonus(self.traj_list)

        # Shuffle samples to distribute them equally to buffers (in case of multiple buffers)
        if self.params.num_replay_buffer_workers > 1:
            random.shuffle(self.traj_list)

        # Split samples into chunks and store them in buffers
        num_samples = len(self.traj_list)
        chunk_size = len(self.traj_list) // self.params.num_replay_buffer_workers
        for i, idx in enumerate(range(0, num_samples, chunk_size)):
            traj_chunk = self.traj_list[idx:(idx + chunk_size)]

            kwargs = {**buffer_kwargs}
            args = [traj_chunk, *buffer_args]
            submit_to_pool(self.replay_buffer_actor_pool,
                           "save_to_buffer",
                           args,
                           kwargs,
                           i)



    def _get_minibatch(self, buffer_args=[], buffer_kwargs={}):
        """
        Retrieves a minibatch from the replay buffer actor
        """
        # Update pools
        update_pools([self.simulation_agent_pool, self.replay_buffer_actor_pool])

        # Create dataset or skip training if replay buffer is not large enough
        args = [self.vars.batch_size, *buffer_args]
        kwargs = {**buffer_kwargs}
        return_values = get_return_from_pool(self.replay_buffer_actor_pool,
                                             "get_minibatch",
                                             args,
                                             kwargs,
                                             skip_none=True)

        self.minibatch_bundle = deepcopy(return_values[0][0])
        self.buffer_info = deepcopy(return_values[0][1])

        # Re-calculate size of replay buffer
        self.vars.replay_buffer_sizes[self.buffer_info["worker_id"]] = self.buffer_info["buffer_size"]
        self.vars.replay_buffer_total_size = 0
        for buffer_size in self.vars.replay_buffer_sizes:
            self.vars.replay_buffer_total_size += buffer_size



    def _update_networks(self, trainer_args=[], trainer_kwargs={}, buffer_args=[], buffer_kwargs={}):
        """
        Updates the q-network and target network.
        """
        # Update pools
        update_pools([self.simulation_agent_pool, self.replay_buffer_actor_pool])

        # Train the Q network (if enough batches in replay buffer)
        if self.minibatch_bundle is not None:
            self.vars.buffer_filled = True

            # Initiate update of q-network
            args = [self.weights, self.vars.episode, self.minibatch_bundle, *trainer_args]
            kwargs = {"learning_rate": self.vars.learning_rate, **trainer_kwargs}
            return_values = get_return_from_pool(self.trainer_pool,
                                                 "train_policy",
                                                 args,
                                                 kwargs)
            self.weights = return_values[0][0]
            self.training_info = return_values[0][1]

            # Send TD errors back to replay buffer actor
            if hasattr(self.params, "PER") and self.params.PER is True:     # PER attribute not necessarily in parameter object
                args = [self.training_info, *buffer_args]
                kwargs = {**buffer_kwargs}
                submit_to_pool(self.replay_buffer_actor_pool,
                               "update_priorities",
                               args,
                               kwargs,
                               self.buffer_info["worker_id"])

            # Reduce learning rate
            if self.params.learning_rate_schedule is not None:
                self.vars.learning_rate = reduce_learning_rate(self.params,
                                                               self.vars.learning_rate,
                                                               self.vars.learning_rate_counter)

            # Reduce epsilon
            self.vars.epsilon, self.vars.epsilon_counter = reduce_epsilon(self.params,
                                                                          self.vars.epsilon,
                                                                          self.vars.epsilon_counter)

            # Adjust batch size according to schedule
            if self.params.batch_size_schedule is True:
                self.vars.batch_size = adjust_batch_size(self.params,
                                                         self.vars.batch_size,
                                                         self.vars.episode)

            # Update Vars object with training statistics
            for key, item in self.training_info.items():
                if key in self.vars.__dict__:
                    self.vars.__dict__[key] = item



    def _evaluate_training(self, eval_args=[], eval_kwargs={}):
        """
        Evaluate policy with epsilon = 0
        """
        # Update pools
        update_pools([self.simulation_agent_pool, self.replay_buffer_actor_pool])

        if self.params.evaluate is not None and (self.vars.episode + 1) % self.params.evaluate == 0:
            args = [self.vars.episode, self.weights, [self.vars.epsilon], *eval_args]
            kwargs = {"rollout_state": self.rollout_state, "deployment_mode": True, **eval_kwargs}
            return_values = get_return_from_pool(self.evaluation_agent_pool,
                                                 "run",
                                                 args,
                                                 kwargs)

            self.eval_traj_list = return_values[0][0]
            self.eval_info = return_values[0][1]

            # Store rollout statistics in variable object
            for key, item in self.eval_info["statistics"].items():
                if "eval_" + key in self.vars.__dict__:
                    self.vars.__dict__["eval_" + key] = item



    def _log_training(self):
        """
        Stores training information as well as the training state
        """
        # Update pools
        update_pools([self.simulation_agent_pool, self.replay_buffer_actor_pool])

        self.vars.add_to_history()
        if (self.vars.episode + 1) % self.params.history_store_rate == 0:
            # Store training history
            self.vars.save_history_to_file()

            # Store network weights
            call_pool_method(self.trainer_pool, "store_weights", [self.weights])

            # Store rollout state
            save_to_file(os.path.join(self.params.training_data, "rollout_state"), self.rollout_state)

            # Store replay buffer
            for worker_id in range(self.params.num_replay_buffer_workers):
                submit_to_pool(self.replay_buffer_actor_pool,
                               "store_replay_buffer",
                               worker_id=worker_id)

            # Store exploration bonus evaluator state
            if self.params.exploration_bonus is True:
                self.exploration_bonus_evaluator.save_bonus_eval_state()

            # Plot training state
            if self.params.plot_conf_path is not None:
                plot(os.path.join(self.params.training_data, "history/history"), layout_path=self.params.plot_conf_path,
                     save_path=os.path.join(self.params.plots_dir, "autoplot"))



    def _print_episode_info(self):
        """
        Prints episode statistics
        """

        # Update pools
        update_pools([self.simulation_agent_pool, self.replay_buffer_actor_pool])

        if "reward_msg" in self.rollout_info["traj_info"][0] and self.rollout_info["traj_info"][0]["reward_msg"] is not None:
            reward_msg = self.rollout_info["traj_info"][0]["reward_msg"]
        else:
            reward_msg = ""

        if "done_msg" in self.rollout_info["traj_info"][0] and self.rollout_info["traj_info"][0]["done_msg"] is not None:
            done_msg = self.rollout_info["traj_info"][0]["done_msg"]
        else:
            done_msg = ""

        # Prints training status to terminal
        print("Episode {0:4.0f}  Score {1:6.2f}  Q-Value {2:5.2f}  Traj {3:2.0f}  Steps {4:3.0f}  Buffer {5:7.0f}  Epsilon {6:6.4f}  LR {7:6.4f}  ST {8:5.3f}  DT {9:5.3f}  TT {10:5.3f}  ET {11:5.3f}  RMsg {12:20}  DMsg {13:15}".format(
                self.vars.episode, self.vars.mean_reward, self.vars.q_value, self.vars.num_traj, self.vars.mean_steps,
                self.vars.replay_buffer_total_size, self.vars.epsilon,
                self.vars.learning_rate, self.vars.sample_time, self.vars.dataset_time,
                self.vars.training_time, self.vars.episode_time, reward_msg, done_msg[:15]))



    def deploy(self, deploy_args=[], deploy_kwargs={}):
        """
        Deploys a trained policy and renders the simulation as a video
        """
        args = [self.vars.episode, self.weights, [0], *deploy_args]
        kwargs = {"rollout_state": self.rollout_state, "deployment_mode": True, **deploy_kwargs}
        get_return_from_pool(self.evaluation_agent_pool,
                             "deploy",
                             args,
                             kwargs)
