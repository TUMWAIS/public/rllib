import os
import numpy as np
import psutil
from copy import deepcopy

from rllib.classes.algorithms.DRLAbstract import DRLAbstract
from rllib.classes.training_actors.TrainingActor import TrainingActor
from rllib.classes.exploration.HashingBonusEvaluator import HashingBonusEvaluator
from rllib.classes.errors.RestoreError import RestoreError
from rllib.functions.parallel import create_worker_instances, get_return_from_pool, call_pool_method
from rllib.functions.training import reduce_learning_rate
from rllib.functions.saving import save_to_file, load_from_file, save_weight_list_to_file, load_weight_list_from_file
from rllib.functions.plotting import plot



class OnPolicyBase(DRLAbstract):
    """
    Standard implementation for policy gradient-based algorithms such as VPG, A2C, and PPO
    """

    def __init__(self,
                 parameter_file,
                 ParObj,
                 VarObj,
                 EnvironmentBuilder,
                 Policy,
                 RolloutActor,
                 EvaluationActor,
                 TrainingActor=TrainingActor,
                 ExpBonusEvaluator=HashingBonusEvaluator,
                 RolloutActorArgs=None,
                 EvaluationActorArgs=None,
                 TrainingActorArgs=None):

        super().__init__(parameter_file, ParObj, VarObj)

        # Create trainer pool (Needs to be created first in order to manage GPU)
        if TrainingActorArgs is None:
            TrainingActorArgs = []
        self.trainer_pool = create_worker_instances(self.params.num_training_workers,
                                                    self.params.fixed_execution_order,
                                                    TrainingActor,
                                                    self.params,
                                                    Policy,
                                                    *TrainingActorArgs)

        # Restore or get network weights
        self.weights = call_pool_method(self.trainer_pool, "get_weights", [self.params.restore])

        # Create simulation agent pool
        if RolloutActorArgs is None:
            RolloutActorArgs = []
        self.simulation_agent_pool = create_worker_instances(self.params.num_sim_workers,
                                                             self.params.fixed_execution_order,
                                                             RolloutActor,
                                                             self.params,
                                                             EnvironmentBuilder,
                                                             Policy,
                                                             *RolloutActorArgs)

        # Restore state of rollout workers
        if self.params.restore is True:
            self.rollout_state = load_from_file(os.path.join(self.params.training_data, "rollout_state"))
        else:
            self.rollout_state = call_pool_method(self.simulation_agent_pool,
                                                  "get_rollout_state" )

        # Create ExplorationBonusEvaluator for Count-based exploration
        if self.params.exploration_bonus is True:
            self.exploration_bonus_evaluator = ExpBonusEvaluator(self.params, EnvironmentBuilder)

        # Create evaluation actor
        if self.params.evaluate is not None:
            self.evaluation_agent_pool = create_worker_instances(1,
                                                                 self.params.fixed_execution_order,
                                                                 EvaluationActor,
                                                                 self.params,
                                                                 EnvironmentBuilder,
                                                                 Policy,
                                                                 *EvaluationActorArgs,
                                                                 force_cpu_use=True)

        print("- - - - - - - - - - - - - - - - - - - - - - - - - - -")
        print("Number of available cores:       {0:1.0f}".format(len(psutil.Process().cpu_affinity())))
        print("Number of Control Threads:       {0:1.0f}".format(1))
        print("Number of Simulation Workers:    {0:1.0f}".format(self.params.num_sim_threads))
        print("Number of Training Workers:      {0:1.0f}".format(self.params.num_training_threads))
        print("- - - - - - - - - - - - - - - - - - - - - - - - - - -")



    def _sample_trajectories(self, rollout_args=[], rollout_kwargs={}):
        """
        Samples/retrieves trajectories from the rollout workers
        """

        # Sample trajectories in the environment
        args = [self.vars.episode, self.weights, [], *rollout_args]
        kwargs = {"rollout_state": self.rollout_state, **rollout_kwargs}
        return_values = get_return_from_pool(self.simulation_agent_pool,
                                             "run",
                                             args,
                                             kwargs,
                                             num_jobs=self.params.num_sim_workers)

        self.return_values = deepcopy(return_values)



    def _process_sampled_trajectories(self):
        """
        Processes the rollout statistics and retrieved samples
        """

        # Process batch of return values
        self.traj_list = []
        self.vars.value = 0
        for idx, rval in enumerate(self.return_values):
            self.traj_list.extend(rval[0])
            self.rollout_info = rval[1]

            # Store rollout actor state
            if "rollout_state" in self.rollout_info:
                self.rollout_state = self.rollout_info["rollout_state"]

            # Store values
            if "values" in self.rollout_info:
                self.vars.value += np.sum(self.rollout_info["values"]) / self.params.num_sim_workers

            # Store rollout statistics
            for key, item in self.rollout_info["statistics"].items():
                if key in self.vars.__dict__:
                    if idx == 0:
                        self.vars.__dict__[key] = 0
                    self.vars.__dict__[key] += item / self.params.num_sim_workers

        # Add exploration bonus
        if self.params.exploration_bonus is True:
            self.traj_list, self.vars.mean_bonus = self.exploration_bonus_evaluator.add_exploration_bonus(self.traj_list)



    def _get_minibatch(self):
        """
        Prepares a minibatch for training 
        """
        self.minibatch = []
        self.traj_list = np.array(self.traj_list, dtype="object")
        for i in range(len(self.traj_list[0])):
            self.minibatch.append(np.stack(self.traj_list[:, i]))
        self.vars.batch_size = len(self.minibatch[0])



    def _update_networks(self, trainer_args=[], trainer_kwargs={}):
        """
        Updates the policy
        """
        # Initiate update of q-network
        args = [self.weights, self.vars.episode, self.minibatch, *trainer_args]
        kwargs = {"learning_rate": self.vars.learning_rate, **trainer_kwargs}
        return_values = get_return_from_pool(self.trainer_pool,
                                             "train_policy",
                                             args,
                                             kwargs)
        self.weights = return_values[0][0]
        self.training_info = return_values[0][1]

        # Reduce learning rate
        if self.params.learning_rate_schedule is not None:
            self.vars.learning_rate = reduce_learning_rate(self.params,
                                                           self.vars.learning_rate,
                                                           self.vars.learning_rate_counter)

        # Update Vars object with training statistics
        for key, item in self.training_info.items():
            if key in self.vars.__dict__:
                self.vars.__dict__[key] = item



    def _evaluate_training(self, eval_args=[], eval_kwargs={}):
        """
        Evaluates the policy
        """
        if self.params.evaluate is not None and (self.vars.episode + 1) % self.params.evaluate == 0:
            args = [self.vars.episode, self.weights, [], *eval_args]
            kwargs = {"rollout_state": self.rollout_state, "deployment_mode": True, **eval_kwargs}
            return_values = get_return_from_pool(self.evaluation_agent_pool,
                                                 "run",
                                                 args,
                                                 kwargs)
            self.eval_info = return_values[0][1]

            # Store rollout statistics in variable object
            for key, item in self.eval_info["statistics"].items():
                if "eval_" + key in self.vars.__dict__:
                    self.vars.__dict__["eval_" + key] = item



    def _log_training(self):
        """
        Stores training information as well as the training state
        """
        self.vars.add_to_history()
        if (self.vars.episode + 1) % self.params.history_store_rate == 0:
            # Store training history
            self.vars.save_history_to_file()

            # Store network weights
            call_pool_method(self.trainer_pool, "store_weights", [self.weights])

            # Store rollout state
            save_to_file(os.path.join(self.params.training_data, "rollout_state"), self.rollout_state)

            # Store exploration bonus evaluator state
            if self.params.exploration_bonus is True:
                self.exploration_bonus_evaluator.save_bonus_eval_state()

            # Plot training state
            if self.params.plot_conf_path is not None:
                plot(os.path.join(self.params.training_data, "history/history"), layout_path=self.params.plot_conf_path,
                     save_path=os.path.join(self.params.plots_dir, "autoplot"))



    def _print_episode_info(self):
        """
        Prints episode statistics
        """

        if "reward_msg" in self.rollout_info["traj_info"][0]:
            reward_msg = self.rollout_info["traj_info"][0]["reward_msg"]
        else:
            reward_msg = ""

        if "done_msg" in self.rollout_info["traj_info"][0]:
            done_msg = self.rollout_info["traj_info"][0]["done_msg"]
        else:
            done_msg = ""

        # Prints training status to terminal
        print("Episode {0:4.0f}  Score {1:6.2f}  Traj {2:2.0f}  Steps {3:3.0f}  Batch Size {4:4.0f}  Value {5:5.2f}  ALoss {6:6.2f} CLoss {7:6.2f}  ELoss {8:8.4f}  LR {9:6.4f}  ST {10:5.3f}  TT {11:5.3f}  ET {12:5.3f}  RMsg {13:20}  DMsg {14:15}".format(
                self.vars.episode, self.vars.mean_reward, self.vars.num_traj, self.vars.mean_steps, self.vars.batch_size, self.vars.value, self.vars.actor_loss, self.vars.critic_loss,
                self.vars.entropy_loss, self.vars.learning_rate, self.vars.sample_time, self.vars.training_time, self.vars.episode_time, reward_msg, done_msg[:15]))



    def deploy(self, deploy_args=[], deploy_kwargs={}):
        """
        Deploys a trained policy and renders the simulation as a video
        """
        args = [self.vars.episode, self.weights, [0], *deploy_args]
        kwargs = {"rollout_state": self.rollout_state, "deployment_mode": True, **deploy_kwargs}
        get_return_from_pool(self.evaluation_agent_pool,
                             "deploy",
                             args,
                             kwargs)
