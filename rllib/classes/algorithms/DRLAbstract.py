from abc import ABC
from datetime import datetime
import wandb
import os

from rllib.functions.seeding import non_gpu_seeding
from rllib.functions.saving import delete_old_files



class DRLAbstract(ABC):
    """
    Abstract base class for reinforcement learning algorithms
    """

    def __init__(self,
                 parameter_file,
                 ParObj,
                 VarObj):

        # Initialize parameter and variable object
        self.params = ParObj(parameter_file)
        self.vars = VarObj(self.params)

        # Load history from file or delete old files
        if self.params.restore is True:
            self.params.restore = self.vars.load_history_from_file()
        else:
            delete_old_files(self.params.training_data, self.params.warn_before_deletion)
            delete_old_files(os.path.join(self.params.wandb_dir, "wandb"))                           # Only warn once, wandb dir is actually the base dir
            delete_old_files(self.params.plots_dir)

        # Initialize weights and biases
        if self.params.wandb_logging is True:
            if self.params.restore is True:
                file = open(os.path.join(self.params.training_dir, "wandb/run_id"), "r")
                resume = file.read()
                file.close()
            else:
                resume = None

            os.environ['WANDB_API_KEY'] = self.params.wandb_key

            wandb.init(name=self.params.wandb_run_name,
                       entity=self.params.wandb_entity,
                       notes=self.params.wandb_run_notes,
                       project=self.params.wandb_project,
                       group=self.params.wandb_group,
                       tags=self.params.wandb_tags,
                       resume=resume,
                       reinit=self.params.wandb_reinit,
                       dir=self.params.wandb_dir)

            wandb.config.update(self.params.get_config(), allow_val_change=True)    # Push config separately to allow changes after resume

            file = open(os.path.join(self.params.training_dir, "wandb/run_id"), "w")
            file.write(wandb.run.id)
            file.close()

        # Set random seeds
        if self.params.fixed_seed is not None:
            non_gpu_seeding(self.params.fixed_seed)



    def train(self):
        """
        Method responsible for the training process.  This method relies on sub-methods for the different parts of
        training.
        """
        overall_timestamp = datetime.now().timestamp()

        self._initialize_training()

        # Main training loop
        for self.vars.episode in range(self.vars.episode, self.params.num_episodes):
            if self.params.timeit is True: start_timestamp = datetime.now().timestamp()

            self._sample_trajectories()
            self._process_sampled_trajectories()

            if self.params.timeit is True: sample_timestamp = datetime.now().timestamp()

            self._get_minibatch()

            if self.params.timeit is True: dataset_timestamp = datetime.now().timestamp()

            self._update_networks()

            if self.params.timeit is True: training_timestamp = datetime.now().timestamp()

            self._evaluate_training()

            if self.params.timeit is True: evaluation_timestamp = datetime.now().timestamp()

            self._log_training()

            if self.params.timeit is True: logging_timestamp = datetime.now().timestamp()

            # Calculate episode times and print statistics
            if self.params.timeit is True:
                self.vars.sample_time = sample_timestamp - start_timestamp
                self.vars.dataset_time = dataset_timestamp - sample_timestamp
                self.vars.training_time = training_timestamp - dataset_timestamp
                self.vars.evaluation_time = evaluation_timestamp - training_timestamp
                self.vars.logging_time = logging_timestamp - evaluation_timestamp
                self.vars.episode_time = evaluation_timestamp - start_timestamp
            self.vars.overall_time = self.vars.prev_overall_time + (
                    datetime.now().timestamp() - overall_timestamp)  # Always calclate overall training time

            self._print_episode_info()



    def _initialize_training(self):
        """
        Responsible for any initialization before the main training loop starts.
        For example, pre-filling of actor pools with tasks.
        """
        pass



    def _sample_trajectories(self):
        """
        Responsible for the sampling of trajectories.
        """
        pass



    def _process_sampled_trajectories(self):
        """
        Responsible for processing statistics and storing trajectories in the replay buffer
        """
        pass



    def _get_minibatch(self):
        """
        Responsible for the sampling of minibatches for training
        """
        pass



    def _update_networks(self):
        """
        Responsible for the training of the neural networks
        """
        pass



    def _evaluate_training(self):
        """
        Responsible for any evaluation steps of the policy
        """
        pass



    def _log_training(self):
        """
        Responsible for storage of training information and saving the training state
        """
        pass



    def _print_episode_info(self):
        """
        Responsible for printing episode information
        """
        pass