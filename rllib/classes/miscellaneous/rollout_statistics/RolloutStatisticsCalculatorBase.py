import numpy as np
from rllib.classes.miscellaneous.rollout_statistics.RolloutStatisticsCalculatorAbstract import RolloutStatisticsCalculatorAbstract


class RolloutStatisticsCalculatorBase(RolloutStatisticsCalculatorAbstract):
    """
    Standard class for calculating statistics during rollout.
    """

    def __init__(self, gamma, max_reward=1):
        super().__init__(gamma, max_reward)

        self.gamma = gamma
        self.max_reward = max_reward
        self.reward_sum = 0
        self.rel_reward_sum = 0
        self.dis_reward_sum = 0
        self.num_steps = 0
        self.trans_counter = 0
        self.num_traj = 0



    def add_transition(self, reward):
        """
        Saves a new transition to the current trajectory

        :param reward:  Reward of the transition to add
        """
        super().add_transition(reward)

        self.reward_sum += reward
        self.rel_reward_sum += (reward/self.max_reward)
        self.dis_reward_sum += reward * self.gamma**self.trans_counter
        self.num_steps += 1
        self.trans_counter += 1



    def start_new_traj(self, max_reward=None):
        """
        Starts a new trajectory
        """
        super().start_new_traj(max_reward)

        self.num_traj += 1
        self.trans_counter = 0

        if max_reward is not None:
            self.max_reward = max_reward



    def get_statistics(self):
        """
        Returns the statistics on all trajectories playes after the last reset

        :return statistics:     Dictionary with names and values of the calculated statistics
        """
        super().get_statistics()

        mean_reward = self.get_mean_from_cum_value(self.reward_sum, self.num_traj)
        mean_rel_reward = self.get_mean_from_cum_value(self.rel_reward_sum, self.num_traj)
        mean_dis_reward = self.get_mean_from_cum_value(self.dis_reward_sum, self.num_traj)
        mean_steps = self.get_mean_from_cum_value(self.num_steps, self.num_traj)

        statistics = {"mean_reward":             mean_reward,
                      "mean_dis_reward":         mean_dis_reward,
                      "mean_rel_reward":         mean_rel_reward,
                      "mean_steps":              mean_steps,
                      "num_traj":                self.num_traj,
                     }

        return statistics



    def reset(self):
        """
        Resets the statistics calculation
        """
        super().reset()

        self.reward_sum = 0
        self.rel_reward_sum = 0
        self.dis_reward_sum = 0
        self.num_steps = 0
        self.trans_counter = 0
        self.num_traj = 0



    def get_mean_from_cum_value(self, cum_val, count):
        """
        Calculates the mean of a performance variable across trajectories and catches error if
        the number of played trajectories is zero.

        :param val:     Float representing the cummulated value of a performance variable
        :param count:   Integer representing the number of played trajectories
        :return:        Float representing the mean of the performance variable across trajectories
        """
        try:
            mean_val = cum_val / count
        except:
            mean_val = 0

        return mean_val



    def get_mean_from_list(self, list):
        """
        Calculates the mean from a list of a performance variable and catches errors if the list is empty.

        :param list:    List to calculate the mean from
        :return:        Float representing the mean of the performance variable
        """
        if list:
            mean_val = np.mean(list)
        else:
            mean_val = 0

        return mean_val