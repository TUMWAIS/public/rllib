from abc import ABC, abstractmethod


class RolloutStatisticsCalculatorAbstract(ABC):
    """
    Standard class for calculating statistics during rollout.
    """

    @abstractmethod
    def __init__(self, gamma, max_reward=1):
        pass


    @abstractmethod
    def add_transition(self, reward):
        """
        Saves a new transition to the current trajectory

        :param reward:  Reward of the transition to add
        """
        pass


    @abstractmethod
    def start_new_traj(self, max_reward=None):
        """
        Starts a new trajectory
        """
        pass


    @abstractmethod
    def get_statistics(self):
        """
        Returns the statistics on all trajectories playes after the last reset

        :return mean_reward:        Average reward across played trajectories
        :return mean_rel_reward:    Normalized average reward across played trajectories
        :return mean_dis_reward:    Average discounted reward across played trajectories
        :return mean_steps:         Average number of transitions across played trajectories
        :return num_traj:           Number of played trajectories

        """
        pass


    @abstractmethod
    def reset(self):
        """
        Resets the statistics calculation
        """
        pass