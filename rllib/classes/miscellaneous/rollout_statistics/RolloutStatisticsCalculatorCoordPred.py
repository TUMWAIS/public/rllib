import math
import numpy as np

from rllib.classes.miscellaneous.rollout_statistics.RolloutStatisticsCalculatorBase import RolloutStatisticsCalculatorBase



class RolloutStatisticsCalculatorCoordPred(RolloutStatisticsCalculatorBase):

    def __init__(self, gamma, max_reward=1):
        super().__init__(gamma, max_reward)
        self.predicition_error = 0
        self.distance_list = []



    def add_transition(self, reward, info):
        """
        Saves a new transition to the current trajectory

        :param reward:  Reward of the transition to add
        :param info:    Info dictionary of the environment
        """
        super().add_transition(reward)

        pos_real = info["workpiece_info"][0]["position_real"]
        pos_pred = info["workpiece_info"][0]["position"]
        self.predicition_error += math.sqrt((pos_real[0] - pos_pred[0]) ** 2 +
                                            (pos_real[1] - pos_pred[1]) ** 2 +
                                            (pos_real[2] - pos_pred[2]) ** 2)

        if "distance" in info.keys():
            self.distance_list[-1] = min(self.distance_list[-1], info["distance"])
        else:
            self.distance_list[-1] = 1



    def start_new_traj(self, max_reward=None):
        """
        Starts a new trajectory
        """
        super().start_new_traj(max_reward)
        self.distance_list.append(1)



    def get_statistics(self):
        """
        Returns the statistics on all trajectories playes after the last reset

        :return statistics:     Dictionary with names and values of the calculated statistics
        """
        statistics = super().get_statistics()

        mean_pred_error = self.get_mean_from_cum_value(self.predicition_error, self.num_steps)

        if self.distance_list:
            mean_distance = np.mean(self.distance_list)
            min_distance = min(self.distance_list)
        else:
            mean_distance = 1         # Max. distance if no trajectory has been played
            min_distance = 1

        statistics["mean_pred_error"] = mean_pred_error
        statistics["mean_distance"] = mean_distance
        statistics["min_distance"] = min_distance

        return statistics



    def reset(self):
        """
        Resets the statistics calculation
        """
        super().reset()
        self.predicition_error = 0
        self.distance_list = []



