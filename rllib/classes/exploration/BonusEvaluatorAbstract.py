from abc import ABC, abstractmethod

class BonusEvaluatorAbstract(ABC):
    """
    Abstract class that provides functionality to calculate exploration bonuses, e.g., for count-based exploration

    """

    def __init__(self, params, EnvironmentBuilder):
        self.params = params

        env_builder = EnvironmentBuilder()
        env = env_builder.create_gym_env(params)
        self.observation_space_mapping = env.observation_space_mapping


    @abstractmethod
    def add_exploration_bonus(self, traj_list):
        """
        Takes a list of transitions and adds the exploration bonus to the reward

        :param traj_list:     List of transitions
        :return traj_list:    List of transitions with added exploration bonus
        """
        pass


    @abstractmethod
    def get_exploration_state(self, obs):
        """
        Extracts states from the observation vector that are relevant for the determination of the exploration bonus

        :param obs:     Numpy vector of observations
        :return:        Numpy vector of observations to be used for the exploration bonus calculation
        """
        pass


    @abstractmethod
    def save_bonus_eval_state(self):
        """
        Saves the current state of the exploration bonus evaluator to a file

        """
        pass


    @abstractmethod
    def load_bonus_eval_state(self):
        """
        Loads the current state of the exploration bonus evaluator from a file

        """
        pass