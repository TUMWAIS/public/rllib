import numpy as np
import os
from datetime import datetime

from rllib.classes.exploration.BonusEvaluatorAbstract import BonusEvaluatorAbstract
from rllib.functions.saving import save_to_file, load_from_file


class HashingBonusEvaluator(BonusEvaluatorAbstract):
    """
    Adapted OpenAI implementation of hash based state counting:
    https://github.com/openai/EPG/blob/master/epg/exploration.py

    Hash-based count bonus for exploration.
    Tang, H., Houthooft, R., Foote, D., Stooke, A., Chen, X., Duan, Y., Schulman, J., De Turck, F., and Abbeel, P. (2017).
    #Exploration: A study of count-based exploration for deep reinforcement learning.
    In Advances in Neural Information Processing Systems (NIPS)
    """

    def __init__(self,
                 params,
                 EnvironmentBuilder,
                 bucket_sizes=None):
        super().__init__(params, EnvironmentBuilder)

        self.dim_key = self.params.hashing_granularity

        # Hashing function: SimHash
        if bucket_sizes is None:
            # Large prime numbers
            bucket_sizes = [999931, 999953, 999959, 999961, 999979, 999983]
        mods_list = []
        for bucket_size in bucket_sizes:
            mod = 1
            mods = []
            for _ in range(self.dim_key):
                mods.append(mod)
                mod = (mod * 2) % bucket_size
            mods_list.append(mods)
        self.bucket_sizes = np.asarray(bucket_sizes)
        self.mods_list = np.asarray(mods_list).T
        self.tables = np.zeros((len(bucket_sizes), np.max(bucket_sizes)))
        self.projection_matrix = None       # Initialize on first call of add_exploration_bonus()

        # Restore state from file
        if self.params.restore is True:
            self.load_bonus_eval_state()


    def add_exploration_bonus(self,
                              trans_list):

        # Get relevant variables from observation vector
        bonus_list = []
        obs = np.array(np.array(trans_list)[:,2].tolist())        # Convert to list to fix shape
        obs_exp = self.get_exploration_state(obs)

        # Late initialization to avoid the need for observations during class instantiation
        if self.projection_matrix is None:
            self.projection_matrix = np.random.normal(
                size=(len(obs_exp[0]), self.dim_key))  # Use first obs of first traj for obs dim

        # Add states to hash table
        self._inc_hash(obs_exp)

        # Add exploration bonus to reward
        for trans, obs in zip(trans_list, obs_exp):
            bonus = self._predict(obs.reshape((1, len(obs))))[0] * self.params.bonus_coefficient
            trans[3] += bonus
            bonus_list.append(bonus)

        return trans_list, np.mean(bonus_list)


    def get_exploration_state(self,
                              obs):
        """
        The standard implementation uses the whole observation space for the exploration bonus calculation

        :param obs:     Numpy vector of observations
        :return:        Numpy vector of observations to be used for the exploration bonus calculation
        """
        return obs


    def save_bonus_eval_state(self):
        """
        Saves the current hashing table to a file
        """
        save_to_file(os.path.join(self.params.training_data, "bonus_eval"), [self.tables, self.projection_matrix])


    def load_bonus_eval_state(self):
        """
        Loads the current hashing table from a file
        """
        self.tables, self.projection_matrix = load_from_file(os.path.join(self.params.training_data, "bonus_eval"))


    def _compute_keys(self, obss):
        binaries = np.sign(np.asarray(obss).dot(self.projection_matrix))
        keys = np.cast["int"](binaries.dot(self.mods_list)) % self.bucket_sizes
        return keys


    def _inc_hash(self, obss):
        keys = self._compute_keys(obss)
        for idx in range(len(self.bucket_sizes)):
            np.add.at(self.tables[idx], keys[:, idx], 1)


    def _query_hash(self, obss):
        keys = self._compute_keys(obss)
        all_counts = []

        for idx in range(len(self.bucket_sizes)):
            all_counts.append(self.tables[idx, keys[:, idx]])        # TODO: Check
        return np.asarray(all_counts).min(axis=0)


    def _fit_before_process_samples(self, obs):
        if len(obs.shape) == 1:
            obss = [obs]
        else:
            obss = obs
        before_counts = self._query_hash(obss)
        self._inc_hash(obss)


    def _predict(self, obs):
        counts = self._query_hash(obs)
        return 1.0 / np.maximum(1.0, np.sqrt(counts))



    #TODO: Delete (old version)
    def _add_exploration_bonus(self, env, traj_list):
        # Pre Bonus 0.009517503068428024
        start = datetime.now().timestamp()

        # Extract rewards and observations
        #print("Shape", np.shape(traj_list[0]))
        #traj_obs = np.array(traj_list)[:,:,2].tolist()           #Returns list for each trajectory with obs_next

        # Get relevant variables from observation vector
        boni = []
        for traj in traj_list:
            for trans in traj:
                #obs_exp = self.get_exploration_state(env, trans[2])
                obs_exp = trans[2]

                # Late initialization to avoid the need for observations during class instantiation
                if self.projection_matrix is None:
                    self.projection_matrix = np.random.normal(
                        size=(len(obs_exp), self.dim_key))  # Use first obs of first traj for obs dim

                # Add states to hash table
                self.__inc_hash(obs_exp.reshape((1, len(obs_exp))))

                # Add exploration bonus to reward
                bonus = self.__predict(obs_exp.reshape((1, len(obs_exp))))[0] * self.params.bonus_coefficient
                trans[3] += bonus
                boni.append(bonus)

        end = datetime.now().timestamp()
        print("Time", start-end)
        print("Bonus", np.mean(boni))
        print(traj_list[0][0])

        return traj_list, self.tables
