import os
import random
import tensorflow as tf
import numpy as np


def non_gpu_seeding(seed):
    """
    Sets the random seed for python modules utilizing the CPU

    :param seed: Integer representing the seed
    """
    tf.random.set_seed(seed)
    np.random.seed(seed)
    random.seed(seed)



def gpu_seeding(seed):
    """
    Sets all random seeds including NVIDIA CUDA and Tensorflow GPU. Only required, if GPU is used.

    :param seed: Integer representing the seed
    """
    os.environ['PYTHONHASHSEED'] = str(seed)
    os.environ['TF_DETERMINISTIC_OPS'] = '1'

    non_gpu_seeding(seed)