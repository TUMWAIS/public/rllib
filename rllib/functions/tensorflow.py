import tensorflow as tf
import logging
import os


def initialize_tf(debug=False, enable_xla=False):
    """
    Initializes tensorflow
    """

    # Reduce log level of tensorflow
    if debug is False:
        os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
        logging.getLogger("tensorflow").setLevel(logging.ERROR)

    # Restrict tensorflow from allocating all available GPU memory
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if physical_devices:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)

    if enable_xla is True:
        tf.config.optimizer.set_jit(True)


def activate_fp16_mixed_precision():
    """
    Sets precision for network calculations to FP16. Weights are still stored in FP32.
    """
    policy = tf.keras.mixed_precision.experimental.Policy('mixed_float16')
    tf.keras.mixed_precision.experimental.set_policy(policy)



def device_placement(cpu, function, *args, **kwargs):
    """
    Wrapper to execute a function on specific device

    :param cpu:                 Flag, indicating wheter function should be called on CPU
    :param network_builder:     Handle to the function to be called
    :param *args:               Arguments to pass to the function
    :return:                    Return value of function
    """

    if cpu is True:
        with tf.device("CPU:0"):                        # Execute function on CPU
            return_value = function(*args, **kwargs)
    else:
        return_value = function(*args, **kwargs)        # Execute function on most fitting device

    return return_value
    
    

class WeightedMSELoss(tf.keras.losses.Loss):
    """
    Weighted version of the Mean Squared Error loss function
    """
    def __init__(self, name="WeightedMSELoss"):
        super(WeightedMSELoss, self).__init__(name=name)

    def __call__(self, y_true, y_pred, weights=None):
        if weights is not None:
            loss = tf.reduce_mean(weights * tf.math.squared_difference(y_true, y_pred), axis=-1)
        else:
            loss = tf.reduce_mean(tf.math.squared_difference(y_true, y_pred), axis=-1)
        return loss
    
    
    
@tf.function
def soft_update_target_network(params, network, target_network):
    """
    Executes a soft update of the target network weights

    :param params:              Parameter object with parameter for soft update
    :param network:             Keras network model with new weights
    :param target_network:      Keras network model with old weights
    """
    weights = [params.tau * t_w + (1-params.tau) * a_w for t_w, a_w in zip(target_network.trainable_weights, network.trainable_weights)]
    [to_var.assign(from_var) for to_var, from_var in zip(target_network.trainable_weights, weights)]



@tf.function
def get_weights(model):
    """
    Casts the network weights into the provided datatype and returns the weight vector.

    :param model:       Keras network model
    :return:            List with weight tensors for each layer
    """
    #weights = [tf.cast(w, fptype) for w in model.trainable_variables]
    return model.trainable_weights



@tf.function
def set_weights(model, weights):
    """
    Updates the network weights with the provided weight tensors. Datatype of the provided tensor does not need to
    match the datatype of the network weights.

    Can be used to transfer weights to the GPU in FP16 and the convert them on the GPU to FP32

    :param model:       Keras network model
    :param weights:     String representing data type for returned weights
    """
    [to_var.assign(from_var) for to_var, from_var in zip(model.trainable_weights, weights)]