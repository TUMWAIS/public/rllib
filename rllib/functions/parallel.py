import os
import psutil
from copy import deepcopy



class run_parallel(object):
    """
    Conditional function decorator for ray. Functions decorated with "run_parallel" are decorated with "ray.remote", if
    the environment variable "RLXPPU_RUN_IN_PARALLEL" is set to True.
    """
    def __init__(self, gpu=False, max_restarts=-1, max_task_retries=-1):
        self.gpu = gpu
        self.max_restarts = max_restarts
        self.max_task_retries = max_task_retries

    def __call__(self, func):

        if os.environ["RLLIB_RUN_IN_PARALLEL"] == "False":
            # Return the function unchanged, not decorated.
            return func
        else:
            import ray

            if "RLLIB_OBJECT_STORE_MEMORY" in os.environ:
                obj_store_mem = int(os.environ["RLLIB_OBJECT_STORE_MEMORY"]) * 1000000  # Convert to bytes
            else:
                obj_store_mem = None

            if self.gpu is False:
                self.decorator = ray.remote(num_gpus=0, object_store_memory=obj_store_mem, max_restarts=self.max_restarts, max_task_retries=self.max_task_retries)
            else:
                num_gpu_workers = int(os.environ["RLLIB_NUM_GPU_WORKERS"])
                self.decorator = ray.remote(num_gpus=1/num_gpu_workers, object_store_memory=obj_store_mem, max_restarts=self.max_restarts, max_task_retries=self.max_task_retries)

            return self.decorator(func)



def activate_parallel_exec(activate=False, num_gpu_workers=0, object_store_memory=None):
    """
    Sets environment variable for conditional function decorator "run_parallel"

    :param activate:        Boolean flag that indicated if ray shall be activated
    :param num_gpu_workers: Number of workers that shall access the GPU
    """
    os.environ["RLLIB_RUN_IN_PARALLEL"] = str(activate)

    if num_gpu_workers > 0:
        os.environ["RLLIB_NUM_GPU_WORKERS"] = str(num_gpu_workers)

    if object_store_memory is not None:
        os.environ["RLLIB_OBJECT_STORE_MEMORY"] = str(object_store_memory)



def initialize_parallel_exec(seed=None, debug_log=False):
    """
    Checks whether parallel execution is activated and initializes ray
    """
    if "RLLIB_RUN_IN_PARALLEL" in os.environ and os.environ["RLLIB_RUN_IN_PARALLEL"] == "True":
        # Import and initialize ray (for the whole module)
        global ray
        import ray

        if "RLLIB_OBJECT_STORE_MEMORY" in os.environ:
            obj_store_mem = int(os.environ["RLLIB_OBJECT_STORE_MEMORY"]) * 1000000  # Convert to bytes
        else:
            obj_store_mem = None

        if debug_log is True:
            log_level = 10
        else:
            log_level = 20

        num_cores = len(psutil.Process().cpu_affinity())
        ray.init(
                 num_cpus=num_cores,                           # Overwrite number of CPUs since ray does not detect correct number
                 logging_level = log_level,
                 dashboard_host="0.0.0.0",
                 ignore_reinit_error=True
                 )

        return True

    else:
        return False



def create_worker_instances(num_workers,
                            fixed_execution_order,
                            actor,
                            *args,
                            **kwargs):
    """
    Create single or multiple instances of an actor class for parallel or non-parallel execution

    :param num_workers: Integer indicating the number of workers in case of parallel execution
    :param actor_path:  Full path to actor class
    :param *args:       Any additional that need to be passed to the actor
    :return:            Ray actor pool object (multiprocessing) or python object
    """

    if "ActorClass" in str(type(actor)):
        actor_list = []
        actor_id_list = []
        for worker in range(num_workers):
            actor_id_list.append(worker)
            actor_list.append(actor.remote(worker, *args, **kwargs))
        pool = PrioActorPool(actor_list, actor_id_list, fixed_execution_order)
    else:
        pool = actor(0, *args, **kwargs)

    return pool



def preload_pool(pool,
                 fn_name,
                 fn_args=[],
                 fn_kwargs={}):
    """
    Submits a task to all free actors in a pool

    :param fixed_execution_order:   Boolean flag indicating whether to execute and retrieve results in a fixed order
    :param pool:                    Actor pool to which the tasks shall be sent
    :param function:                String of function name to be executed
    :param arguments:               Python list of arguments that need to be passed to the task
    """

    if "PrioActorPool" in str(type(pool)):
        num_idle_workers = len(pool.get_idle_actors())
        for actor in range(num_idle_workers):
            pool.submit(fn_name, fn_args, fn_kwargs)



def call_pool_method(pool,
                     fn_name,
                     fn_args=[],
                     fn_kwargs={}):
    """
    Executes a method of a parallel or non-parallel worker

    :param pool:        Actor pool from which a worker is picked
    :param method:      String representing the name of the methods
    :param args:        Arguments to pass to the method
    :return:            Return value of the method
    """
    if "PrioActorPool" in str(type(pool)):
        return_value = pool.call_method(fn_name, fn_args, fn_kwargs)
    else:
        return_value = eval("pool." + fn_name + "(*fn_args, **fn_kwargs)")

    return return_value



def submit_to_pool(pool,
                   fn_name,
                   fn_args=[],
                   fn_kwargs={},
                   worker_id=None):
    """
    Submits a task to an actor pool or a single actor

    :param pool:        Actor pool to which the tasks shall be sent
    :param function:    String of function name to be executed
    :param arguments:   Python list of arguments that need to be passed to the task
    """
    
    if "PrioActorPool" in str(type(pool)):
        pool.submit(fn_name, fn_args, fn_kwargs, worker_id)
    else:
        exec("pool." + fn_name + "(*fn_args, **fn_kwargs)")



def get_return_from_pool(pool,
                         fn_name,
                         fn_args=[],
                         fn_kwargs={},
                         skip_none=False,
                         num_jobs=1):
    """
    Sends jobs to pool and returns results. If actors are currently working on similar jobs, results for these
    previous submitted jobs are immediately returned

    :param fixed_execution_order:   Boolean flag indicating whether to execute and retrieve results in a fixed order
    :param pool:                    Ray actor pool object (multiprocessing) or python object
    :param function:                String of function name to be executed
    :param arguments:               Python list of arguments that need to be passed to the actor
    :param skip_none:               Boolean flag indicating that the retrieval of a return_value shall be repeated if the value is None
    :param num_jobs:                Number of jobs to execute and number of results to retrieve
    :return:                        Tuple containing the return values from the actor pool
    """
    
    # Get list of sampled trajectories
    return_values = []
    if "PrioActorPool" in str(type(pool)):

        # Submit all jobs
        for i in range(num_jobs):
            pool.submit(fn_name, fn_args, fn_kwargs)

        # Get finished jobs
        for i in range(num_jobs):
            while True:
                return_value = pool.get_next_from_result_list()

                if not skip_none or return_value is not None:
                    return_values.append(return_value)
                    break

    else:
        for i in range(num_jobs):
            return_values.append(eval("pool." + fn_name + "(*fn_args, **fn_kwargs)"))

    return return_values



def update_pools(list_of_pools):
    """
    Updates all pools provided in a list, i.e., stores return values from finished jobs and assigns workers
    new jobs from the queue

    :param list_of_pools:           List of actor pools to be updated
    :param fixed_execution_order:   Boolean flag indicating whether to retrieve results in a fixed order
    """
    for pool in list_of_pools:
        if "PrioActorPool" in str(type(pool)):
            pool.update()



class PrioActorPool:
    """
    Adapted ActorPool class from ray module in order to allow fixed assigment of jobs to actors.
    A separate buffer for finished jobs is also added in order to improve performance.
    A separate flag can be provided on initialization to ensure a deterministic behaviour of the pool.
    """

    def __init__(self, actors, actor_ids, fixed_execution_order=False):
        """
        Initializes the actor pool

        :param actors:                  List of actor handles to be included in the pool
        :param actor_ids:               List of actor ids of actors to be included in the pool
        :param fixed_execution_order:   Boolean flag indicating whether the pool should be have deterministically (harms performance)
        """
        self._fixed_execution_order = fixed_execution_order

        self._actors = list(actors)         # List of actors handles in the pool
        self._actor_id_list = actor_ids     # List of actor ids in the pool

        self._idle_actors = list(actors)    # List of idle actor handles
        self._future_to_actor = {}          # Get actor from future
        self._index_to_future = {}          # Get future from index
        self._next_task_index = 0           # Index of next task to execute
        self._next_return_index = 0         # Index of next task to return
        self._pending_submits = []          # Task queue
        self._result_list = []              # List of return values of finished tasks
        self._next_actor_idx = {}           # Dictionary managing the deterministic actor assignment

        # Create mapping between actor handles and actor ids
        self._actor_ids = {}
        for actor, actor_id in zip(actors, actor_ids):
            self._actor_ids[str(actor)] = actor_id



    def get_idle_actors(self):
        """
        Returns a list of idle actors

        :return:    List with handles of idle actors
        """
        return self._idle_actors



    def call_method(self, fn_name, args, kwargs):
        """
        Calls a provided method of an actor in the pool and immediately returns its result.

        :param method:  String representing the name of the actor method to be executed
        :param args:    Arguments to be passed to the function
        :param kwargs:  Keyword arguments to be passed to the function
        :return:        Return value of the called method
        """

        if self._fixed_execution_order is True:
            actor = self._actors[0]
        else:
            actor = self._idle_actors[0]

        handle = eval("actor." + fn_name + ".remote(*args, **kwargs)")
        return_value = ray.get(handle)

        return return_value



    def submit(self, fn_name, fn_args, fn_kwargs, actor_id=None):
        """
        Schedules a single task to run in the pool. If an actor ID is provided, the
        task is explicitely assigned to the actor with this ID. If the pool is
        initialized with the fixed_execution_order flag, tasks are assigned to actors
        in a fixed order and not based on free resources.

        :param fn_name:     String representing the name of the actor function to be executed
        :param fn_args:     Arguments to be passed to the function
        :param fn_kwargs:   Keyword arguments to be passed to the function
        :param actor_id:    Optional actor id of the actor to execute the function
        """

        # Set actor_id to enable deterministic actor assignment
        if actor_id is None and self._fixed_execution_order is True:
            actor_id = self._get_next_actor(fn_name)

        # Check for idle actors
        actor = None
        if self._idle_actors:
            if actor_id is not None:
                for idx, idle_actor in enumerate(self._idle_actors):
                    if actor_id == self._actor_ids[str(idle_actor)]:
                        actor = self._idle_actors.pop(idx)
                        break
            else:
                actor = self._idle_actors.pop()

        # Assign task to actor or queue task
        if actor is not None:
            fn = eval("lambda a, args, kwargs: a." + fn_name + ".remote(*args, **kwargs)")
            future = fn(actor, fn_args, fn_kwargs)
            self._future_to_actor[future] = (self._next_task_index, actor)
            self._index_to_future[self._next_task_index] = future
            self._next_task_index += 1
        else:
            self._pending_submits.append((fn_name, fn_args, fn_kwargs, actor_id))



    def update(self, wait_for_return=False):
        """
        Checks if jobs have finished and adds their return values to a list.
        Finished actors are assigned a new job from the queue.

        :param ordered:             Return values in submission order
        :param wait_for_return:     Force the function to wait for at least 1 finished job
        """
        if wait_for_return is True:
            timeout = None
        else:
            timeout = 0        # Only check if results are available, return_values=0 is unfortunately not supported

        # Check for new results until no new result is found
        return_found = True
        while return_found:
            if self._fixed_execution_order is False:
                flag, result = self.get_next_unordered(timeout)
            else:
                flag, result = self.get_next(timeout)

            if flag is True:
                self._result_list.append(result)
                timeout = 0                         # Only wait for first return
            else:
                return_found = False



    def get_next_from_result_list(self):
        """
        Returns the return value of a finished job from the list of finished jobs.
        If the list is empty, the function waits for at least one job to finish

        :param ordered:     Return values in submission order
        :return:            Return value of finished job
        """
        if not self._result_list:
            self.update(wait_for_return=True)
        result = self._result_list.pop(0)

        return result



    def has_next(self):
        """
        Returns whether there are any pending results to return.

        :return:  Flag indicating whether there are pending result
        """
        return bool(self._future_to_actor)



    def get_next(self, timeout=0):
        """
        Returns the next pending result in order, blocking for up to the specified
        timeout.

        :param timeout:     Integer representing the maximum wait time for results
        :return flag:       Flag indicating, whether a result is returned
        :return result:     Returned result
        """
        if self.has_next():
            future = self._index_to_future[self._next_return_index]
            res, _ = ray.wait([future], timeout=timeout)
            if res:
                del self._index_to_future[self._next_return_index]
                self._next_return_index += 1
                i, a = self._future_to_actor.pop(future)
                self._return_actor(a)
                return True, ray.get(future)

        return False, None



    def get_next_unordered(self, timeout=0):
        """
        Returns any of the next pending results blocking for up to the specified
        timeout. Unlike get_next(), the results are not always returned in same
        order as submitted, which can improve performance.

        :param timeout:     Integer representing the maximum wait time for results
        :return flag:       Flag indicating, whether a result is returned
        :return result:     Returned result
        """
        if self.has_next():
            res, _ = ray.wait(list(self._future_to_actor), num_returns=1, timeout=timeout)
            if res:
                [future] = res
                i, a = self._future_to_actor.pop(future)
                self._return_actor(a)
                del self._index_to_future[i]
                self._next_return_index = max(self._next_return_index, i + 1)
                return True, ray.get(future)

        return False, None



    def _return_actor(self, actor):
        """
        Returns an actor back to the list of idle actors and resubmits the next
        pending task.

        :param actor:   Handle to actor to be returned
        """
        self._idle_actors.append(actor)
        if self._pending_submits:

            # Check if there is a pending task explicitly scheduled on the returned actor
            submit_idx = None
            for idx, submit in enumerate(self._pending_submits):
                _, _, _, actor_id = submit
                if actor_id == self._actor_ids[str(actor)]:
                    submit_idx = idx
                    break
                else:
                    if actor_id is None and submit_idx is None:         # Otherwise, take oldest pending task that does not require a specific worker
                        submit_idx = idx

            if submit_idx is not None:                                  # Only submit in case of pending task that fits the worker
                self.submit(*self._pending_submits.pop(submit_idx))



    def _get_next_actor(self, fn_name):
        """
        Returns the next actor a tasks needs to be scheduled on if the pool is
        initialized with the fixes_execution_order flag. Actors ids are determined
        on an actor function name basis.

        :param fn_name:     String representing the function name for which an actor needs to be selected
        :return:            Actor id
        """
        # Initialize index on first function call
        if not fn_name in self._next_actor_idx:
            self._next_actor_idx[fn_name] = 0

        next_actor_id = self._actor_id_list[self._next_actor_idx[fn_name]]

        # Increase actor idx and restart at 0 if maximum index is exceeded
        self._next_actor_idx[fn_name] += 1
        if self._next_actor_idx[fn_name] > len(self._actors)-1:
            self._next_actor_idx[fn_name] = 0

        return next_actor_id