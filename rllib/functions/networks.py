import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np


def build_standard_input(num_input_vars,
                         input_name,
                         num_goal_input_vars=0,
                         dtype=None):
    """
    Creates a single network input and returns its input and output.

    :param num_input_vars:      Number of input variables in the input
    :param input_name:          Name of input
    :param num_goal_input_vars: Number of goal variables in the input (Universal value function approximation)
    :param dtype:               String describing the data type for each layer
    :return inputs:             List with all layer inputs
    :return outputs:            Layer output
    """
    inputs = []

    obs = tf.keras.Input(shape=(num_input_vars,), dtype=dtype, name=input_name)
    inputs.append(obs)

    if num_goal_input_vars > 0:
        goal = tf.keras.Input(shape=(num_goal_input_vars,), dtype=dtype, name=input_name + "_goal")
        output = tf.keras.layers.concatenate([obs, goal], dtype=dtype)
        inputs.append(goal)
    else:
        output = obs

    return inputs, output



def build_shared_feature_input(num_features,
                               input_name,
                               num_inputs=None,
                               dtype=None):
    """
    Creates an input to a shared feature embedding network of dimension (batch_size, num_inputs, num_features]

    :param num_features:                Number of input variables in each input
    :param input_name:                  The name of the input
    :param num_inputs:                  Number of inputs into the network (None to be set at first call)
    :return input:                      Network input  (batch_size, num_inputs, num_features)
    :return output:                     Network output (batch_size, num_inputs, num_features)
    """
    input = tf.keras.Input(shape=(num_inputs, num_features,),
                           dtype=dtype,
                           name=input_name)

    inputs = [input]
    output = input

    return inputs, output



def build_state_action_input(num_input_vars,
                             input_name,
                             num_action_input_vars,
                             num_goal_input_vars=0,
                             dtype=None):
    """
    Creates a network input to model a state-action-value function with action input

    :param num_input_vars:          Number of input variables in the input
    :param input_name:              Name of input
    :param num_goal_input_vars:     Number of goal variables in the input (Universal value function approximation)
    :param dtype:                   String describing the data type for each layer
    :return inputs:                 List with all layer inputs
    :return output:                 Layer output
    """
    inputs, state_output = build_standard_input(num_input_vars, input_name, num_goal_input_vars, dtype)

    actions = tf.keras.Input(shape=(num_action_input_vars,), dtype=dtype, name="action")
    inputs.append(actions)
    output = tf.keras.layers.concatenate([state_output, actions], dtype=dtype)

    return inputs, output



def build_mask_input(input_name,
                     dtype=None):
    """
    Creates a network input representing a feature mask.

    :param input_name:              Name of input
    :param dtype:                   String describing the data type for each layer
    :return inputs:                 List with all layer inputs
    :return output:                 Layer output
    """

    mask = tf.keras.Input(shape=(None, 1,),
                          name=input_name,
                          dtype=dtype)
    inputs = [mask]
    output = mask

    return inputs, output



def add_fully_connected_network(input,
                                hidden_layers,
                                num_output_vars=None,
                                output_name=None,
                                hidden_layer_activation="relu",
                                output_layer_activation="linear",
                                layer_normalization=False,
                                residual_connection=False,
                                dtype=None):
    """
    Adds a fully connected network to an input provided to the function and returns its output

    :param input:                       Input to which the network is added to
    :param hidden_layers:               List with number of neurons for each hidden layer
    :param num_output_vars:             Number of output variables in the network output (None for no output layer)
    :param output_name:                 String representing the name of the output layer
    :param hidden_layer_activation:     String describing the activation function for the hidden layers
    :param output_layer_activation:     String describing the activation function for the output layer
    :param layer_normalization:         Boolean flag indicating whether to use LayerNormalization after each dense layer
    :param residual_connection:         Boolean flag indicating whether to connect the layers with residual connections
    :param dtype:                       String describing the data type for each layer
    :return:                            Layer output
    """

    # Create hidden layers
    for i, num_neurons in enumerate(hidden_layers):
        output = tf.keras.layers.Dense(num_neurons,
                                  activation=hidden_layer_activation,
                                  dtype=dtype)(input)

        if residual_connection is True and i > 0:
            input = input + output
        else:
            input = output

        if layer_normalization is True:
            input = tf.keras.layers.LayerNormalization()(input)

    # Build output layer
    if num_output_vars is not None:
        output = tf.keras.layers.Dense(num_output_vars,
                                       activation=output_layer_activation,
                                       dtype=dtype,
                                       name=output_name)(input)
    else:
        output = input

    return output



def add_multi_head_attention_layer(target_input,
                                   source_input,
                                   mask_input,
                                   num_heads,
                                   use_bias=False,
                                   dtype=None):
    """
    Multi-head attention layer based on "Attention is all you need".

    :param target_input:    Target input for the attention layer [batch_size, num_target_entities, num_target_features]
    :param source_input:    Source input for the attention layer [batch_size, num_source_entities, num_source_features]
    :param mask_input:      Mask input to avoid attention on certain source inputs [batch_size, num_source_entities]
    :param num_heads:       Integer representing the number of attention heads to be used
    :param use_bias:        Flag indicating whether to use a bias when creating the quere, key, value matrices
    :param dtype:           String describing the data type for each layer
    :return:                Layer output
    """

    # Get shapes of target and source input
    batch_size, num_jobs, num_embd = get_shape_list(target_input)
    _, source_dim, _ = get_shape_list(source_input)

    # Reshape mask
    if mask_input is not None:
        mask = tf.transpose(tf.math.multiply(mask_input, tf.ones(shape=(batch_size, source_dim, num_jobs), dtype=target_input.dtype)), [0,2,1])
    else:
        mask = None

    # Create query, key, and value matrix
    layer_dim = int(num_embd // num_heads)
    query = tf.keras.layers.Dense(num_embd, use_bias=use_bias, dtype=dtype)(target_input)
    value = tf.keras.layers.Dense(num_embd, use_bias=use_bias, dtype=dtype)(source_input)
    key = tf.keras.layers.Dense(num_embd, use_bias=use_bias, dtype=dtype)(source_input)

    # Multi head attention layer
    output = tf.keras.layers.MultiHeadAttention(num_heads=num_heads,
                                                key_dim=layer_dim)(query, value, key, mask)

    return output



def add_duelling_network(input,
                         num_output_vars,
                         hidden_layers_value,
                         hidden_layers_advantage=None,
                         hidden_layer_activation="relu",
                         output_layer_activation="linear",
                         layer_normalization=False,
                         residual_connection=False,
                         combination="avg",
                         dtype=None):
    """
    Adds a duelling network architecture to a provided input

    :param input:                       Input to which the network is added to
    :param num_output_vars:             Number of output variables in the network output
    :param hidden_layers_value:         List with number of neurons for each hidden layer in the value network
    :param hidden_layers_advantage:     List with number of neurons for each hidden layer in the advantage network
    :param hidden_layer_activation:     String describing the activation function for the hidden layers
    :param output_activation:           String describing the activation function for the output layer
    :param layer_normalization:         Boolean flag indicating whether to use LayerNormalization after each dense layer
    :param residual_connection:         Boolean flag indicating whether to connect the layers with residual connections
    :param combination:                 String indicating how to combine the value and advantage stream ("avg", "max")
    :param dtype:                       String describing the data type for each layer
    :return:                            Network output
    """

    if hidden_layers_advantage is None:
        hidden_layers_advantage = hidden_layers_value

    # Create value network
    value = add_fully_connected_network(input=input,
                                        hidden_layers=hidden_layers_value,
                                        num_output_vars=1,
                                        hidden_layer_activation=hidden_layer_activation,
                                        output_layer_activation=output_layer_activation,
                                        layer_normalization=layer_normalization,
                                        residual_connection=residual_connection,
                                        dtype=dtype)

    value = tf.tile(value, [1, num_output_vars])

    # Create advantage network
    advantage = add_fully_connected_network(input=input,
                                            hidden_layers=hidden_layers_advantage,
                                            num_output_vars=num_output_vars,
                                            hidden_layer_activation=hidden_layer_activation,
                                            output_layer_activation=output_layer_activation,
                                            layer_normalization=layer_normalization,
                                            residual_connection=residual_connection,
                                            dtype=dtype)

    # Combine both networks
    if combination == "avg":
        q_value = value + advantage - tf.reduce_mean(advantage, axis=-1, keepdims=True)
    else:
        q_value = value + advantage - tf.reduce_max(advantage, axis=-1, keepdims=True)

    return q_value



def add_branching_network(input,
                          num_branches,
                          hidden_layers_value,
                          num_output_vars,
                          hidden_layers_advantage=None,
                          hidden_layer_activation="relu",
                          output_layer_activation="linear",
                          layer_normalization=False,
                          residual_connection=False,
                          combination="avg",
                          dtype=None):
    """
    Adds a branching network architecture to a provided input

    :param input:                       Input to which the network is added to
    :param num_branches:                Number of output branches
    :param num_outputs:                 Number of output neurons per branch
    :param hidden_layers_value:         List with number of neurons for each hidden layer in the value network
    :param hidden_layers_advantage:     List with number of neurons for each hidden layer in the advantage network
    :param hidden_layer_activation:     String describing the activation function for the hidden layers
    :param output_activation:           String describing the activation function for the output layer
    :param layer_normalization:         Boolean flag indicating whether to use LayerNormalization after each dense layer
    :param residual_connection:         Boolean flag indicating whether to connect the layers with residual connections
    :param combination:                 String indicating how to combine the value and advantage stream ("avg", "max")
    :param dtype:                       String describing the data type for each layer
    :return:                            List of network outputs
    """

    if hidden_layers_advantage is None:
        hidden_layers_advantage = hidden_layers_value

    # Create value network
    value = add_fully_connected_network(input=input,
                                        hidden_layers=hidden_layers_value,
                                        num_output_vars=1,
                                        hidden_layer_activation=hidden_layer_activation,
                                        output_layer_activation=output_layer_activation,
                                        layer_normalization=layer_normalization,
                                        residual_connection=residual_connection,
                                        dtype=dtype)

    value = tf.tile(value, [1, num_output_vars])      # TODO: To be checked  tf.tile(value, [1, 1])

    # Create advantage network
    outputs = []
    for i in range(num_branches):
        advantage = add_fully_connected_network(input=input,
                                                hidden_layers=hidden_layers_advantage,
                                                num_output_vars=num_output_vars,
                                                hidden_layer_activation=hidden_layer_activation,
                                                output_layer_activation=output_layer_activation,
                                                layer_normalization=layer_normalization,
                                                residual_connection=residual_connection,
                                                dtype=dtype)

        # Combine both networks (Q = V + A - mean(A)
        if combination == "avg":
            q_value = value + advantage - tf.reduce_mean(advantage, axis=-1, keepdims=True)
        else:
            q_value = value + advantage - tf.reduce_max(advantage, axis=-1, keepdims=True)

        outputs.append(q_value)

    return outputs




def add_actor_critic_head(input,
                          num_output_vars,
                          dtype=None):
    """
    Adds an actor-critic head with one output representing the action distribution and
    one head representing the value function

    :param input:               Input to which the network is added to
    :param num_output_vars:     Number of output variables in the action distribution head
    :param dtype:               String describing the data type for each layer
    :return:                    Output of the added network
    """

    dist = tf.keras.layers.Dense(num_output_vars,
                                activation="linear",
                                dtype=dtype,
                                name="actor_head")(input)

    value = tf.keras.layers.Dense(num_output_vars=1,
                                  activation="linear",
                                  dtype=dtype,
                                  name="critic_head")(input)

    return [dist, value]



def add_cont_actor_critic_head(input,
                               num_output_vars,
                               dtype=None):
    """
    Adds an actor-critic head for continuous actions. The first output is representing the continuous action and the
    second output the value function.

    :param input:               Input to which the network is added to
    :param num_output_vars:     Number of output variables in the action distribution head
    :param dtype:               String describing the data type for each layer
    :return:                    Output of the added layer
    """
    mean = tf.keras.layers.Dense(num_output_vars,
                                 activation="linear",
                                 name="actor_head_mean",
                                 dtype=dtype)(input)

    log_stds = tf.keras.layers.Dense(num_output_vars,
                                     activation="linear",
                                     name="actor_head_std",
                                     dtype=dtype)(input)

    actions = mean + tf.random.normal(shape=tf.shape(mean), dtype=dtype) * tf.math.exp(log_stds)

    value = tf.keras.layers.Dense(num_output_vars,
                                  activation="linear",
                                  name="critic_head",
                                  dtype=dtype)(input)

    return [actions, value]



def add_deterministic_distribution_head(inputs,
                                        num_output_vars,
                                        action_bounds=None,
                                        dtype=None):
    """
    Adds a head that returns deterministic continuous actions.

    :param inputs:              Input to which the network is added to
    :param num_output_vars:     Number of output variables in the network output
    :param dtype:               String describing the data type for each layer
    :params action_bounds:      An optional list with a lower and upper bound for the actions (same bounds for all actions)
    :return:                    Outputs of the added layer
    """
    actions = tf.keras.layers.Dense(num_output_vars,
                                    activation="tanh",
                                    dtype=dtype,
                                    name="action_head")(inputs)

    # Shift and scale actions to defined range
    if action_bounds is not None:
        scale = (action_bounds[1] - action_bounds[0]) / 2
        offset = (action_bounds[1] + action_bounds[0]) / 2
    else:
        scale = 1
        offset = 0

    actions = (actions * scale) + offset

    return actions



def add_distribution_head(inputs,
                          num_output_vars,
                          clip_std=None,
                          action_factor=1,
                          action_shift=0,
                          dtype=None,
                          eps=1e-6):
    """
    Adds a mean and standard deviation head to represent continuous distributions

    :param inputs:              Input to which the network is added to
    :param num_output_vars:     Number of output variables in the network output
    :param dtype:               String describing the data type for each layer
    :return:                    Outputs of the added network
    """

    mean = tf.keras.layers.Dense(num_output_vars,
                                 activation=None,
                                 dtype=dtype,
                                 name="mean_head")(inputs)

    log_stds = tf.keras.layers.Dense(num_output_vars,
                                     activation=None,
                                     dtype=dtype,
                                     name="std_head")(inputs)

    if clip_std is not None:
        log_stds = tf.clip_by_value(log_stds, clip_std[0], clip_std[1])

    stds = tf.math.exp(log_stds)

    actions = mean + tf.random.normal(shape=tf.shape(mean), dtype=dtype) * stds

    # Calculate log probability
    log_prob = tfp.distributions.Normal(loc=mean, scale=stds).log_prob(actions)

    # Reparametrization trick
    actions = tf.math.tanh(actions)
    mean = tf.math.tanh(mean)
    log_prob = tf.reduce_sum(log_prob - tf.math.log(1 - actions ** 2 + eps), axis=1, keepdims=True)

    # Shift and scale actions to defined range
    actions = (actions * action_factor) + action_shift
    mean = (mean * action_factor) + action_shift

    return [actions, mean, log_prob]



def concatinate_outputs(output_list,
                        dtype=None):
    """
    Concatenates outputs provided in a list

    :param output_list: Lists of outputs to Concatenated
    :param dtype:       String describing the data type for each layer
    :return:            Concatenated layer output
    """
    if len(output_list) > 1:    # Allows function to be called with a list containing a single output
        output = tf.keras.layers.concatenate(output_list, dtype=dtype)
    else:
        output = output_list[0]

    return output



def masked_avg_pooling(input, axis, mask_input=None):
    """
    Masked average pooling layer.
    The average is only calculated across unmasked entities.

    :param output:          Input to be average pooled
    :param axis:            Integer representing the axis to pool across
    :param mask_input:      Mask to apply to input [batch_size, num_entities]
    :return:                Masked input
    """
    if mask_input is not None:
        mask_input = tf.cast(mask_input, input.dtype)
        masked_output = tf.math.multiply(mask_input, input)
    else:
        masked_output = input
        mask_input = tf.ones_like(input)

    summed = tf.reduce_sum(masked_output, axis=axis)
    denom = tf.reduce_sum(mask_input, axis=axis) + 1e-5     # Avoid division by 0

    return summed / denom



def masked_max_pooling(output, axis, mask_input=None):
    """
    Masked max pooling layer.
    The maximum is only calculated across unmasked entities.

    :param output:          Input to be max pooled
    :param axis:            Integer representing the axis to pool across
    :param mask_input:      Mask to apply to input [batch_size, num_entities]
    :return:                Masked input
    """
    if mask_input is not None:
        has_unmasked_entities = tf.sign(tf.reduce_sum(mask_input))
        offset = (mask_input - 1) * 1e9
        output = (output + offset) * has_unmasked_entities

    return tf.reduce_max(output, axis=axis)



def get_shape_list(x):
    """
    Returns the shape of a tensor-
    Automatically deals with dynamic shapes.
    Based on OpenAI. Seems to be better than always using the results of tf.shape().

    :param x:   Tensor of which the shape should be returned
    :return:    List with integers representing the dimensions of the tensor
    """
    fixed_shape = x.get_shape().as_list()
    dynamic_shape = tf.shape(x)

    shape_list = [dynamic_shape[i] if fixed_shape[i] is None else fixed_shape[i] for i in range(len(fixed_shape))]

    return shape_list





# Work in progress: Implementation of noisy DQN (see rainbow Paper) (feel free to contribute)
class NoisyDense(tf.keras.layers.Dense):

    def __init__(self,
                 *args,
                 noisy_distribution="factorized",
                 **kwargs):

        super(NoisyDense, self).__init__(*args, **kwargs)
        self.noisy_distribution = noisy_distribution


    def func(self, e_list):
        return tf.multiply(tf.sign(e_list), tf.pow(tf.abs(e_list), 0.5))


    def build(self, input_shape):
        super(NoisyDense, self).build(input_shape)

        self.noisy_kernel = self.add_weight(
                                            'noisy_kernel',
                                            shape=[input_shape[-1], self.units],
                                            initializer=self.kernel_initializer,
                                            regularizer=self.kernel_regularizer,
                                            constraint=self.kernel_constraint,
                                            dtype=self.dtype,
                                            trainable=True)

        if self.use_bias:
            self.noisy_bias = self.add_weight(
                                                'noisy_bias',
                                                shape=[self.units, ],
                                                initializer=self.bias_initializer,
                                                regularizer=self.bias_regularizer,
                                                constraint=self.bias_constraint,
                                                dtype=self.dtype,
                                                trainable=True)
        else:
            self.noisy_bias = None


        if self.noisy_distribution == 'independent':
            self.noise1 = tf.random.normal(shape=tf.shape(self.kernel))
        elif self.noisy_distribution == 'factorized':
            self.noise1 = self.func(tf.random.normal(shape=tf.shape(self.kernel), dtype=self.dtype))
            self.noise2 = self.func(tf.random.normal(shape=[self.units,], dtype=self.dtype))

        if self.noisy_distribution == 'independent':
            self.noise_bias = tf.random.normal(shape=[self.units,])


    def call(self, inputs):
        self.noisy_kernel_mul = tf.multiply(self.noise1 * self.noise2, self.noisy_kernel)
        self.noisy_bias_mul = tf.multiply(self.noise2, self.noisy_bias)
        return tf.matmul(inputs, self.noisy_kernel_mul) + self.noisy_bias_mul + tf.matmul(inputs, self.kernel) + self.bias



# Work in progress
def custom_multi_head_attention(input,
                                mask_input,
                                num_heads,
                                dtype=None,
                                bias=False):
    """
    Multi-head attention implementation based on OpenAIs Hide & Seek.

    :param input:
    :param mask_input:
    :param num_heads:
    :param dtype:
    :return:
    """

    # Get shape data
    batch_size = tf.shape(input)[0]         # Dynamic axis
    num_jobs = input.shape[1]
    num_embd = input.shape[2]

    # Create Query, Key, and Value matrix
    query = tf.keras.layers.Dense(num_embd, use_bias=bias, dtype=dtype)(input)                     # (batch_size, jobs, num_embd)
    value = tf.keras.layers.Dense(num_embd, use_bias=bias, dtype=dtype)(input)                     # (batch_size, jobs, num_embd)
    key = tf.keras.layers.Dense(num_embd, use_bias=bias, dtype=dtype)(input)                       # (batch_size, jobs, num_embd)

    query = tf.reshape(query, (batch_size, num_jobs, num_heads, num_embd // num_heads))             # (batch_size, jobs, heads, embd/heads)
    key = tf.reshape(key, (batch_size, num_jobs, num_heads, num_embd // num_heads))                 # (batch_size, jobs, heads, embd/heads)
    value = tf.reshape(value, (batch_size, num_jobs, num_heads, num_embd // num_heads))             # (batch_size, jobs, heads, embd/heads)

    query = tf.transpose(query, (0, 2, 1, 3))                                                       # (batch_size, heads, jobs, embd/heads)
    key = tf.transpose(key, (0, 2, 3, 1))                                                           # (batch_size, heads, embd/heads, jobs)
    value = tf.transpose(value, (0, 2, 1, 3))                                                       # (batch_size, heads, jobs, embd/heads)

    # Apply attention weights
    logits = tf.matmul(query, key) / np.sqrt(num_embd / num_heads)                                  # (batch_size, heads, jobs, jobs)
    softmax = masked_softmax(logits, mask_input)
    attention_sum = tf.matmul(softmax, value)                                                       # (batch_size, heads, jobs, embd/heads)

    # Reshape outputs
    output = tf.transpose(attention_sum, (0,2,1,3))                                                 # (batch_size, jobs, heads, embd/heads)
    output = tf.reshape(output, (batch_size, num_jobs, num_embd))                                   # (batch_size, jobs, num_embd)

    return output



def masked_softmax(input, mask_input=None):
    """
    Masked softmax

    :param input:       Input to apply softmax to
    :param mask_input:  (batch_size, jobs)
    :return:
    """

    if mask_input is not None:
        mask = tf.expand_dims(mask_input, axis=1)   # (batch_size, 1, jobs)
        mask = tf.expand_dims(mask, axis=1)         # (batch_size, 1, 1, jobs)
        input -= (1.0 - mask) * 1e10

    #  Subtract the max logit from everything so we don't overflow
    input -= tf.math.reduce_max(input, axis=-1, keepdims=True)
    unnormalized_p = tf.exp(input)

    #  Mask the unnormalized probibilities and then normalize and remask
    if mask_input is not None:
        unnormalized_p *= mask
    normalized_p = unnormalized_p / (tf.math.reduce_sum(unnormalized_p, axis=-1, keepdims=True) + 1e-10)
    if mask_input is not None:
        normalized_p *= mask

    return normalized_p