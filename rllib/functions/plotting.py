import matplotlib.pyplot as plt
import os
import json
import numpy as np
from math import sqrt
import __main__
import csv

from rllib.functions.saving import load_all_history_files, get_datetimestamp_as_string


def export(variable,
           source_file,
           moving_average=None,
           skip=None,
           export_path=None,
           max_episodes=None,
           var_limits=[None, None]):
    """
    Exports data from several training runs into a .csv file for use within latex. Each run group represents a separate
    column in the output file. Data of the runs within a run group is averaged.

    The paths to the training history files need to be provided via a JSON file with the structure:
    {
    "run group 1": [
                    <Path to first run of run group>,
                    <Path to second run of run group>,
                   ]
    }

    :param variable:        Variable to export
    :param source_file:     JSON file with paths to training history files
    :param moving_average:  None or number of episodes for the moving average
    :param skip:            Integer i to only plot every ith episode
    :param export_path:     Path of output file
    :param max_episodes:    Maximum number of episodes to include
    """

    # Load data
    root_path = os.path.dirname(os.path.abspath(__main__.__file__))
    sources_path = os.path.join(root_path, source_file)
    with open(sources_path) as json_file:
        sources = json.load(json_file)

    # Load x data
    run_path = os.path.join(root_path,list(sources.values())[0][0])
    if moving_average is not None:
        x = load_all_history_files(run_path)["episode"][:-moving_average+1]
    else:
        x = load_all_history_files(run_path)["episode"]   # Get x coordinates from first file

    # Load y data
    y = []
    for i, source in enumerate(sources.values()):
        y.append([])
        for j, seed in enumerate(source):
            run_path = os.path.join(root_path, seed)
            ydata = load_all_history_files(run_path)

            if moving_average is not None:
                y[i].append(np.convolve(ydata[variable], np.ones((moving_average,)) / moving_average, mode='valid'))
            else:
                y[i].append(ydata[variable])
            print("Num of Episodes", len(y[i][j]))

    # Get export path (if not provided)
    if export_path is None:
        export_path = os.path.join(root_path, ("plot_" + get_datetimestamp_as_string()))
    else:
        export_path = os.path.join(root_path, export_path)

        # Write data to csv
    with open(export_path, mode='w', newline='') as export_file:
            csv_writer = csv.writer(export_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            if max_episodes is None:
                max_episodes = len(x)

            for i in range(max_episodes):
                if skip is None or i % skip == 0:
                    write_csv = True
                    data = []
                    data.append(x[i])

                    for j, source in enumerate(sources.values()):
                        tmp = []
                        for k, seed in enumerate(source):
                            tmp.append(y[j][k][i])

                        # Skip entry if any value is None
                        if not None in tmp:
                            data.append(np.mean(tmp))

                            if var_limits[0] is None or (np.mean(tmp) - np.std(tmp)) > var_limits[0]:
                                data.append(np.mean(tmp) - np.std(tmp))
                            else:
                                data.append(var_limits[0])

                            if var_limits[1] is None or (np.mean(tmp) + np.std(tmp)) < var_limits[1]:
                                data.append(np.mean(tmp) + np.std(tmp))
                            else:
                                data.append(var_limits[1])

                        else:
                            write_csv = False
                            break

                    if write_csv:
                        csv_writer.writerow(data)



def plot(data_path,
         x_name = None,
         y_name = None,
         moving_average = None,
         xlimits=None,
         layout_path=None,
         save=True,
         save_path=None,
         plotsize=(15, 7)):
    """
    Creates a plot from data provided through a pickled dictionary

    :param data_path:       Path to .pkl file containing the data to plot
    :param x_name:          Name of x-values to plot
    :param y_name:          Name of y-values to plot
    :param moving_average:  Integer representing the number of datapoints for the moving average or None
    :param xlimits:         A list containing the start and end x-value to include [xstart, xend]
    :param layout_path:     A json file containing the variables to be ploted. Previous 4 arguments are ignored if file is provided
    :param save:            If false, plot is opened instead of saved to file
    :param save_path:       Save path for plot. If not provided a subfolder is created at data_path
    :param plotsize:        A tuple describing the size of the plot
    :return:                Pyplot figure object   
    """

    # Load data
    data = load_all_history_files(data_path)

    # Load plot layout
    if x_name is None or y_name is None:
        if layout_path is not None:
            with open(layout_path) as json_file:
                layout = json.load(json_file)
        else:
            layout = get_standard_layout()
    else:
        layout = {}
        layout["subplot1"] = {"x": x_name,
                              "y": y_name,
                              "moving_average": moving_average}

    # Calculate a feasible subplot structure for layout
    num_rows, num_columns = get_plot_structure(layout)

    # Create figure
    fig = plt.figure(figsize=plotsize)
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.35, hspace=0.5)

    for i, (subplot, subplot_data) in enumerate(layout.items()):

        # Get data to plot
        if xlimits is None:
            x = data[subplot_data["x"]]
            y = data[subplot_data["y"]]
        else:
            x = data[subplot_data["x"][xlimits[0]:xlimits[1]]]
            y = data[subplot_data["x"][xlimits[0]:xlimits[1]]]

        x, y = remove_empty_values(x, y)

        # Create plot
        subplot = fig.add_subplot(num_rows, num_columns, i+1)
        subplot.plot(x, y)

        # Add moving average
        if "moving_average" in subplot_data.keys() and subplot_data["moving_average"] is not None:
            subplot.plot(x[:-subplot_data["moving_average"]+1], np.convolve(y, np.ones((subplot_data["moving_average"],))/subplot_data["moving_average"], mode='valid'))

        # Set plot title
        if "name" in subplot_data.keys():
            subplot.set_title(subplot_data["name"])
        else:
            subplot.set_title(subplot_data["y"])

    if save is True:
        if save_path is None:
            filename = "plot_" + get_datetimestamp_as_string()
            save_path = os.path.join(os.path.dirname(data_path), "plots", filename)

        if not os.path.exists(os.path.dirname(save_path)):
            os.makedirs(os.path.dirname(save_path))

        plt.savefig(save_path)
        plt.close()
    else:
        plt.show()

    return fig



def get_plot_structure(layout):
    """
    Returns an integer encoding the subplot structure, e.g., 1x3, 2x3

    :param layout:  Dictionary describing the plot layout
    :return:        Integer encoding the subplot structure
    """

    # Calculate plot dimensions
    num_plots = len(layout)
    num_rows = sqrt(num_plots/2)
    if 2*(round(num_rows)*round(num_rows)) >= num_plots:
        num_columns = 2* num_rows
    else:
        num_columns = 2 * num_rows + 1

    return round(num_rows), round(num_columns)



def remove_empty_values(x, y):
    """
    Removes none values from data

    :param x:   List of x values
    :param y:   List of y values
    :return x:  List of x values without none value
    :return y:  List of y values without none value
    """
    x_clean = []
    y_clean = []
    for v_x, v_y in zip(x, y):
        if v_x is not None and v_y is not None:
            x_clean.append(v_x)
            y_clean.append(v_y)

    return x_clean, y_clean



def get_standard_layout():
    """
    Returns a fallback layout

    :return: Dictionary with the fallback layout
    """

    layout={}
    layout["subplot1"] = {"x": "episode",
                          "y": "mean_reward",
                          "moving_average": 100,
                          "name": "Mean Reward"}

    layout["subplot2"] = {"x": "episode",
                          "y": "mean_rel_reward",
                          "moving_average": 100,
                          "name": "Standardized Mean Reward"}

    layout["subplot3"] = {"x": "episode",
                          "y": "q_value",
                          "moving_average": 100,
                          "name": "Q-Value"}

    layout["subplot4"] = {"x": "episode",
                          "y": "epsilon",
                          "moving_average": None,
                          "name": "Epsilon"}

    layout["subplot5"] = {"x": "episode",
                          "y": "replay_buffer_total_size",
                          "moving_average": None,
                          "name": "Replay Buffer Size"}

    layout["subplot6"] = {"x": "episode",
                          "y": "episode_time",
                          "moving_average": 100,
                          "name": "Episode Time"}

    return layout



def scatter_plot(input_points, path):
    """
    Plots coordinates into a wireframe model of the xppu.
    
    :param input_points: List of coordinates represented as lists [x, y, z]
    :param path:         Save path for plot.   
    :return:             Pyplot figure object   
    """
    
    fig = plt.figure(figsize=(25, 25))
    ax = fig.add_subplot(111, projection='3d')

    theta = np.linspace(0, 2 * np.pi, 201)
    x = 21.9584 * np.cos(theta)
    y = 21.9584 * np.sin(theta)

    ax.plot(74.4 + x, 27.0 + y, np.ones([x.shape[0], ]) * 12.8, color='dimgray')

    # Stack
    p1 = [103.8773, 27.0000, 6.8000]    # Pos: initial
    p2 = [96.3584, 27.0000, 6.8000]     # Pos: at_stack
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    # Crane
    p1 = [96.3584, 27.0000, 6.8000]     # Pos: at_stack
    p2 = [96.3584, 27.0000, 12.8000]    # Pos: at_stack_up
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    p1 = [52.4416, 27.0000, 12.8000]    # Pos: at_stamp_up
    p2 = [52.4416, 27.0000, 6.8000]     # Pos: at_stamp
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    p1 = [74.4000, 48.9584, 12.8000]    # Pos: at_lsc_pos0_up
    p2 = [74.4000, 48.9584, 6.8000]     # Pos: at_lsc_pos0
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    # LSC
    p1 = [74.4000, 48.9584, 6.8000]     # Pos: at_lsc_pos0
    p2 = [74.4000, 102.0000, 6.8000]    # Pos: end_of_lsc
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    # Ramp 1
    p1 = [74.4000, 70.5000, 6.8000]     # Pos: at_lsc_ramp1
    p2 = [55.0000, 70.5000, 1.356]      # Pos: at_ramp1_pos1
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    # Ramp 2
    p1 = [74.4000, 98.9584, 6.8000]     # Pos: at_lsc_ramp2
    p2 = [55.0000, 98.5000, 1.3568]     # Pos: at_ramp2_pos1
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    # Ramp 3
    p1 = [74.4000, 102.0000, 6.8000]    # Pos: at_lsc_ramp3
    p2 = [74.4000, 120.8741, 0.6808]    # Pos: at_ramp3_pos1
    ax.plot3D([p1[0], p2[0]], [p1[1], p2[1]], [p1[2], p2[2]], linestyle='-', color='dimgray')

    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    for i in range(len(input_points)):
        p1 = input_points[i][0]
        p2 = input_points[i][1]
        ax.plot3D([p1[0]], [p1[1]], [p1[2]], 'o', label='goal_' + str(i) + ' - ' + str(p1), color='C' + str(i))
        ax.plot3D([p2[0]], [p2[1]], [p2[2]], 'v', label='achieved_' + str(i) + ' - ' + str(p2), color='C' + str(i))

    plt.legend()

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    plt.savefig(path)
    plt.close()

    return fig