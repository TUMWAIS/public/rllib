import argparse

from rllib.functions.parallel import activate_parallel_exec


def get_cli_parameters():
    """
    Very basic command line interface that returns the configuration file path and
    activates parallel execution

    :return:    String representing path to configuration file
    """

    parser = argparse.ArgumentParser()

    parser.add_argument("parameter_file")
    parser.add_argument("parallel")

    args = parser.parse_args()

    # Activate parallel execution and import Actors
    if args.parallel == "True":
        activate_parallel_exec(args.parallel)
    else:
        activate_parallel_exec(False)

    config_file_path = args.parameter_file

    return config_file_path