import git
import os


def get_git_commit_from_dir(git_dir):
    """
    Searches the provided folder and its parent directories for a git repository
    and returns the checked out commit hash and message

    :param git_dir:         Path to directory within git repository
    :return commit_hash:    Checked out commit hast
    :return commit_msg:     Commit message of checked out commit
    """
    try:
        repo = git.Repo(git_dir, search_parent_directories=True)
        commit_hash = repo.head.object.hexsha
        commit_msg = repo.commit(commit_hash).message
    except:
        commit_hash = ""
        commit_msg = ""

    return  commit_hash, commit_msg



def get_module_commit(module_name):
    """
    Returns the checked out git commit of a module

    :param module_name:     String representing the name of the module
    :return commit_hash:    Checked out commit hast
    :return commit_msg:     Commit message of checked out commit
    """

    exec("import " + module_name)
    module_init = eval(module_name + ".__file__")
    dir = os.path.dirname(module_init)

    commit_msg, commit_hash = get_git_commit_from_dir(dir)

    return commit_hash, commit_msg
