from rllib.classes.errors.ConfigurationError import ConfigurationError



def reduce_learning_rate(params, learning_rate, learning_rate_counter):
    """
    Decreases the learning rate according to the learning rate schedule defined in params.

    :param params:                  Parameter object containing the training configuration
    :param learning_rate:           Float representing the current learning rate
    :param learning_rate_counter:   Integer representing the number of learning rate updates since start of training (only for PiecewiseLinear)
    :return:                        Float representing the reduced learning rate
    """
    if params.learning_rate_schedule == "ExponentialDecay":
        new_learning_rate = max(learning_rate * params.learning_rate_decay_rate, params.learning_rate_min)

    elif params.learning_rate_schedule == "PiecewiseLinear":
        learning_rate_counter += 1
        new_learning_rate = learning_rate
        for i in range(len(params.learning_rate_boundaries)-1):
            if learning_rate_counter >= params.learning_rate_boundaries[i] and learning_rate_counter < params.learning_rate_boundaries[i+1]:
                new_learning_rate = (params.learning_rate_values[i + 1] - params.learning_rate_values[i]) / (
                                     params.learning_rate_boundaries[i + 1] - params.learning_rate_boundaries[i]) * (
                                     learning_rate_counter - params.learning_rate_boundaries[i]) + params.learning_rate_values[i]

    else:
        raise ConfigurationError(params.learning_rate_schedule, "Learning rate schedule not found")

    return new_learning_rate



def adjust_batch_size(params, batch_size, episode):
    """
    Adjusts the batch size according to the defined schedule.

    :param params:          Parameter object containing the training configuration
    :param batch_size:      Integer representing the current batch size
    :param episode:         Current episode
    :return:                Integer representing the adjusted batch size
    """
    new_batch_size = batch_size
    for i in range(len(params.batch_size_boundaries) - 1):
        if episode >= params.batch_size_boundaries[i] and episode < params.batch_size_boundaries[i + 1]:
            new_batch_size = (params.batch_size_values[i + 1] - params.batch_size_values[i]) / (
                              params.batch_size_boundaries[i + 1] - params.batch_size_boundaries[i]) * (
                              episode - params.batch_size_boundaries[i]) + params.batch_size_values[i]

    return int(new_batch_size)



def reduce_epsilon(params,
                   epsilon,
                   epsilon_counter=None):
    """
    Decreases epsilon according to the exploration schedule defined in params.

    :param params:              Parameter object containing the training configuration
    :param epsilon:             Float representing the current value of epsilon
    :param epsilon_counter:     Integer representing the number of epsilon updates since start of training (only for PiecewiseLinear)
    :return:                    Float representing the reduced epsilon
    """

    if params.exploration_schedule == "Exponential":
        if epsilon > params.epsilon_min:
            new_epsilon = epsilon*params.epsilon_decay
        else:
            new_epsilon = epsilon

    elif params.exploration_schedule == "PiecewiseLinear":
        epsilon_counter += 1
        new_epsilon = epsilon
        for i in range(len(params.epsilon_boundaries)-1):
            if epsilon_counter >= params.epsilon_boundaries[i] and epsilon_counter <= params.epsilon_boundaries[i+1]:
                new_epsilon =  (params.epsilon_values[i+1] - params.epsilon_values[i])/(params.epsilon_boundaries[i+1] - params.epsilon_boundaries[i]) * (epsilon_counter - params.epsilon_boundaries[i]) + params.epsilon_values[i]
                break

    elif params.exploration_schedule is None:
        new_epsilon = epsilon

    else:
        raise ConfigurationError(params.exploration_schedule, "Exploration schedule not found")

    return new_epsilon, epsilon_counter
