import os
import shutil
import pickle
import time
import gc
import numpy as np
import h5py
from datetime import datetime


def load_from_file(path):
    """
    Loads a pickled object from a file. Returns None if file does not exist.
    Pickle is more flexibel than hdf5, but consumes significantly more memory during saving.

    :param path:    Full path to file
    :return:        Loaded object or None in case of error
    """

    try:
        file = open(path, "rb")
        obj = pickle.load(file)
        file.close()
    except:
        obj = None

    return obj



def save_to_file(path, obj):
    """
    Pickles an object and saves it to a file
    Pickle is more flexibel than hdf5, but consumes significantly more memory during saving.

    :param path:  Full path to file
    :param obj:   Object to be saved
    """

    # Create directory if not existing
    folder = os.path.dirname(path)
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Save file
    file = open(path, 'wb')
    pickle.dump(obj, file)
    file.close()
    gc.collect()



def save_weight_list_to_file(file, object):
    """
    Writes a list of weights to a file in the hdf5 format.
    Compared to pickle, the read and write process is much less memory intensive.

    :param file:        Path to HDF5 file
    :param object:      List of weights
    """

    # Create directory if not existing
    folder = os.path.dirname(file)
    if not os.path.exists(folder):
        os.makedirs(folder)

    with h5py.File(file, "w") as file:
        for i in range(len(object)):
            group = file.create_group("weights_" + str(i))
            for j in range(len(object[i])):
                group.create_dataset("layer_" + str(j), data=np.array(object[i][j]), compression="gzip")



def load_weight_list_from_file(file):
    """
    Loads a list of weights from a file in the hdf5 format.
    Compared to pickle, the read and write process is much less memory intensive.

    :param file:        Path to HDF5 file
    :return:            List of weights
    """
    try:
        # Read column data from file
        data = []
        with h5py.File(file, "r") as file:
            for i in range(len(file)):
                data.append([])
                for j in range(len(file["weights_" + str(i)])):
                    data[-1].append(file["weights_" + str(i)]["layer_" + str(j)][:])

        return data

    except:
        return None



def save_history_to_multiple_files(path,
                                   obj,
                                   max_file_size,
                                   file_id):
    """
    Saves the training history to multiple files. If a file exceeds the number of entries defined by max_file_size,
    a new file is started. This also keeps the number of entries stored in memory
    low.

    :param path:             Path to file without ID and type extension, i.e., /training/history
    :param obj:              Object that should be written to the file
    :param max_file_size:    Maximum number of entries per file
    :param file_id:          ID of latest file
    :return file_id:         ID of the latest file (might be increased compared to the provided ID)
    """

    # Create directory if not existing
    folder = os.path.dirname(path)
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Save history to file by overwriting existing one
    current_file_path = path + "_" + str(file_id) + ".pkl"
    with h5py.File(current_file_path, "w") as file:
        for key, value in obj.items():
            file.create_dataset(key, data=np.array(value), compression="gzip")

    # Check if maximum history length is reached and clear history
    if len(list(obj.values())[0]) >= max_file_size:
        file_id += 1
        for key in obj:
            obj[key] = []

    return file_id



def load_latest_history_file(path):
    """
    Loads the latest training history file

    :param path:        Path to file without ID and type extension, i.e., /training/history
    :return data:       Dictionary containing the data
    :return file_id:    ID of the latest file from which the data is read
    """

    # Identify latest history file
    file_id = 0
    while True:
        current_file_path = path + "_" + str(file_id + 1) + ".pkl"
        if not os.path.exists(current_file_path): break
        file_id += 1

    # Load data from latest file into history dictionary
    current_file_path = path + "_" + str(file_id) + ".pkl"
    with h5py.File(current_file_path, "r") as file:
        data = {}
        for key, value in file.items():
            data[key] = value[:]

    return data, file_id



def load_all_history_files(path):
    """
    Loads the complete training history from multiple files

    :param path:    Path to file without ID and type extension, i.e., /training/history
    :return data:   Dictionary containing the data
    """
    file_id = 0
    data = {}

    # Load history data from files
    while True:
        current_file_path = path + "_" + str(file_id) + ".pkl"
        file_id += 1

        if os.path.exists(current_file_path):
            with h5py.File(current_file_path, "r") as file:
                for key, value in file.items():
                    if not key in data:
                        data[key] = []
                    data[key].extend(file[key])

        else:
            break

    return data



def delete_old_files(path, warning_flag=False):
    """
    Deletes all existing file parts

    :param params: Parameter object
    """
    if os.path.exists(path):
        if warning_flag is True:
            print("------------------------------------------------------")
            print("OLD TRAINING DATA IS DELETED IN 30 SECONDS!")
            print("------------------------------------------------------")
            time.sleep(30)

        shutil.rmtree(path)



def get_datetimestamp_as_string():
    """
    Creates a date-time-stamp, i.e., <year><month><day>_<hour><minute><second>, to be used as a filename extension

    :return: String representing the date-time-stamp
    """
    year = datetime.now().year
    month = datetime.now().month
    day = datetime.now().day
    hour = datetime.now().hour
    minute = datetime.now().minute
    second = datetime.now().second

    datetimestamp = "{0:4.0f}{1:02.0f}{2:02.0f}_{3:02.0f}{4:02.0f}{5:02.0f}".format(year, month, day, hour, minute, second)

    return datetimestamp
